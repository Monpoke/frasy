                ______                   
               |  ____|                  
               | |__ _ __ __ _ ___ _   _ 
               |  __| '__/ _` / __| | | |
               | |  | | | (_| \__ \ |_| |
               |_|  |_|  \__,_|___/\__, |
                                    __/ |
                                   |___/ 

===========================================================
Frasy is a light PHP framework which use powerfull libraries 
to work as Twig like render view and Doctrine as ORM.

This framework want to be easy to pratice and learn it. 
It owns a router where you can connect your controller and url.
You can to defines much environment with differents configuration files.
A console is present, and a lot of features destined to debug your application.

© Pierre BOURGEOIS