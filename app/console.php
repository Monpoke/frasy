<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * This file is frontend for clients *
 */

if(!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);

require "Private" . DS . "bootProject.php";

/*
 * Lunch Frasy and project
 */
new bootProject('Production');