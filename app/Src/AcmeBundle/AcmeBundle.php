<?php

/**
  ====================================================
  ------ This file is a generated file by Frasy ------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */



namespace App\AcmeBundle;
use Frasy\DefaultBundle\Controllers\Bundle;

class AcmeBundle extends Bundle {
    
    public function declareResources(){
       $this->loadResource("routing", $this->getDir() . DS . "Resources" . DS . "Config" . DS . "routing.php");
       $this->loadResource("events", $this->getDir() . DS . "Resources" . DS . "Config" . DS . "events.php");
       $this->loadResource("consolecommands", $this->getDir() . DS . "Resources" . DS . "Config" . DS . "consoleCommands.php");
    }
    
    public function getDir() {
        return __DIR__;
    }
    
    public function getNamespace() {
        return __NAMESPACE__;
    }
    
    public function getName() {
        return "AppAcmeBundle";
    }
    
    
}


