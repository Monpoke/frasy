<?php

/**
  ====================================================
  ------ This file is a generated file by Frasy ------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */


namespace App\AcmeBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;

class HomeController extends Controller {

    public function __construct(){
        parent::__construct();
    }
 
    public function indexAction() {
        return $this->render("App:AcmeBundle:home/index.html.twig");
    }
    
    public function demosAction() {
        return $this->render("App:AcmeBundle:home/demos.html.twig");
    }
    
    
    
}


