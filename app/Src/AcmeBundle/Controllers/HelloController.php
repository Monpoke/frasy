<?php

/**
  ====================================================
  ------ This file is a generated file by Frasy ------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */


namespace App\AcmeBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;

class HelloController extends Controller {

    public function __construct(){
        parent::__construct();
    }
 
    public function helloWorldAction($name) {
        if(empty($name))
            $name = "World";
        
        return $this->render("App:AcmeBundle:demos/hello.html.twig", array('name' => $name, 'code' => $this->getSrc()));
    }
    
    protected function getSrc(){
        return file_get_contents(__FILE__);
    }
}


