<?php

/*
  ====================================================
  ------ This file is a generated file by Frasy ------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * HOME
 */
$router->setPrefix('/');
$router->addRoute('acme_home', '/', 'HomeController::indexAction');

/*
 * DEMOS
 */
$router->setPrefix('/demo');
$router->addRoute('acme_demo_home', '/', 'HomeController::demosAction');
$router->addRoute('acme_demo_hello', '/hello/{name}', 'HelloController::helloWorldAction', array(
    'constraints' => array(
        'name' => array(
            'type' => 'alpha',
            'nullable' => true
        )
    )
));
