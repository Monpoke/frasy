<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * bootProject boot a project with selected environment

 */
class bootProject {

    /**
     * Create constants to determines paths 
     */
    private function determinesPath() {
        /*
         * Application path
         */
        define('APP', realpath(__DIR__ . DS . ".."));


        /**
         * Vendor path
         */
        define('VENDOR', realpath(APP . DS . ".." . DS . "vendor"));
    }

    /**
     * AFTER THIS, RESERVED TO FRASY FRAMEWORK
     */
    
    /**
     * Construct function to lunch Frasy
     * @param type $environment
     */
    public function __construct($environment) {

        $this->determinesPath();

        $this->getFrasy();

        $this->launchFrasy($environment);
    }

    protected function getFrasy() {
        $FrasyFile = VENDOR . DS . "Frasy" . DS . "Bin" . DS . "FrasyBoot.php";

        if (!is_file($FrasyFile)) {
            exit("Frasy could not be loaded... <br />Please check VENDOR constant in File : " . __FILE__ . "::determinesPath()");
        }

        require_once $FrasyFile;
    }

    public function launchFrasy($environment) {
        $class = "\Frasy\Bin\FrasyBoot";
        if (!class_exists($class)) {
            exit("Frasy could not be loaded... Class not Found...<br />Please check VENDOR constant in File : " . __FILE__ . "::determinesPath()");
        }


        \Frasy\Bin\FrasyBoot::boot($environment);
    }

}

