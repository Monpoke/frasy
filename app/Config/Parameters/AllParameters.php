<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Parameters;
use Frasy\DefaultBundle\Controllers\DefaultParameters;
use Frasy\DefaultBundle\Controllers\Controller;

class AllParameters extends DefaultParameters {

    public function __construct() {
        
        parent::__construct();
        
        $parameters = array(
            
            'Auth' => array(
                'Active' => true,
                
                'UserAccess' => "Frasy\ComponentsBundle\Auth\UserAccess",
                'TwigFunctions' => "Frasy\ComponentsBundle\Auth\AuthTwigFunctions",
                
                
                
            ),
            
            
            'Security' => array(
               'Hash' => "ZkdZDZzFZODkxscFBgvbnSLkdjaajDIdeTidjs",
               'Crypt' => "8952489456568415465389654874121465418",
                
            ),
            
            'Twig' => array(
                'Active' => true,
                'extensions' => array(
                   
                    
                ),
                
            ),
            
            
            'Router' => array(
                'dispatchFrom' => "server.path_info",
            ),
            
            'Helpers' => array(
                'Form' => array(
                    'invalidClass' => 'invalidField'
                )
            )
            
        );
        
        $this->saveParameters($parameters);
    }
    
    
    
}

