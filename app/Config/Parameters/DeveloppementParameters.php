<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Parameters;
use App\Config\Parameters\AllParameters;

class ProductionParameters extends AllParameters {

    public function __construct() {
        
        parent::__construct();
        
        $parameters = array();
        
        $this->saveParameters($parameters);
        
    }

    
    
    
    
}

