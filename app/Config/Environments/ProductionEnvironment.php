<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Environments;
use App\Config\Environments\AllEnvironment;

class ProductionEnvironment extends AllEnvironment {

    public function __construct() {
        
        parent::__construct();
        
        $parameters = array(
            'Tmp' => array(
                //'Cache' => __DIR__ . DS . ".." . DS . ".." . DS . "Private" . DS . "Tmp" . DS . "Cache",
                'Cache' => false,
                'Logs' =>  __DIR__ . DS . ".." . DS . ".." . DS . "Private" . DS . "Tmp" . DS . "Logs",
            ),
            
            'Debug' => false,
            
            'BaseUri' => ''
            
        );
        
        $this->saveParameters($parameters);
        
        
        
    }

    
    
    
    
}

