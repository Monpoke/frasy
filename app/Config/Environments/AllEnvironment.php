<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Environments;

use Frasy\DefaultBundle\Controllers\DefaultEnvironment;

class AllEnvironment extends DefaultEnvironment {

    public function __construct() {

        parent::__construct();

        $parameters = array(
            'Active' => true,
            'Tmp' => array(
                'Cache' => APP . DS . "Private" . DS . "Tmp" . DS . "Cache",
                'Logs' => false
            ),
            
            'dbMultiple' => true,
            'dbParams' => array(
                'default' => array(
                    'driver' => 'pdo_mysql',
                    'user' => 'root',
                    'password' => 'root',
                    'dbname' => 'dbname',
                ),
            ),
        );

        $this->saveParameters($parameters);
    }

}

