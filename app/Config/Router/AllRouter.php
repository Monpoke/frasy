<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Router;
use Frasy\DefaultBundle\Controllers\DefaultRouter as Router;

class AllRouter extends Router {
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
}
