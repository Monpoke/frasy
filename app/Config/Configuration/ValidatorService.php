<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace App\Config\Configuration;

use Frasy\ServicesBundle\Controllers\ValidatorController as DefaultValidator;
use Frasy\DefaultBundle\Controllers\Controller;

class ValidatorService extends DefaultValidator {

    protected $types = array();
    protected $delEmailsDomains = array(
        // To fill
        'yopmail.com',
        'cool.fr.nf',
    
    );

    public function __construct() {
        parent::__construct();
        
        $this->addTypeToValidate("emailVendor", "App\Config\Configuration\ValidatorService::validateEmailVendor");
        
    }

    public function validateEmailVendor($value, $options) {

        $email = explode("@", $value);
        
        if(count($email)!==2)
            return false;
        
        $vendor = $email[1];

        if (in_array($vendor, $this->delEmailsDomains))
            return false;

        return true;
    }

}