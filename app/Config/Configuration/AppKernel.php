<?php
/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

// List of declared bundles
$bundles = array(
    
    new SwiftMailer\SwiftMailerBundle(),
    new App\AcmeBundle\AcmeBundle(),
);



// Declares Bundles
$this->registerBundle($bundles);

