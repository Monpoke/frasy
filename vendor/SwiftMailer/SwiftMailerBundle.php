<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace SwiftMailer;
use Frasy\DefaultBundle\Controllers\Bundle;

class SwiftMailerBundle extends Bundle {
    
    public function __construct() {
        parent::__construct();
        
        require_once __DIR__ . DS . "swift_required.php";
    }
    
    public function getDir() {
        return __DIR__;
      
    }
    
    public function getNamespace() {
        return __NAMESPACE__;
    }
    
    public function getName() {
        return "Mailer";
    }
}


