<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * Description of FrasyBoot
 *
 * @author Pierre
 * 
 * 
 */

namespace Frasy\Bin;

use Frasy\ExceptionsBundle\Exceptions\FrontException;
use Frasy\LoadersBundle\Controllers\BundleController;
use Frasy\LoadersBundle\Controllers\ConfigurationController;
use Frasy\LoadersBundle\Controllers\ConsoleController;
use Frasy\LoadersBundle\Controllers\DisplayController;
use Frasy\LoadersBundle\Controllers\DoctrineController;
use Frasy\LoadersBundle\Controllers\ErrorController;
use Frasy\LoadersBundle\Controllers\TwigController;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

require __DIR__ . DS . "globalFunctions.php";

class FrasyBoot {

    /**
     * Boot Frasy
     * @param type $environment
     */
    public static function boot($environment) {

        set_exception_handler(array("\Frasy\Bin\FrasyBoot", "detectException"));
        set_error_handler(array("\Frasy\Bin\FrasyBoot", "detectError"));

        try {
            /*
             * Get Autoloader
             */
            self::getAutoloader();

            /**
             * Lunch Frasy
             */
            self::lunchFrasy($environment);
        }
        catch (\Exception $e) {
            self::detectException($e);
        }
    }

    /**
     * Get Autoloader
     */
    private static function getAutoloader() {
        require __DIR__ . DS . "FrasyLoader.php";
        new FrasyLoader();
    }

    /**
     * Lunch Frasy
     * @param type $environment
     */
    public static function lunchFrasy($environment) {

        // Determines console
        self::determinesConsole();

        // First step : lunch bundle controller
        new BundleController();

        /*
         * Second step : init services
         * 
         */
        $Services = BundleController::getMyBundle("FrasyServices");

        /**
         *  Third step : init listener
         */
        EventsListener::initialization();
        EventsListener::notifyEvent('FRASY_BOOT_START');

        /**
         *  Fourth step :  get configuration
         */
        $Loaders = BundleController::getMyBundle("FrasyLoaders");

        /* @var $Configurator ConfigurationController */
        $Configurator = $Loaders->callController('Configuration');
        $Configurator->load($environment);

        $Configuration = $Configurator::getConfig();

        // Not active website
        if ($Configuration['Environment']['Active'] !== true) {
            throw new FrontException("This environment is not active");
        }

        /**
         * Lunch Doctrine
         */
        new DoctrineController();

        if (!CONSOLE) {
            $Displayer = new DisplayController();
        }
        else {
            $Displayer = new ConsoleController();
        }

        // Dont use license manager
        //$ManagerController = new \Frasy\ComponentsBundle\License\ManagerController();


        // Lunch cache
        FrasyLoader::lunchCache();

        // Notify from end boot
        EventsListener::notifyEvent('FRASY_BOOT_END');


        EventsListener::notifyEvent("FRASY_BEFORE_RENDER");
        $Displayer->display();
    }

    /**
     * 
     * @param \Exception $e
     */
    public static function detectException($e) {
        
        if (CONSOLE === true) {
            exit("Erreur : " . $e->getMessage());
        }

        if (!method_exists($e, "getTypeError") or $e->getTypeError() !== "FrontException") {
            if (TwigController::$initied === true) {
                new ErrorController($e);
            } else {
                new ErrorController($e);
            }



            exit();
        } else {
            exit("<h1>Error</h1><p>{$e->getMessage()}</p>");
        }
    }

    /**
     * This static function is used by PHP as error handler
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     */
    public static function detectError($errno, $errstr, $errfile, $errline) {
        EventsListener::notifyEvent("ERROR_DETECTED", array("params" => array(
                'errno' => $errno,
                'errstr' => $errstr,
                'errfile' => $errfile,
                'errline' => $errline,
        )));
    }

    /**
     * Determines if it is lunch in console
     */
    public static function determinesConsole() {
        if (isset($_SERVER['SHELL']) || isset($argc) || isset($argv) || defined('STDIN') || defined('FRASY_IS_CONSOLE')) {
            define('CONSOLE', true);
        } else {
            define('CONSOLE', false);
        }
    }

}
