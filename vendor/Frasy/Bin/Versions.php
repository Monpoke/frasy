<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\Bin;

class Versions {

    protected $Frasy = "1.0.0";
    protected $Twig = "1.13.2";
    protected $Doctrine = "2.3.0";
    protected $FacebookLib = "?";

    public function getFrasy() {
        return $this->Frasy;
    }

    public function getTwig() {
        return $this->Twig;
    }

    public function getDoctrine() {
        return $this->Doctrine;
    }

    public function getFacebookLib() {
        return $this->FacebookLib;
    }

    public function getAll() {
        return array(
            'Frasy' => $this->getFrasy(),
            'Twig' => $this->getTwig(),
            'Doctrine' => $this->getDoctrine(),
            'Facebook' => $this->getFacebookLib()
        );
    }

}