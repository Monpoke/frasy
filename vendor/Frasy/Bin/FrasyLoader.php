<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * Description of FrasyLoader
 *
 * @author Pierre
 */

namespace Frasy\Bin;

use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\LoadersBundle\Controllers\CacheController as Cache;
use Frasy\LoadersBundle\Controllers\ConfigurationController as Configurator;
use Frasy\DefaultBundle\Controllers\Controller;

class FrasyLoader {

    private static $namespaces = array();
    private static $defaultNamespaces = array();
    private static $configuration = null;
    private static $filesToCache = array();

    public function __construct() {
        self::registerDefaultNamespaces();


        spl_autoload_register(array($this, 'loader'));

        $fileRegistredNamespace = APP . DS . "Config" . DS . "Configuration" . DS . "Autoload.php";
        if (is_file($fileRegistredNamespace)) {
            require $fileRegistredNamespace;
        }
    }

    private static function registerDefaultNamespaces() {

        $namespaces = array(
            'App' => APP,
            'Vendor' => VENDOR,
            'Frasy' => VENDOR . DS . "Frasy",
            'Profiler' => VENDOR . DS . "Frasy" . DS . "Src" . DS . "ProfilerBundle",
            'Twig' => VENDOR . DS . "Twig",
            'Doctrine' => VENDOR . DS . "Doctrine",
            'DoctrineProxies' => APP . DS . "Private" . DS . "Tmp" . DS . "Cache" . DS . "Doctrine",
            'Facebook' => VENDOR . DS . "Facebook",
            'SwiftMailer' => VENDOR . DS . "SwiftMailer",
            'Gedmo' => VENDOR . DS . "Doctrine" . DS . "Extensions" . DS . "Gedmo",
            'PHPSandbox' => VENDOR . DS . "PHPSandbox"
        );


        self::$defaultNamespaces = $namespaces;

        return true;
    }

    /**
     * This function adds possibility to add personnal namespaces
     * @param array $namespaces
     * @return boolean
     */
    public function add(array $namespaces) {
        self::$namespaces = array_merge(self::$namespaces, $namespaces);
        return true;
    }

    /**
     * This function load each class automatically
     * @param type $className
     * @return type
     * @throws \Exception
     */
    public function loader($className) {
        /**
         * Load configuration
         */
        if (self::$configuration === null && defined('FRASY_CONFIGURATION_LOADED')) {
            self::$configuration = Configurator::getConfig();
        }

        if (strpos($className, "Symfony") !== false) {
            return $this->loadSymfony($className);
        }


        $pathOriginal = explode("\\", $className);
        if (count($pathOriginal) == 1) {
            return;
        }

        $file = $pathOriginal[count($pathOriginal) - 1];

        unset($pathOriginal[count($pathOriginal) - 1]);

        $nameSpace = $pathOriginal[0];
        unset($pathOriginal[0]);

        // First is namespace
        $finalPath = array();

        if (isset(self::$defaultNamespaces[$nameSpace])) {
            $finalPath[] = self::$defaultNamespaces[$nameSpace];
        }
        elseif (isset(self::$namespaces[$nameSpace])) {
            $finalPath[] = self::$namespaces[$nameSpace];
        }
        else {
            $finalPath[] = $nameSpace;
        }



        if (isset($pathOriginal[1]) && strpos($pathOriginal[1], "Bundle") !== false) {
            $finalPath[] = "Src";
        }


        foreach ($pathOriginal as $key => $value) {
            $finalPath[] = $value;
        }

        $finalPath[] = $file;

        $fileToInclude = implode(DS, $finalPath) . ".php";

        $wantCache = false;
        if (self::$configuration !== null && self::$configuration['Environment']['Tmp']['Cache'] !== false && strpos($fileToInclude, APP . DS . "Src") !== false) {

            $cachedFile = Cache::getFile($fileToInclude);
            if ($cachedFile === $fileToInclude) {
                $wantCache = true;
            }
            else {
                $fileToInclude = $cachedFile;
            }
        }


        if (is_file($fileToInclude)) {
            require_once $fileToInclude;

            if ($wantCache === true) {
                self::$filesToCache[] = $fileToInclude;
            }
        }
        else {

            if ($nameSpace === "DoctrineProxies") {
                $initialPath = $finalPath[0];
                unset($finalPath[0]);

                $fileToInclude = $initialPath . DS . implode($finalPath) . ".php";
                if (is_file($fileToInclude)) {
                    require_once $fileToInclude;
                    return;
                }
            }


            throw new AppException("The class $className can't be loaded from path $fileToInclude", 505);
        }
    }

    /**
     * This function is used in order to return a ressource static file
     * @param type $path
     * @return type
     * @throws AppException
     */
    public static function returnPath($path) {

        $namespacesDefault = self::$defaultNamespaces;
        $namespacesPerso = self::$namespaces;

        $explode = explode(':', $path);
        $oldPath = $path;
        $path = "";
        $file = "";

        try {
            if (!empty($oldPath) && count($explode) < 2) {
                throw new AppException(__("Please check that path to file render is correct. Given : " . $oldPath));
            }
        }
        catch (\Exception $e) {
            debug($e);
            exit;
        }

        // first part
        if ($explode[0] === "App" or empty($explode[0])) {
            $path .= APP;
        }
        elseif (isset($namespacesDefault[$explode[0]])) {
            $path .= $namespacesDefault[$explode[0]];
        }
        elseif (isset($namespacesPerso[$explode[0]])) {
            $path .= $namespacesPerso[$explode[0]];
        }
        else {
            throw new AppException(__("Unable to solve " . $explode[0] . " repertory. Have you add it to autoloader ?"));
        }

        if (empty($explode[1])) {
            $path .= DS . "Resources";
        }
        else {
            $path .= DS . "Src" . DS . $explode[1] . DS . "Resources";
        }

        $file = str_replace('..', '', $explode[count($explode) - 1]);

        if (strpos($file, '.html')) {
            $path .= DS . "Views";
        }

        if (strpos($file, '.block')) {
            $path .= DS . "Blocks";
        }

        if (strpos($file, '.layout')) {
            $path .= DS . "Layouts";
        }


        $complet = $path . DS . $file;

        return array('all' => $complet, 'path' => $path, 'file' => $file);
    }

    public static function returnDir($namespace) {

        $namespacesDefault = self::$defaultNamespaces;
        $namespacesPerso = self::$namespaces;

        $path = "";
        $file = "";


        // first part
        if ($namespace === "App" or empty($namespace)) {
            $path .= APP;
        }
        elseif (isset($namespacesDefault[$namespace])) {
            $path .= $namespacesDefault[$namespace];
        }
        elseif (isset($namespacesPerso[$namespace])) {
            $path .= $namespacesPerso[$namespace];
        }
        else {
            throw new AppException(__("Unable to solve " . $namespace . " repertory. Have you add it to autoloader ?"));
        }

        return $path;
    }

    public function loadSymfony($className) {

        $className = str_replace("\\", "/", $className);

        if (is_file(VENDOR . "Doctrine" . DS . $className)) {
            require VENDOR . "Doctrine" . DS . $className;
        }
    }

    public static function lunchCache() {
        foreach (self::$filesToCache as $k => $fileToInclude) {
            Cache::cache($fileToInclude);
            unset(self::$filesToCache[$k]);
        }
    }

    public static function set($namespace, $value, $default = false) {

        if ($default === true) {
            $var = &self::$defaultNamespaces;
        }
        else {
            $var = &self::$namespaces;
        }

        return $var[$namespace] = $value;
    }

}
