<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0


 */

/**
 * This function return a translated string
 * @param type $str
 * @return type
 */
function __($str, array $vars=array()) {
    if (function_exists("gettext")) {
        $string = gettext($str);
    } else {
        $string = (string) $str;
    }
    
    if(!empty($vars)){
        foreach ($vars as $key => $value) {
            $string = str_replace($key, $value, $string);
        }
    }
    
    return $string;
}

function getCodeSrc($file, $startLine = null, $linesBetween = 5) {

    $content = file_get_contents($file);

    $content = explode("\n", $content);
    $content = array_change_key_case($content, CASE_UPPER);

    $final = array();
    foreach ($content as $k => $v) {
        $final[$k + 1] = $v;
    }


    if ($startLine === null) {
        return $final;
    } else {
        $lines = array();
        for ($i = $startLine - $linesBetween; $i <= $startLine + 5; $i++) {
            if ($i < $startLine && !isset($final[$i])) {
                continue;
            } elseif ($i > $startLine && !isset($final[$i])) {
                break;
            }

            $final[$i] = htmlentities($final[$i]);

            if ($startLine === $i) {
                $lines[$i] = "<span style='background-color:yellow'>$final[$i]</span>";
            } else
                $lines[$i] = $final[$i];
        }

        return $lines;
    }
}

function debug($in) {
    echo "<pre>" . print_r($in, true) . "</pre>";
    return;
}

function urlPrettify($in) {

    $url = parse_url($in);


    if (!isset($url['port']) || $url['port'] == "80") {
        $port = "";
    } else {
        $port = ":" . $url['port'];
    }


    $i = explode('/', $url['path']);
    $f = array();
    foreach ($i as $k => $v) {
        if (empty($v)) {
            continue;
        }

        $f[] = $v;
    }
    $f = implode('/', $f);

    $url = "{$url['scheme']}://{$url['host']}$port/$f";


    return $url;
}

function dump($in, $return = false) {
    $out = "<pre>" . print_r($in, true) . "</pre>";
    if (!$return) {
        echo $out;
    }
    return $out;
}

function returnFilesDir($dirname) {
    $dir = opendir($dirname);

    $allFiles = array();
    
    while ($file = readdir($dir)) {
        if ($file != '.' && $file != '..' && !is_dir($dirname . $file)) {
            $allFiles['files'][] = realpath($dirname.$file);
        } elseif($file != "." && $file != ".."){
            $allFiles['dirs'][] = realpath($dirname.$file);
        }
            
    }

    closedir($dir);
    return $allFiles;
}

function is_blank($value){
    if (empty($value) && !is_numeric($value)) {
        return true;
    } else {
        return false;
    }
}

/**
 * This function replace first occurence of $to by $str
 * @param string $from
 * @param string $to
 * @param string $str
 * @return string
 */
function str_replace_once($from, $to, $str) {  
    if(!$newStr = strstr($str, $from)) {  
        return $str;  
    }  
    $iNewStrLength = strlen($newStr);  
    $iFirstPartlength = strlen($str) - $iNewStrLength;  
    return substr($str, 0, $iFirstPartlength).$to.substr($newStr, strlen($from), $iNewStrLength); 
} 


if(!function_exists("apache_getenv")){
    
    function apache_getenv($name){
        return false;
    }
    
}
