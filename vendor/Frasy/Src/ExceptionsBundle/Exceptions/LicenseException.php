<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class LicenseException extends BasicException  {
    
    // Default
    public $code = 505;
    public $message = "License exception";
    
    public function __construct($message, $code=505, $previous=null) {
        parent::__construct($message, $code, $previous);
        $this->file="";
        $this->line=0;
    }
    

}