<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class AppException extends BasicException  {
    
    // Default
    public $code = 503;
    public $type = "critical";
    public $message = "Error coming from app";
    public $typeError = "App exception";
    

}