<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class RouterException extends BasicException  {
    
    // Default
    public $code = 500;
    public $type = "error";
    public $message = "Router exception";
    public $typeError = "Router exception";
    

}