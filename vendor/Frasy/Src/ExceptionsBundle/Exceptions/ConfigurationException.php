<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class ConfigurationException extends BasicException  {
    
    // Default
    public $code = 500;
    public $type = "critical";
    public $message = "Configuration exception";
    
    

}