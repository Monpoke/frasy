<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

    class NotFoundException extends BasicException {
    
    // Default
    public $code = 404;
    public $type = "warning";
    public $message = "Page not found";
    public $typeError = "Page not found";
    
    
    
    public function declareHeaders() {
        \Frasy\LoadersBundle\Controllers\DisplayController::addHeader("404", "StatusPage");
        
    }

}