<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */

namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\DefaultBundle\Interfaces\BasicExceptionInterface;

class BasicException extends \Exception implements BasicExceptionInterface {
    
    // Default
    public $code = 500;
    public $message = null;
    public $type = "warning";
    public $typeError = "Basic exception";
    
    public function getTypeError(){
        return $this->typeError;
    }
    
    public function __construct($message, $code=null, $previous=null) {
        
        $message = (empty($message)) ? $this->message : $message;
        
        $code = (empty($code)) ? $this->code : $code;
        
        parent::__construct($message, $code, $previous);
        
        if(method_exists($this, "declareHeaders")){
            $this->declareHeaders();
        }
    }
    
    
    public function declareHeaders(){
        
    }
    
    public function getType() {
        return $this->type;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setType($type) {
        $this->type = $type;
    }



}