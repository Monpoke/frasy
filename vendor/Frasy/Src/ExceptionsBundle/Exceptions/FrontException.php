<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class FrontException extends BasicException  {
    
    // Default
    public $code = 200;
    public $type = "warning";
    public $message = "Website is not active";
    public $typeError = "FrontException";
    

}