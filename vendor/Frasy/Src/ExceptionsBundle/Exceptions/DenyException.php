<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class DenyException extends BasicException {
    
    // Default
    public $code = 403;
    public $type = "error";
    public $message = "Page not found";
    public $typeError = "Deny exception";
    

    public function declareHeaders() {
        parent::declareHeaders();
        \Frasy\LoadersBundle\Controllers\DisplayController::addHeader("HTTP/1.0 403 Forbidden");
        
    }
}