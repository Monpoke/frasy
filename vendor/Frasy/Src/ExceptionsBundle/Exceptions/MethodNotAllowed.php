<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */


namespace Frasy\ExceptionsBundle\Exceptions;

use Frasy\ExceptionsBundle\Exceptions\BasicException;

class MethodNotAllowed extends BasicException  {
    
    // Default
    public $code = 405;
    public $type = "warning";
    public $message = "Method not allowed";
    public $typeError = "Method not allowed";
    

}