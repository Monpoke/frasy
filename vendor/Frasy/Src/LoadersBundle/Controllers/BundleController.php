<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * Description of BundleController
 *
 * @author Pierre
 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;

class BundleController extends Controller {

    public static $registredBundles = array();

    public function __construct() {
        $this->loadErrorHandler();

        // Load Frasy Bundles
        $this->loadFrasyBundle();

        // Personals bundles
        $this->loadAppKernel();
    }

    /**
     * This static function register a bundle
     * @param type $data
     * 
     * @throws \Exception
     */
    public static function registerBundle($data) {
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                self::registerBundle($v);
            }
        } elseif (is_object($data)) {

            $name = $data->getName();

            $cl = get_class($data);
            $cl = explode('\\', $cl);
            unset($cl[count($cl) - 1]);

            $namespace = "\\" . implode('\\', $cl);
            $data->bundleNamespace = $namespace;

            if (isset(self::$registredBundles[$name])) {
                throw new \Exception("$name bundle already exists");
            }

            if (!method_exists($data, "heriteFromBundle")) {
                throw new \Exception("$name bundle must extends Frasy\DefaultBundle\Controllers\Bundle");
            }

            self::$registredBundles[$name] = $data;
        } else {
            trigger_error("Unable to register bundle", E_USER_WARNING);
        }
    }

    /**
     * This function loads basic bundles for Frasy
     */
    public function loadFrasyBundle() {
        $bundles = array(
            new \Frasy\LoadersBundle\LoadersBundle(),
            new \Frasy\ConsoleBundle\ConsoleBundle(),
            new \Frasy\HttpBundle\HttpBundle(),
            new \Frasy\RouterBundle\RouterBundle(),
            new \Frasy\ProfilerBundle\ProfilerBundle(),
            new \Frasy\ServicesBundle\ServicesBundle(),
            new \Frasy\ComponentsBundle\ComponentsBundle(),
        );

        self::registerBundle($bundles);
    }

    public static function getMyBundles() {
        $bundles = self::$registredBundles;

        return $bundles;
    }

    public static function getMyBundle($name) {
        if (isset(self::$registredBundles[$name]))
            return self::$registredBundles[$name];

        return false;
    }

    protected function loadAppKernel() {
        $AppKernel = APP . DS . "Config" . DS . "Configuration" . DS . "AppKernel.php";
        if (is_file($AppKernel)) {
            require $AppKernel;
        } else {
            throw new \Exception("AppKernel was not found under $AppKernel");
        }
    }

    public function loadErrorHandler() {

        
        //throw new Exception("kfeef");      

    }

}
