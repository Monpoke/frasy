<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\AppException;

class TwigController extends Controller {

    protected static $twig = null;
    protected static $loader = null;
    protected static $config = null;
    public static $initied = false;
    public static $tryToInit = false;
    public static $twigIncluded = false;

    public function __construct() {
        self::initTwig();
    }

    public static function initTwig($fromRegister = false) {
        if (self::$initied === true) {
            return true;
        }

        $file = VENDOR . DS . "Twig" . DS . "Autoloader.php";

        self::$config = self::getConfiguration();
        if (!is_file($file)) {
            throw new AppException(__("Twig cannot be loaded"));
        }

        require_once $file;

        if (!$fromRegister) {
            self::registerTwig();
        }
    }

    protected static function registerTwig() {

        if (self::$initied === true) {
            return true;
        }

        self::initTwig(true);
        self::$tryToInit = true;
        \Twig_Autoloader::register();

        self::$loader = new \Twig_Loader_Filesystem(APP . DS . 'Resources' . DS . 'Views' . DS . 'Layouts');


        self::$twig = new \Twig_Environment(self::$loader, array(
            'cache' => is_string(self::$config['Environment']['Tmp']['Cache']) ? self::$config['Environment']['Tmp']['Cache'] . DS . "Twig" . DS : false,
        ));


        // extensions
        self::$twig->addExtension(new \Frasy\DefaultBundle\Defaults\TwigFunctions());
        self::$twig->addExtension(new \Frasy\DefaultBundle\Defaults\FormTwigFunctions());
        self::$twig->addExtension(new \Twig_Extensions_Extension_Text());

        // auth component 
        if (isset(self::$config['Parameters']['Auth']['Active']) && self::$config['Parameters']['Auth']['Active'] === true) {
            if (isset(self::$config['Parameters']['Auth']['TwigFunctions'])) {
                $n = "\\" . self::$config['Parameters']['Auth']['TwigFunctions'];
                self::$twig->addExtension(new $n);
            }

            if (isset(self::$config['Parameters']['Auth']['UserAccess'])) {
                $n = "\\" . self::$config['Parameters']['Auth']['UserAccess'];
                self::$twig->addGlobal('user', new $n);
            }
        }


        if (isset(self::$config['Parameters']['Twig']['extensions']) && is_array(self::$config['Parameters']['Twig']['extensions'])) {
            foreach (self::$config['Parameters']['Twig']['extensions'] as $k => $v) {
                if (!class_exists($v)) {
                    throw new AppException(__("Twig extension $v not exists"));
                } else {
                    self::$twig->addExtension(new $v);
                }
            }
        }
        $bundles = self::getBundles();

        if (isset(self::$config['Parameters']['Twig']['activeExtensions']) && self::$config['Parameters']['Twig']['activeExtensions'] === true) {
            self::registerExtension();
        }


        foreach ($bundles as $bundleName => $bundle) {
            if (!method_exists($bundle, "getDir")) {
                continue;
            }

            $dir = $bundle->getDir() . DS . "Resources" . DS . "Views";
            if (is_dir($dir)) {
                self::$loader->addPath($dir);
            }
        }

        self::$initied = true;
    }

    /**
     * 
     * @return \Twig_Environment
     */
    public function getTwig() {
        if (self::$twig === null) {
            return;
        }

        return self::$twig;
    }

    protected static function registerExtension() {

        $file = VENDOR . DS . "Twig" . DS . "Extensions" . DS . "Autoloader.php";

        if (!is_file($file)) {
            throw new AppException(__("Twig extensions cannot be loaded"));
        }

        require_once $file;

        \Twig_Extensions_Autoloader::register();

        self::$twig->addExtension(new \Twig_Extensions_Extension_Text());
    }

    public static function displayFile($input, array $vars) {

        ob_start();
        if (self::$tryToInit === false) {
            self::registerTwig();
        }

        if (self::$twig != null && method_exists(self::$twig, "render")) {
            echo self::getTwigRender($input, $vars);
        } else {
            echo "Inconnu... " . __FILE__;
        }

        return ob_get_clean();
    }

    /**
     * 
     * @return \Twig_Environment
     */
    public static function returnTwig() {
        $twig = &self::$twig;
        return $twig;
    }

    public static function getTwigRender($input, $vars) {
        try {
            echo self::$twig->render($input, $vars);
        } catch (\Exception $twigExcept) {
            error_log($twigExcept->getMessage());
            exit("An error has occured...");
        }
    }

}
