<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ConsoleBundle\Controllers\CommandsController;

class ConsoleController extends Controller {
    protected static $content = "";
    protected static $toRender = null;
    
    private static $headers = array();
    
    public function __construct() {
       
        $configuration = $this->getConfiguration();
        
        
        // Content which could be outputed is captured
        //ob_start();
            $Console = new \Frasy\ConsoleBundle\Controllers\FrontController();
            self::$content = $Console->lunch();
            
        //self::$content = ob_get_clean();
        
        
    }
    
    public function display($dontEcho=FALSE){
        $config = $this->getConfiguration();
        
        
        if($dontEcho !== true)
            echo self::$content;
        
        
        return self::$content;
    }
    
    /**
     * Return decoration header
     * @return string
     */
    public function getHeader(){
        $header = "-------------------\n";
        $header.= "------ FRASY ------\n";
        $header.= "-------------------\n\n";
        
        return $header;
    }
    
    
    
}

