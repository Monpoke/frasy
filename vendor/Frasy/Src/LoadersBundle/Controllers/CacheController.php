<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;

class CacheController extends Controller {
    
    public static function getCacheKey($filename){
        return sha1($filename);
    }
    
    public static function getPathFile($filename, $cacheKey=false){
        $configuration = self::getConfiguration();
       
        if($cacheKey!==true){
            $filename = self::getCacheKey($filename);
        }
        
        if ($configuration['Environment']['Tmp']['Cache'] === false) {
            return false;
        }

        $filename = $configuration['Environment']['Tmp']['Cache'] . DS . $filename . ".php";
        
        return $filename;
        
    }
    
    public static function getFile($file){
        $path = self::getPathFile($file, false);
        
        if ($path === false) {
            return $file;
        } elseif (is_file($path)) {
            return $path;
        } else {
            return $file;
        }
    }
    
    public static function cache($file){
        if(is_file($file)){
            $contentFile = file_get_contents($file);
            $cachePath = self::getPathFile($file);
            
            if (!file_put_contents($cachePath, $contentFile)) {
                trigger_error("Unable to cache file", E_USER_ERROR);
            }
        }
    }
}

