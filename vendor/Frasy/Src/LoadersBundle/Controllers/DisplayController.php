<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\ProfilerBundle\Controllers\ProfilerController;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

class DisplayController extends Controller {
    protected static $content = "";
    protected static $toRender = null;
    
    private static $headers = array();
    private static $headersPrinted = false;
    
    public function __construct() {
        $configuration = $this->getConfiguration();
        
        
        // Content which could be outputed is captured
        ob_start();
            
            if($configuration['Parameters']['Twig']['Active'] === true){
                $this->lunchTwig();
            } else {
                
            }
            
            $this->lunchRouter();
        
            
            // Now that all is initialized
            if(self::$toRender !== null){
                
                $controllerClass = self::$toRender['namespace'] . "\\Controllers\\" .  self::$toRender['controller'];
                
                $controller = new $controllerClass();
                
                // Get controller
                $result = $controller->callAction(self::$toRender['action'], self::$toRender['parameters']);


                // Print headers
                self::printHeaders();
                
                // Print controller
                $result->display();
                
                
            } else {
                throw new AppException(__("Houps..."));
            }
            
        self::$content = ob_get_clean();

    }
    
    public function display($dontEcho=FALSE){
        $config = $this->getConfiguration();
        
        if($config['Environment']['Debug'] === true){
            self::$content = ProfilerController::addBar(self::$content);
            
        }
            
        if($dontEcho !== true) {
            echo self::$content;
            
        }
        
        return self::$content;
    }
    
    protected function lunchTwig(){
        new \Frasy\LoadersBundle\Controllers\TwigController();
        
    }

    public function lunchRouter() {
        $router = $this->getBundle("FrasyRouter");
        
        
       /* @var $routerController \Frasy\RouterBundle\Controllers\RouterController */
        $routerController=$router->callController('Router');
        
        $return = $routerController->lunchAnalyse();
        
        self::$toRender = $return;
        
        EventsListener::notifyEvent("FRASY_DETERMINED_ROUTE", array('return' => $return, 'status' => 200));
        
    }
    
    public static function getToRender() {
        return self::$toRender;
    }

    public static function setToRender($toRender) {
        self::$toRender = $toRender;
    }

        
    public static function addHeader($header, $identifier=null) {
        
        if(is_array(self::$headers)){
            
            switch($header){
                
                // Applications types
                case "json": case "application/json":
                    $header = "Content-type: application/json";
                    break;
                
                case "png": case "image/png":
                    $header = "Content-Type: image/png";
                    break;
              
                case "js": case "text/javascript": case "javascript": case "txt/js": case "application/javascript":
                    $header = "Content-Type: application/javascript";
                    break;
              
                case "css": case "text/css": case "txt/css":
                    $header = "Content-Type: text/css";
                    break;
              
                case "404": case 404: case "not found":
                    $header = "HTTP/1.0 404 Not Found";
                    break;
                
                case "200": case 200: case "found":
                    $header = "HTTP/1.1 200 OK";
                    break;
                
                
                case "500": case 500: case "found":
                    $header = "HTTP/1.1 500 Internal Server Error";
                    break;
                
                
                
            }
                   
            self::$headers[$identifier] = $header;
            
        } elseif($header === false){
            self::$headers = false;
        }
        
        return true;
        
    }
    
    /**
     * This function print headers
     * @return type
     */
    public static function printHeaders($force=false){
        $headers = self::$headers;
        
        if (self::$headersPrinted === true && $force == false) {
            return;
        }

        if ($headers === false) {
            return;
        }

        if(!headers_sent() || $force===true){
            foreach($headers as $header){
                header($header);
            }
            self::$headersPrinted = true;
       }
    }
    
    
    public static function getHeaders(){
        return self::$headers;
    }
    
}

