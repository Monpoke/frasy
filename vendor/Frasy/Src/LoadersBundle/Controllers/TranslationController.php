<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\ExceptionsBundle\Exceptions\ConfigurationException;
use Frasy\ServicesBundle\Services\SessionService;
use Frasy\ServicesBundle\Services\CookieService;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

class TranslationController extends Controller {

    protected static $config;
    protected static $current = false;

    public function __construct(array $Configuration) {

        if (!isset($Configuration['Parameters']['Translation']))
            return;

        $config = $Configuration['Parameters']['Translation'];
        self::$config = $this->checkConfig($config);
        self::$config = $this->getCorrectConfig(self::$config);
        $this->initGettext(self::$config);
    }

    /**
     * This function check the current translation config
     * @param type $config
     * @return boolean
     * @throws ConfigurationException
     */
    protected function checkConfig($config) {

        if ($config['active'] === false)
            return false;

        if ($config['baseDir'] === false || !is_dir($config['baseDir'])) {
            throw new ConfigurationException("The base dir for translations is incorrect. Please check it.");
        }

        if (!in_array($config['default'], $config['availables'])) {
            throw new ConfigurationException("The default language setted in Parameters does not exist in availables array.");
        }
        
        return $config;
    }

    /**
     * It's will try to fill default parameters
     * @param type $config
     * @return type
     */
    protected function getCorrectConfig($config) {

        // SESSION
        if ($config['session'] === true) {
            $val = SessionService::get($config['sessionPath']);

            if ($val !== null && in_array($val, $config['availables']))
                $config['currentLanguage'] = self::$current = $val;
        }

        // COOKIE 
        if ($config['cookie'] === true) {
            $val = CookieService::get(self::$config['cookieName']);

            // NO SESSION DATA FOUND
            if (self::$current === false) {
                if ($val !== null && in_array($val, $config['availables']))
                    $config['currentLanguage'] = self::$current = $val;
            }

            // SESSION !== COOKIE
            elseif (self::$current !== $val) {
                $expiration = time() + self::$config['cookieTime'];
                self::setCookieLanguage(self::$current, $expiration);
            } 
        }

        // ELSE
        if (self::$current === false) {
            $config['currentLanguage'] = self::$current = $config['default'];
        }

        return $config;
    }

    /**
     * This function is usefull to switch language.
     * @param type $language
     * @return boolean
     */
    public static function switchLanguage($language) {
        if (!in_array($language, self::$config['availables'])) {
            return false;
        }

        if (self::$config['currentLanguage'] === $language)
            return true;


        $expiration = time() + self::$config['cookieTime'];

        if (self::$config['session'] === true)
            self::setSessionLanguage($language);

        if (self::$config['cookie'] === true)
            self::setCookieLanguage($language, $expiration);



        if (self::$config['sendEvent'] !== false)
            EventsListener::notifyEvent(self::$config['sendEvent'], array(
                'language' => $language,
                'expiration' => $expiration
            ));
    }

    /**
     * This function set a cookie
     * @param string $language
     * @param int $expiration
     */
    protected static function setCookieLanguage($language, $expiration) {
        if (CookieService::get(self::$config['cookieName']) !== null)
            CookieService::delete(self::$config['cookieName']);
        
        CookieService::set(self::$config['cookieName'], $language, $expiration, "/");

    }

    /**
     * This function set language in session
     * @param string $language
     */
    protected static function setSessionLanguage($language) {
        SessionService::set(self::$config['sessionPath'], $language);
    }

    /**
     * This function init gettext and translations
     * @param type $config
     */
    protected function initGettext($config) {
        // PROTECT CONSOLE FROM CHANGES
        if(CONSOLE)
            return;
        
        // Set language to French
        putenv('LC_ALL=' . $config['currentLanguage']);
        setlocale(LC_ALL, $config['currentLanguage']);

        // Specify the location of the translation tables
        bindtextdomain($config['nameDomain'], $config['baseDir']);
        bind_textdomain_codeset($config['nameDomain'], 'UTF-8');

        // Choose domain
        textdomain($config['nameDomain']);

        self::switchLanguage($config['currentLanguage']);
    }

    /**
     * Return the current language
     * @return current Language
     */
    public static function getCurrentLanguage(){
        return self::$current;
    }
    
}

