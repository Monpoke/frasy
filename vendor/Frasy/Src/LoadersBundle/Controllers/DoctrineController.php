<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\DataFixtures\Loader as FixtureLoader;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Mapping\Driver\DriverChain;
use Doctrine\ORM\Tools\Setup;
use Frasy\Bin\FrasyLoader;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\ExceptionsBundle\Exceptions\ConfigurationException;

class DoctrineController extends Controller {

    protected static $initied = false;
    protected static $em = null;
    protected static $fixturesLoader = null;

    public function __construct() {
        if (self::$initied !== true) {
            self::loader();

            if (CONSOLE === true) {
                self::loadDataFixtures();
            }
        }
    }

    private static function loader() {
        $dir = FrasyLoader::returnDir("Doctrine");

        require_once($dir . '/ORM/Tools/Setup.php');

        Setup::registerAutoloadDirectory(VENDOR);

        self::configV2();

        self::$initied = true;
    }

    /**
     *  LOAD DATA FIXTURES
     */
    public static function loadDataFixtures() {
        self::$fixturesLoader = new FixtureLoader();

        $bundles = self::getBundles();

        $fixturesDir = array();

        foreach ($bundles as $name => $object) {

            $dir = $object->getDir() . DS . "Fixtures";
            if (is_dir($dir)) {
                $fixturesDir[$name] = $dir;
                self::$fixturesLoader->loadFromDirectory($dir);
            }
        }
    }

    /**
     * 
     * @return FixtureLoader
     */
    public static function getFixturesLoader() {
        return self::$fixturesLoader;
    }

    public static function getEm() {
        return self::$em;
    }

    private static function getDBConfiguration() {
        $config = self::getConfiguration();

        $connexionName = "default";
        // the connection configuration
        if ($config['Environment']['dbMultiple'] === false) {
            $enviDb = $config['Environment']['dbParams'];
        } else {
            $count = count($config['Environment']['dbParams']);
            if ($count > 1 && !defined($config['Environment']['dbConstantConnexion'])) {
                throw new ConfigurationException("I don't know which connexion used for database...");
            } elseif ($count > 1) {
                if (!isset($config['Environment']['dbParams'][constant($config['Environment']['dbConstantConnexion'])])) {
                    throw new AppException("The database connexion called " . constant($config['Environment']['dbConstantConnexion']) . " not exist.");
                } else {
                    $connexionName = constant($config['Environment']['dbConstantConnexion']);
                    $enviDb = $config['Environment']['dbParams'][$connexionName];
                }
            } else { // Just one connexion, just find the name
                $key = array_keys($config['Environment']['dbParams']);
                $connexionName = $key[0];
                $enviDb = $config['Environment']['dbParams'][$connexionName];
            }
        }


        if (!isset($enviDb['driver']) or ! isset($enviDb['user']) or ! isset($enviDb['password']) or ! isset($enviDb['dbname'])) {

            throw new ConfigurationException(__("Please verify that Db settings are correctly configured for database connexion <b>$connexionName</b>"));
        }

        return $enviDb;
    }

    private static function getEntitiesDirs() {
        $bundles = self::getBundles();

        $entitiesDir = array();

        foreach ($bundles as $name => $object) {

            $dir = $object->getDir() . DS . "Entities";
            if (is_dir($dir)) {
                $entitiesDir[$name] = $dir;
            }
        }
        return $entitiesDir;
    }

    /**
     * @todo Change with configuration
     */
    private static function configV2() {
        // cache
        $cache = new ArrayCache();
        $configuration = self::getConfiguration();

        // annotation reader
        $annotationReader = new AnnotationReader();

        // cached annotation reader
        $cachedAnnotationReader = new CachedReader($annotationReader, $cache);

        AnnotationRegistry::registerAutoloadNamespace("Doctrine\ORM", VENDOR);
        AnnotationRegistry::registerAutoloadNamespace("Gedmo\Mapping\Annotation", VENDOR . DS . "Doctrine" . DS . "Extensions");

        // driver chain
        $driverChain = new DriverChain();

        // annotation driver
        $annotationDriver = new AnnotationDriver($cachedAnnotationReader);
        $annotationDriver->addPaths(self::getEntitiesDirs());
        // add entity namespaces 

        $driverChain->addDriver($annotationDriver, 'App');
        $driverChain->addDriver($annotationDriver, 'Users');
        $driverChain->addDriver($annotationDriver, 'Common');
        $driverChain->addDriver($annotationDriver, 'Admin');

        $evm = new EventManager();

        // PROXY PATH
        /**
         * LOAD PROXY DIR FOR DOCTRINE
         */
        $dir = empty($configuration['Environment']['Tmp']['DoctrineCache']) ? $configuration['Environment']['Tmp']['Cache'] : $configuration['Environment']['Tmp']['DoctrineCache'];

        if (is_bool($dir)) {
            $dir = APP . DS . "Private" . DS . "Tmp" . DS . "Cache";
        }

        $doctrineCache = $dir . DS . "Doctrine";
        $fDir = realpath($doctrineCache);

        if ($fDir === false) {

            if (@mkdir($doctrineCache, 0777, true) === false) {
                umask(0);
                exit("Sorry... I can't create cache folder for $doctrineCache");
            } else {
                $fDir  = realpath($doctrineCache);
            }
        }


       // exit("OK");
        // configuration
        $config = new Configuration();
        $config->setMetadataCacheImpl($cache);
        $config->setMetadataDriverImpl($driverChain);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir($fDir);
        FrasyLoader::set("DoctrineProxies", $fDir, true);

        $config->addCustomStringFunction("DATEDIFF", "Frasy\ComponentsBundle\Doctrine\DateDiff");
        $config->addCustomStringFunction("TIMESEC", "Frasy\ComponentsBundle\Doctrine\TimeSec");
        $config->addCustomStringFunction("UNIX_TIMESTAMP", "Frasy\ComponentsBundle\Doctrine\UnixTimestamp");

        $config->setProxyNamespace('DoctrineProxies');
        $config->setAutoGenerateProxyClasses(true);






        $connectionOptions = self::getDBConfiguration();
        // entity manager
        self::$em = EntityManager::create($connectionOptions, $config);

        define('FRASY_DOCTRINE_LOADED', true);
    }

}
