<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\SecurityException;
use Frasy\ExceptionsBundle\Exceptions\ConfigurationException;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;
use Frasy\LoadersBundle\Controllers\TranslationController;

class ConfigurationController extends Controller {

    protected static $environmentName = null;
    protected static $parameters = null;
    protected static $environment = null;
    protected static $routing = null;
    protected static $checkedConfiguration = array();
    protected static $nonCheckedConfiguration = array();

    public function load($environment) {

        // Save Environment name
        self::$environmentName = $environment;
        define('FRASY_ENVIRONMENT', $environment);

        // Load Environment
        self::$environment = self::loadEnvironment($environment);


        /**
         * CHECK CONFIGURATION
         */
        self::handleConfigEnvironment();

        $mustCheck = true;
        $cachedVersion = false;

        if (self::$nonCheckedConfiguration['Environment']['Tmp']['Cache'] !== false && file_exists(self::$nonCheckedConfiguration['Environment']['Tmp']['Cache'])) {
            $cachedVersion = true;

            $Path = self::$nonCheckedConfiguration['Environment']['Tmp']['Cache'] . DS . FRASY_ENVIRONMENT . "Configuration.php";
            if (file_exists($Path)) {
                $co = require $Path;
                self::$checkedConfiguration = $co;
                $mustCheck = false;
            }
        }

        if ($mustCheck) {
            /**
             * LOAD PARAMETERS AND ROUTING
             */
            // Load Parameters
            self::$parameters = self::loadParameters($environment);
            
            // Load routing files
            self::$routing = self::loadRouting($environment);

            
            self::handleConfigParameters();
            self::handleConfigRouting();
            self::handleConfig();

            /**
             * NOT CACHED
             */
            if ($cachedVersion === true) {
                file_put_contents($Path, "<?php return " . var_export(self::$checkedConfiguration, true) . ";");
            }
        }


        EventsListener::notifyEvent("FRASY_CONFIGURATION_LOADED");
    }

    /**
     * This function is used to load parameters for environment
     * @param type $environment
     * @return \Frasy\LoadersBundle\Controllers\parametersClass
     * @throws ConfigurationException
     */
    private static function loadParameters($environment) {
        // Load Parameters
        $parametersClass = "\App\Config\Parameters\\{$environment}Parameters";

        /**
         * First and second tests to check if parameters was found
         */
        if (!class_exists($parametersClass)) {
            $parametersClass = "\App\Config\Parameters\\AllParameters";
            if (!class_exists($parametersClass)) {
                throw new ConfigurationException("Parameters class for $environment was not found");
            }
        }
        $parameters = new $parametersClass;

        if (!method_exists($parameters, "getParameters")) {
            throw new ConfigurationException("Unable to found parameters in class $parametersClass");
        }

        return $parameters;
    }

    private static function loadEnvironment($environment) {
        // Load Parameters
        $environmentClass = "\App\Config\Environments\\{$environment}Environment";

        /**
         * First and second tests to check if environment was found
         */
        if (!class_exists($environmentClass)) {

            $environmentClass = "\App\Config\Environments\\AllEnvironment";
            if (!class_exists($environmentClass)) {
                throw new ConfigurationException("Environment class for $environment was not found");
            }
        }

        $environment = new $environmentClass;

        if (!method_exists($environment, "getParameters")) {
            throw new ConfigurationException("Unable to found parameters in class $environment");
        }

        return $environment;
    }

    private function loadRouting($environment) {
        $router = $this->loadRoutingFiles($environment);


        $bundles = $this->getBundles();

        foreach ($bundles as $k => $bundle) {

            if (isset($bundle->resources['routing'])) {
                foreach ($bundle->resources['routing'] as $bundleName => $values) {

                    $router->setTmpNamespace($bundle->bundleNamespace);

                    foreach ($values as $resource) {
                        require $resource;
                    }
                }
            }
        }



        return $router;
    }

    private static function loadRoutingFiles($environment) {
        // Load Parameters
        $environmentClass = "\App\Config\Router\\{$environment}Router";

        /**
         * First and second tests to check if environment was found
         */
        if (!class_exists($environmentClass)) {
            $environmentClass = "\App\Config\Router\\AllRouter";
            if (!class_exists($environmentClass)) {
                throw new ConfigurationException("Router class for $environment was not found");
            }
        }
        $environment = new $environmentClass;


        return $environment;
    }

    protected static function handleConfig($configuration = null) {
        if ($configuration === null) {
            $configuration = self::$nonCheckedConfiguration;
        }

        if (self::checkConstraints($configuration)) {
            self::$checkedConfiguration = $configuration;
            define('FRASY_CONFIGURATION_LOADED', true);
        }
    }

    protected static function handleConfigEnvironment() {
        $environment = self::$environment->getParameters();

        /**
         * LOGS LEVELS
         *      - all
         *      * errors
         *      - warn  
         *      - none
         */
        $logsLevelsAvailable = self::getAllLogsLevels();

        $return = array('Environment' => array_replace_recursive($environment, array(
                'Active' => (isset($environment['Active']) && $environment['Active'] === true) ? true : false,
                'Debug' => (isset($environment['Debug']) && $environment['Debug'] === true) ? true : false,
                'SiteUri' => isset($environment['SiteUri']) && is_string($environment['SiteUri']) ? $environment['SiteUri'] : self::getDefaultUri(),
                'BaseUri' => isset($environment['BaseUri']) && is_string($environment['BaseUri']) ? $environment['BaseUri'] : self::getDefaultBaseUri(),
                'Tmp' => array_merge($environment['Tmp'], array(
                    'Logs' => isset($environment['Tmp']['Logs']) && (is_string($environment['Tmp']['Logs']) or is_bool($environment['Tmp']['Logs'])) ? $environment['Tmp']['Logs'] : NULL,
                    'Cache' => isset($environment['Tmp']['Cache']) && (is_string($environment['Tmp']['Cache']) or is_bool($environment['Tmp']['Cache'])) ? $environment['Tmp']['Cache'] : null,
                    'PublicFiles' => isset($environment['Tmp']['PublicFiles']) && (is_string($environment['Tmp']['PublicFiles']) or is_bool($environment['Tmp']['PublicFiles'])) ? $environment['Tmp']['PublicFiles'] : null,
                )),
                'LogLevel' => isset($environment['LogLevel']) && isset($logsLevelsAvailable[$environment['LogLevel']]) ? $environment['LogLevel'] : 'none',
                'dbMultiple' => isset($environment['dbMultiple']) && is_bool($environment['dbMultiple']) ? $environment['dbMultiple'] : false,
                'dbConstantConnexion' => isset($environment['dbConstantConnexion']) && is_string($environment['dbConstantConnexion']) ? $environment['dbConstantConnexion'] : "APP_DB_USE_CONNEXION",
                'dbParams' => (isset($environment['dbParams']) && is_array($environment['dbParams'])) ? $environment['dbParams'] : array(),
                'Layouts' => array(
                    'Errors' => array(
                        'Dir' => isset($environment['Layouts']['Errors']['Dir']) && is_string($environment['Layouts']['Errors']['Dir']) ? $environment['Layouts']['Errors']['Dir'] : "::Errors" . DS,
                        'defaultError' => isset($environment['Layouts']['Errors']['defaultError']) && is_string($environment['Layouts']['Errors']['defaultError']) ? $environment['Layouts']['Errors']['defaultError'] : "defaultError.html.twig",
                        'fileFormat' => isset($environment['Layouts']['Errors']['fileFormat']) && is_string($environment['Layouts']['Errors']['fileFormat']) ? $environment['Layouts']['Errors']['fileFormat'] : "{code}.html.twig",
                    ),
                ),
        )));
        

        self::$nonCheckedConfiguration = array_merge(self::$nonCheckedConfiguration, $return);

        return $return;
    }

    protected static function handleConfigRouting() {
        $return = array('Routing' => self::$routing);

        self::$nonCheckedConfiguration = array_merge(self::$nonCheckedConfiguration, $return);

        return $return;
    }

    protected static function handleConfigParameters() {
        $parameters = self::$parameters->getParameters();
        $environment = self::$environment->getParameters();

        $return = array('Parameters' => array_replace_recursive($parameters, array(
                'Security' => array(
                    'Hash' => isset($parameters['Security']['Hash']) && is_string($parameters['Security']['Hash']) ? $parameters['Security']['Hash'] : "UjzhfkFZcKaNcbuhdFRfzeoifzeCZcsqGHds",
                    'Crypt' => isset($parameters['Security']['Crypt']) && ctype_digit($parameters['Security']['Crypt']) ? $parameters['Security']['Crypt'] : "12598983654832412476315746",
                    'RegenerateCrsfOnRefresh' => isset($parameters['Security']['RegenerateCrsfOnRefresh']) && is_bool($parameters['Security']['RegenerateCrsfOnRefresh']) ? $parameters['Security']['RegenerateCrsfOnRefresh'] : true,
                    'SessionDestroyAfter' => isset($parameters['Security']['SessionDestroyAfter']) ? (int)($parameters['Security']['SessionDestroyAfter']) : 2400, // destroy session after 30 minutes of inactivity
                    'SessionDestroyFrasy' => isset($parameters['Security']['SessionDestroyFrasy']) ? (bool)($parameters['Security']['SessionDestroyFrasy']) : true, // destroy automatically the session
                    ),
                'Twig' => array_merge(isset($parameters['Twig']) ? $parameters['Twig'] : array(), array(
                    'Active' => isset($parameters['Twig']['Active']) && is_bool($environment['Tmp']['Cache']) ? $parameters['Twig']['Active'] : true,
                )),
                'Router' => array_merge(isset($parameters['Router']) ? $parameters['Twig'] : array(), array(
                    'dispatchFrom' => isset($parameters['Router']['dispatchFrom']) && is_string($parameters['Router']['dispatchFrom']) ? $parameters['Router']['dispatchFrom'] : null,
                )),
                'Translation' => array(
                    'active' => isset($parameters['Translation']['active']) && is_bool($parameters['Translation']['active']) ? $parameters['Translation']['active'] : false,
                    'nameDomain' => isset($parameters['Translation']['nameDomain']) && is_string($parameters['Translation']['nameDomain']) ? $parameters['Translation']['nameDomain'] : 'App',
                    'baseDir' => isset($parameters['Translation']['baseDir']) && is_string($parameters['Translation']['baseDir']) ? $parameters['Translation']['baseDir'] : false,
                    'default' => isset($parameters['Translation']['default']) && is_string($parameters['Translation']['default']) ? $parameters['Translation']['default'] : false,
                    'availables' => isset($parameters['Translation']['availables']) && is_array($parameters['Translation']['availables']) ? $parameters['Translation']['availables'] : array(),
                    // COOKIE
                    'cookie' => isset($parameters['Translation']['cookie']) && is_bool($parameters['Translation']['cookie']) ? $parameters['Translation']['cookie'] : true,
                    'cookieName' => isset($parameters['Translation']['cookieName']) && is_string($parameters['Translation']['cookieName']) ? $parameters['Translation']['cookieName'] : "FRASY_APP_LANGUAGE",
                    'cookieTime' => isset($parameters['Translation']['cookieTime']) && is_integer($parameters['Translation']['cookieTime']) ? $parameters['Translation']['cookieTime'] : 60 * 60 * 31,
                    // SESSION
                    'session' => isset($parameters['Translation']['session']) && is_bool($parameters['Translation']['session']) ? $parameters['Translation']['session'] : true,
                    'sessionPath' => isset($parameters['Translation']['sessionPath']) && is_string($parameters['Translation']['sessionPath']) ? $parameters['Translation']['sessionPath'] : "App.Translation.Language",
                    'sendEvent' => isset($parameters['Translation']['sendEvent']) && is_string($parameters['Translation']['sendEvent']) ? $parameters['Translation']['sendEvent'] : false,
                )
        )));

        self::$nonCheckedConfiguration = array_merge(self::$nonCheckedConfiguration, $return);

        return $return;
    }

    /**
     * This function return the analysed configuration 
     * @param bool $secured
     * @return array
     */
    public static function getConfig($secured = false) {
        $configuration = (count(self::$checkedConfiguration) > 0) ? self::$checkedConfiguration : self::$nonCheckedConfiguration;

        if ($secured === true) {
            return self::secureConfiguration($configuration);
        }

        return $configuration;
    }

    protected static function getDefaultUri() {
        $sName = $_SERVER['SERVER_NAME'];

        $before = empty($_SERVER['HTTPS']) ? 'http://' : 'https://';

        $port = $_SERVER['SERVER_PORT'];
        if ($port !== "80") {
            $port = ":" . $port;
        }
        else {
            $port = "";
        }

        $fileName_script = explode('/', $_SERVER['SCRIPT_NAME']);
        $fileName = $fileName_script[count($fileName_script) - 1];

        $dir = substr($_SERVER['SCRIPT_NAME'], 0, -(strlen($fileName)));

        return $before . $sName . $port . $dir;
    }

    protected static function getDefaultBaseUri() {
        $fileName_script = explode('/', $_SERVER['SCRIPT_NAME']);
        $fileName = $fileName_script[count($fileName_script) - 1];

        $base = $fileName;

        return $base;
    }

    protected static function checkConstraints($configuration) {
        // Security checks
        if (strlen($configuration['Parameters']['Security']['Hash']) <= 4 || $configuration['Parameters']['Security']['Hash'] == "UjzhfkFZcKaNcbuhdFRfzeoifzeCZcsqGHds") {
            throw new SecurityException(("Please change Security Hash with 5 characters min."));
        }
        elseif (strlen($configuration['Parameters']['Security']['Crypt']) <= 4 || $configuration['Parameters']['Security']['Crypt'] == "12598983654832412476315746") {
            throw new SecurityException(("Please change Security Crypt with 5 characters min."));
        }

        // Logs and cache paths chech
        if ($configuration['Environment']['Tmp']['Cache'] !== false && !is_string($configuration['Environment']['Tmp']['Cache'])) {
            throw new ConfigurationException("Please set cache path in environment to false or set a valid path");
        }
        elseif ($configuration['Environment']['Tmp']['Logs'] !== false && !is_string($configuration['Environment']['Tmp']['Logs'])) {
            throw new ConfigurationException("Please set logs path in environment to false or set a valid path");
        }

        // TRANSLATION MODULE
        elseif ($configuration['Parameters']['Translation']['active'] === true) {
            new TranslationController($configuration);
        }



        return true;
    }

    public static function secureConfiguration($config = null) {
        if ($config === null) {
            $config = self::$checkedConfiguration;
        }

        unset($config['Environment']['dbParams']);
        return $config;
    }

    public static function getAllLogsLevels() {
        return array(
            'emergency' => LOG_EMERG,
            'critical' => LOG_CRIT,
            'error' => LOG_ERR,
            'warning' => LOG_WARNING,
            'notice' => LOG_NOTICE,
            'debug' => LOG_DEBUG,
            'all' => LOG_DEBUG,
            'none' => false
        );
    }

}
