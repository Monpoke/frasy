<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\LoadersBundle\Controllers;

use Frasy\Bin\FrasyLoader;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HttpBundle\HttpKernel\HttpResponse;
use Frasy\ServicesBundle\Services\DataLogger;

class ErrorController extends Controller {

    protected static $configuration = null;

    /**
     * 
     * @param \Exception $e
     */
    public function __construct(\Exception $e) {
        exit($e->getMessage());
        self::$configuration = $this->getConfiguration();

        DataLogger::logThrowing($e);

        try {
            $data = $this->lunch($e);

            // CALL A CONTROLLER 
            if (isset(self::$configuration['Environment']['ErrorPages']['Controller'])) {
                $errorController = self::$configuration['Environment']['ErrorPages']['Controller'];
                if ($this->lunchUserErrorController($errorController, $e) === true) {
                    return true;
                }
            }

            if (self::$configuration['Environment']['Debug'] !== true && (!isset(self::$configuration['Environment']['DisplayErrorPages']) || self::$configuration['Environment']['DisplayErrorPages'] !== false)) {
                // TEST
                // format file
                $code_one = array(
                    '{code}'
                );
                $code_two = array(
                    $e->getCode(),
                );

                // log message
                error_log($e->getMessage());

                $file = str_replace($code_one, $code_two, self::$configuration["Environment"]["Layouts"]['Errors']['fileFormat']);

                $dir = self::$configuration["Environment"]["Layouts"]['Errors']['Dir'];
                $defaultError = self::$configuration["Environment"]["Layouts"]['Errors']['defaultError'];


                $path1 = FrasyLoader::returnPath($dir . $file);
                $path2 = FrasyLoader::returnPath($dir . $defaultError);

                /**
                 * AJAX REQUESTS
                 */
                if ($this->methodIs("ajax")) {
                    $return = new HttpResponse($e->getMessage());
                }
                
                /**
                 * TEST RENDERS PATH
                 */
                else {
                    if (!file_exists($path1['all'])) {
                        if (!file_exists($path2['all'])) {

                            $return = $this->render('Frasy:ProfilerBundle:globalError.html.twig', $data);
                        }
                        else {
                            $return = $this->render($dir . $defaultError, $data);
                        }
                    }
                    else {                            
                        
                        $return = $this->render($dir . $file, $data);
                    }
                }
                
                DisplayController::printHeaders();

                $return->display();
            }
            else {

                $return = $this->render('Frasy:ProfilerBundle:exceptionDetected.html.twig', $data);
                $return->display();
            }
            

        }
        catch (\Exception $v) {
            exit('Final error : ' . $v->getMessage());
        }
    }

    /**
     * This function get source code origin from error.
     * @param \Exception $e
     * @return type
     */
    protected function lunch(\Exception $e) {
        $sourceCode = \getCodeSrc($e->getFile(), $e->getLine());
        $sourceCode = implode("\n", $sourceCode);

        return array('e' => $e, 'sourceCode' => $sourceCode);
    }

    /**
     * This function get a personnal user error controller.
     * This user error controller must return TRUE or Frasy will continue with his own layout.
     * @param type $errorController
     */
    public function lunchUserErrorController($errorController, \Exception $e) {
        if (is_string($errorController)) {
            $parts = explode('::', $errorController);
            $controllerE = (isset($parts[0]) && is_string($parts[0])) ? $parts[0] : null;
            $methodError = (isset($parts[1]) && is_string($parts[1])) ? $parts[1] : null;

            if (class_exists($controllerE)) {
                $class = new $controllerE();
                if (method_exists($class, $methodError)) {
                    return call_user_func_array(array($class, $methodError), array('e' => $e));
                }
            }
        }

        return false;
    }

}
