<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand;

class CacheCommand extends ConsoleCommand {

    public function cacheClearCommand($input = null) {
        $configuration = $this->getConfiguration();

        $cachePath = $configuration['Environment']['Tmp']['Cache'];
        if ($cachePath === false) {
            if ($input !== null)
                return "Cache is disabled in environment.";
            else
                return array(
                    "message" => "Cache is disabled in environment.",
                    "status" => false
                );
        } 
        
        

        $delete = $this->deleteFilesAndDir($cachePath);
        if ($delete !== true) {

            if ($input !== null) {
                return $delete;
            }
            else {
                return array(
                    "message" => $delete,
                    "status" => false
                );
            }
        }


        if ($input !== null) {
            return "Cache was cleaned !";
        }
        else {
            return array(
                "message" => "Cache was cleaned !",
                "status" => true
            );
        }
    }

    protected function deleteFilesAndDir($dirname) {
        $dir = opendir($dirname);

        $realPath = realpath($dirname);
        if($realPath==="/"){
            return "Can't remove root.";
        }
        
        while ($file = readdir($dir)) {
            if ($file != '.' && $file != '..' && !is_dir($dirname . DS . $file)) {
                if (!unlink($dirname . DS . $file)) {
                    return "Error to delete file " . $dirname . DS . $file;
                }
            }
            elseif ($file != "." && $file != "..") {
                $r = $this->deleteFilesAndDir($dirname . DS . $file);
                if ($r !== true)
                    return $r;
                if (!rmdir(realpath($dirname . DS . $file)))
                    return "Error to delete dir " . $dirname . DS . $file;
            }
        }

        closedir($dir);
        return true;
    }

}

