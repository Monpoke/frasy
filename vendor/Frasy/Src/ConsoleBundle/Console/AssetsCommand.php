<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand;
use Frasy\HelpersBundle\Helpers\HeritanceHelper;
use Frasy\HelpersBundle\Helpers\OSHelper;

class AssetsCommand extends ConsoleCommand {

    protected $basePath;
    protected $equivalentsFiles;
    protected $equalsMinBig;
    protected $tmpBundleName;
    private $log = "";

    public function installAssetsCommand($parameters) {

        $copyOption = isset($parameters['options']['use']) ? $parameters['options']['use'] : "copy";
        $cacheClear = isset($parameters['options']['clear']) && is_bool($parameters['options']['clear']) ? $parameters['options']['clear'] : true;

        $bundles = $this->getBundles();
        $dirsToExplore = array();

        /**
         * This foreach will get all public resources
         */
        foreach ($bundles as $name => $bundle) {

            $path = $bundle->getDir() . DS . "Resources" . DS . "Public";
            if (is_dir($path)) {
                $dirsToExplore[$name] = $path;
            }
        }

        // SET BASE PATH
        $this->basePath = APP . DS . "Web" . DS . "bundles";


        if ($copyOption === "copy") {
            /**
             * Scan all directory files
             */
            foreach ($dirsToExplore as $k => $value) {
                $this->equivalentsFiles[$k] = $value;
                $dirsToExplore[$k] = HeritanceHelper::scanDirectory($value);
            }

            $r = $this->copyFiles($copyOption, $dirsToExplore);
        } elseif ($copyOption === "symlink") {
            $r = $this->linkDirs($dirsToExplore);
        }

        if ($r !== true) {
            return $r;
        }

        $out = "Success copy using $copyOption option!\n";

        /**
         * Cache clear
         */
        if ($cacheClear) {
            $cacheCommand = new CacheCommand();
            $out.="Requesting to delete cache...\n";
            $out .= $cacheCommand->cacheClearCommand($parameters) . "\n";
        }

        /**
         * Add some logs
         */
        if(!empty($this->log)){
            $out .= "\nLogs:\n".$this->log;
        }
        
        return $out;
    }

    /**
     * 
     * @param string $bundleName
     * @return string
     */
    public function getAssetDirName($bundleName) {
        if (strpos($bundleName, "Bundle") === false) {
            $bundleName .= "bundle";
        }

        $bundleName = preg_replace("#^(App|Frasy)#Usi", "", $bundleName);
        $bundleName = strtolower($bundleName);
        return $bundleName;
    }

    public function copyFiles($copyOption, $dirsToExplore) {
        if (is_dir($this->basePath)) {
            HeritanceHelper::deleteTree($this->basePath);
        }


        mkdir($this->basePath);
        
        foreach ($dirsToExplore as $bundleName => $hierarchy) {
            $bundlename = $this->getAssetDirName($bundleName);
            mkdir($this->basePath . DS . $bundlename);
            $this->tmpBundleName = $bundlename;
            $this->equalsMinBig[$bundlename] = $bundleName;
            $r = $this->subCopy($copyOption, $hierarchy, $bundlename, $bundleName);
            
            if ($r !== true) {
                return $r;
            }
        }

        return true;
    }

    public function subCopy($copyOption, $dir, $parent = "", $bundleName = "") {
        $parent2 = explode(DS, $parent);
        unset($parent2[0]);
        $parentFinal = implode(DS, $parent2);
        $baseFrom = $this->equivalentsFiles[$bundleName] . DS . $parentFinal;
       
        foreach ($dir as $k => $v) {
            
            if (is_dir($baseFrom . DS . $k)) { // Directory
                mkdir($this->basePath . DS . $parent . DS . $k);
                $r = $this->subCopy($copyOption, $v, $parent . DS . $k, $bundleName);
                if ($r !== true) {
                    return $r;
                }
            } else {

                $originFrom = $baseFrom . DS . $v;

                $basePath = $this->basePath . DS . $parent;
                $baseFile = $basePath . DS . $v;
                
                if ($copyOption === "copy") {
                    $r = copy($originFrom, $baseFile);
                } elseif ($copyOption === "symlink") {
                    $r = symlink($originFrom, $baseFile);
                } else {
                    return "Unrecognized option : $copyOption";
                }

                if (!$r) {
                    $this->log .= "Error has occured : cannot write $baseFile. Skiped...\n";
                }
            }
        }
        return true;
    }

    /**
     * 
     * @param array $dirsToExplore
     */
    protected function linkDirs($dirsToExplore) {
        
        $os = OSHelper::getNameOs();
        
        if($os==="windows"){
            /**
             * Scan all directory files
             */
            foreach ($dirsToExplore as $k => $value) {
                $this->equivalentsFiles[$k] = $value;
                $dirsToExplore[$k] = HeritanceHelper::scanDirectory($value);
            }
            
            return $this->copyFiles("symlink", $dirsToExplore);
        }
        
        if (is_dir($this->basePath)) {
            HeritanceHelper::deleteTree($this->basePath);
        }
        
        mkdir($this->basePath);
        chmod($this->basePath, 0777);
        
        foreach ($dirsToExplore as $name => $dir) {

            $bundlename = $this->getAssetDirName($name);

            $pathDir = $this->basePath . DS . $bundlename;
            symlink($dir, $pathDir);
        }
        return true;
    }

}
