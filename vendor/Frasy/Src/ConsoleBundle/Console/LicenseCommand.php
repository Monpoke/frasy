<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand;
use Frasy\HelpersBundle\Helpers\HeritanceHelper;
use Frasy\ComponentsBundle\License\ActivatorController;

class LicenseCommand extends ConsoleCommand {

    
    public function activeCommand($params){
        $re = new ActivatorController($params);
        return $re->active();
    }
    
    public function installCommand($params){
        $re = new ActivatorController($params);
        return $re->downloadLicense();
    }
    
    public function removeCommand($params){
        $re = new ActivatorController($params);
        return $re->removeLicense();
    }
    
    

}