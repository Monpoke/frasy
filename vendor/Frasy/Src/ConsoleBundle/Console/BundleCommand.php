<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand;

class BundleCommand extends ConsoleCommand {
    protected $basePath;
    protected $originFrom;
    
    public function createBundleCommand($params) {

        $bundlename = isset($params['commands'][1]) ? $params['commands'][1] : "";

        if (empty($bundlename))
            return "Please provide a bundle name";

        if (strpos($bundlename, "Bundle") === false) {
            $bundlename.="Bundle";
        }

        $this->basePath = APP . DS . "Src" . DS . $bundlename;

        if (is_dir($this->basePath))
            return "An error has occured : The directory $this->basePath exists.";

        $this->originFrom = $this->getDefaultBundle();

        // Default bundle will be copied as 
        $resultCopy = $this->lunchCopy($bundlename, $this->originFrom);
        if($resultCopy !== true){
            return $resultCopy;
        } 

        return "Succes operation !";
    }

    public function getDefaultBundle() {
        $origin = VENDOR . DS . "Frasy" . DS . "Src" . DS . "DefaultBundle" . DS . "Folders" . DS . "DefaultBundleGeneration" . DS . "SrcBundle";
        return $origin;
    }

    public function lunchCopy($bundlename, $originFrom) {

        $files = \Frasy\HelpersBundle\Helpers\HeritanceHelper::scanDirectory($originFrom);
        
        $parametersForInterpretation = array(
          'bundleName' => $bundlename,  
          'bundleMinName' => str_replace('bundle', '', strtolower($bundlename)),
        );
        
        if(mkdir($this->basePath) === false){
            return "An error has occured : Cannot write !";
        }
        
        return $this->copyData($files, $parametersForInterpretation);
        
    }
    
    public function copyData($files, $parametersForInterpretion, $parent=""){
        foreach($files as $name => $file){
            /**
             * It's a directory
             */
            if(is_string($name)) {
                  
                if(mkdir($this->basePath . DS . $parent . DS . $name) === false){
                    return "An error has occured : Cannot write !";
                }
                
                $this->copyData($files[$name], $parametersForInterpretion, $parent . DS . $name);
            } else { // Else, it's a file
                $final = $this->replaceInTemplate($parametersForInterpretion, $this->originFrom  . $parent . DS . $file);
                
                if(isset($final['parameters']['saveAs'])){
                    $file = $final['parameters']['saveAs'];
                }
                
                if(file_put_contents($this->basePath . $parent . DS . $file, $final['srcCode']) === false)
                   return "An error has occured : Cannot write !";
            }
        }
        
        return true;
    }
    
    public function replaceInTemplate($parameters, $template){
        
        $srcTemplate = file_get_contents($template);
        if($srcTemplate === false)
            return "Error template";
        
        $srcTemplate = str_replace(array(
            "/*** START TEMPLATE ***",
            "*** END TEMPLATE **/"
        ), "", $srcTemplate);
        
        /* @var $matches array */
        $matches=array();
        preg_match_all("#\{([a-zA-Z0-9_-]{1,})\}#Usi", $srcTemplate, $matches);
        foreach ($matches[1] as $value) {
            if(isset($parameters[$value]))
                $srcTemplate = str_replace ("{".$value."}", $parameters[$value], $srcTemplate); 
            
        }
        
        /**
         * Check for parameters
         */
        $allParameters=array();
        preg_match_all("#\[FILE(.+)=\'(.+)\'\]#Usi", $srcTemplate, $allParameters);
        
        $finalParameters = array();
        foreach ($allParameters[1] as $k=>$name) {
            $value = $allParameters[2][$k];
            $srcTemplate = str_replace($allParameters[0][$k], "", $srcTemplate);
            $finalParameters[$name] = $value;
        }
        
        return array(
          'srcCode' =>  $srcTemplate,
          'parameters' => $finalParameters
            
        );
        
    }
    
    

}

