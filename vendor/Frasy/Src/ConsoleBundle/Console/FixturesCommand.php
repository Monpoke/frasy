<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand;
use Frasy\LoadersBundle\Controllers\DoctrineController;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class FixturesCommand extends ConsoleCommand {

    public function loadAllCommand($params) {
        $append = true;
        if (isset($params['options']['noappend']) && $params['options']['noappend'] === true) {
            $append = false;
        }

        $fixturesLoader = DoctrineController::getFixturesLoader();

        if (isset($params['commands'][1])) {
            // SO TRY TO LOAD THIS FIXTURE
            $path = \Frasy\Bin\FrasyLoader::returnDir($params['commands'][1]);
            return $path;
        }

        debug($params);
        exit();
        $em = $this->getEntityManager();

        $purger = new ORMPurger();
        $executor = new ORMExecutor($em, $purger);
        $result = $executor->execute($fixturesLoader->getFixtures(), $append);

        if ($result === null) {
            if ($append === false) {
                return "Fixtures data loaded!";
            } else {
                return "Fixtures data appended!";
            }
        } else {
            return $result;
        }
    }

}
