<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ConsoleBundle;
use Frasy\DefaultBundle\Controllers\Bundle;

class ConsoleBundle extends Bundle {
    
    public function declareResources(){
       $this->loadResource("consolecommands", $this->getDir() . DS . "Config" . DS . "consoleCommands.php");
    }
    
    public function getDir() {
        return __DIR__;
      
    }
    
    public function getNamespace() {
        return __NAMESPACE__;
    }
    
    public function getName() {
        return "FrasyConsole";
    }
}


