<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */
use Frasy\ConsoleBundle\Controllers\CommandsController as Commands;

Commands::registerCommand("assets", array(
    'commands' => array(
        'install' => array(
            'description' => 'Install assets',
            'action' => '\Frasy\ConsoleBundle\Console\AssetsCommand::installAssets'
        ),
        'help' => array(
        ),
    ),
));

Commands::registerCommand("bundle", array(
    'commands' => array(
        'generate' => array(
            'description' => 'Generate bundle',
            'action' => '\Frasy\ConsoleBundle\Console\BundleCommand::createBundle'
        ),
    ),
    'description' => array(
        'Actions about bundles.'
    ),
));



Commands::registerCommand("cache", array(
    'commands' => array(
        'clear' => array(
            'description' => 'Clear cache',
            'commands' => array(
                'test' => array(),
            ),
            'action' => '\Frasy\ConsoleBundle\Console\CacheCommand::cacheClear'
        ),
        'help' => array(
        ),
    ),
));


Commands::registerCommand("fixtures", array(
    'commands' => array(
        'load' => array(
            'help' => array(
                'Load fixtures',
                'Test',
                'ooo'
            ),
            'action' => '\Frasy\ConsoleBundle\Console\FixturesCommand::loadAll'
        ),
    ),
    
    'help' => array(
        "This command is used to import data fixtures in database."
    )
));



Commands::registerCommand("license", array(
    'commands' => array(
        'manage' => array(
            'commands' => array(
                'active' => array(
                    'description' => 'Active a license',
                    'action' => '\Frasy\ConsoleBundle\Console\LicenseCommand::active'
                ),
                'install' => array(
                    'description' => 'Install a license',
                    'action' => '\Frasy\ConsoleBundle\Console\LicenseCommand::install'
                ),
                'remove' => array(
                    'description' => 'Remove a license',
                    'action' => '\Frasy\ConsoleBundle\Console\LicenseCommand::remove'
                ),
            ),
        ),
    ),
));



Commands::registerCommand("doctrine", array());

