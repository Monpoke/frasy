<?php

/**
 * This file is a part of Framewwork 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */

namespace Frasy\ConsoleBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ConsoleBundle\Components\Colors;
use Frasy\HelpersBundle\Helpers\OSHelper;
use Frasy\ConsoleBundle\Controllers\CommandsController;

class FrontController extends Controller {

    private $commands = null;
    
    public function __construct($Config, $Bundles) {
        parent::__construct();

        $this->reserved_Bundles = $Bundles;
        $this->reserved_Config = $Config;
        $this->commands = new CommandsController();
    }

    public function lunch() {
        
        return $this->analyze();
    }

    public function colorizeOutput($input, $fg = null, $bg = null) {
        if (OSHelper::getNameOs() === "linux") {
            $colors = new Colors();
            return $colors->getColoredString($input, $fg, $bg);
        } else {
            return $input;
        }
    }

    public function displayLines($lines) {
        $outEncoding = $this->detectEncodage();

        // L'utilisation de PHP_EOL garantit que le retour à la ligne sera correct
        foreach ($lines as $line) {
            if ($outEncoding != 'UTF-8') {
                $line = iconv('UTF-8', $outEncoding, $line);
            }
            echo $line . PHP_EOL;
        }
    }

    public function detectEncodage() {
        // PHP_OS nous permet de détecter le jeu de caractère du terminal
        if (OSHelper::getNameOs() === "windows") {
            // L'encodage CP850 est utilisé pour les fenêtres DOS
            $outEncoding = 'CP850';
        } else {
            // Sous Unix, on peut avoir plusieurs jeux de caractère différents
            $locale = setlocale(LC_CTYPE, 0);
            $outEncoding = substr($locale, 6);
            if (empty($outEncoding)) {
                $outEncoding = 'ISO-8859-1';
            } else {
                switch ($outEncoding) {
                    case 'euro':
                        $outEncoding = 'ISO-8859-15';
                        break;
                }
            }
        }
        // Demande à iconv d'ignorer les caractères non supportés 
        // par le jeu de caractère de sortie
        $outEncoding .= '//IGNORE';

        return $outEncoding;
    }

    function cli_clearscreen($out = TRUE) {
        if (OSHelper::getNameOs() !== "linux") {
            return;
        }

        $clearscreen = chr(27) . "[H" . chr(27) . "[2J";
        if ($out) {
            print $clearscreen;
        } else {
            return $clearscreen;
        }
    }

    function cli_arguments($args) {
        array_shift($args);
        $endofoptions = false;

        $ret = array
            (
            'commands' => array(),
            'options' => array(),
            'flags' => array(),
            'arguments' => array(),
        );

        while ($arg = array_shift($args)) {

            // if we have reached end of options,
            //we cast all remaining argvs as arguments
            if ($endofoptions) {
                $ret['arguments'][] = $arg;
                continue;
            }

            // Is it a command? (prefixed with --)
            if (substr($arg, 0, 2) === '--') {

                // is it the end of options flag?
                if (!isset($arg[3])) {
                    $endofoptions = true;
                     // end of options;
                    continue;
                }

                $value = "";
                $com = substr($arg, 2);

                // is it the syntax '--option=argument'?
                if (strpos($com, '=')) {
                    list($com, $value) = split("=", $com, 2);
                }

                // is the option not followed by another option but by arguments
                elseif (isset($args[0]) && strpos($args[0], '-') !== 0) {
                    while (isset($args[0]) && strpos($args[0], '-') !== 0) {
                        $value .= array_shift($args) . ' ';
                    }
                    $value = rtrim($value, ' ');
                }

                $ret['options'][$com] = !empty($value) ? $value : true;
                continue;
            }

            // Is it a flag or a serial of flags? (prefixed with -)
            if (substr($arg, 0, 1) === '-') {
                for ($i = 1; isset($arg[$i]); $i++) {
                    $ret['flags'][] = $arg[$i];
                }
                continue;
            }

            // finally, it is not option, nor flag, nor argument
            $ret['commands'][] = $arg;
            continue;
        }

        if (!count($ret['options']) && !count($ret['flags'])) {
            if (count($ret['commands']) === 0) {
                $ret['commands'] = array_merge($ret['commands'], $ret['arguments']);
            }
        }
        return $ret;
    }

    protected function analyze() {
        $input =  $this->cli_arguments((!empty($_SERVER['argv']) ? $_SERVER['argv'] : array()));



       // if (!in_array('S', $input['flags']))
         //   $this->cli_clearscreen();

        if (count($input['commands']) < 1 && count($input['arguments']) < 1) {
            $o = "List of availables commands\nFor some help, type command:help\n\n";
            $o .= $this->commands->getAllCommands();
            
            return $this->display($o);
        }

        $command = empty($input['commands'][0]) ? $input['arguments'][0] : $input['commands'][0];
        
        
        if($command === "doctrine"){
            unset($_SERVER['argv'][1]);
            $this->detectEncodage();
            return $this->commands->lunchDoctrineConsole();
            
        }
        
        $resultCommand = $this->commands->commandExists($command);
        
        if ($resultCommand === true) {
            
            return $this->display($this->commands->callCommand($command, $input));
        } else if(is_array($resultCommand)){
            // most relevant commands
            $relevant = "\t-".implode("\n\t-", $resultCommand);
            
            return $this->showError("Did you want say :\n".$relevant);
        }

        return $this->showError($this->getMsgError(1));
    }

    protected function getMsgError($id) {
        switch ($id) {
            case 1:
                $m = "Command not found";
                break;
            default:
                $m = "No message configured";
                break;
        }
        
        return $m;
    }

    protected function showError($m){
        $this->cli_clearscreen(true);
        
        $h = $this->getHeader();
        
        return $h  . $m."\n";
    }
    
    private function display($input){
        $this->cli_clearscreen(true);
        
        $h = $this->getHeader();
        
        return $h  . $input."\n";
        
    }
    
    
    public function getHeader(){
        $r =  "--------------------\n";
        $r .= "------- FRASY ------\n";
        $r .= "--------------------\n";
        
        
        return $r;
    }
    
    
}