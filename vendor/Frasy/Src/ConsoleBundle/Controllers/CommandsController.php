<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

/**
 * Description of BundleController
 *
 * @author Pierre
 */

namespace Frasy\ConsoleBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\LoadersBundle\Controllers\DoctrineController as DoctrineLoader;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

class CommandsController extends Controller {

    private static $commands = array();
    private static $init = false;

    /**
     * This function returns all commands allowed for this app 
     * @return String
     */
    public function getAllCommands() {
        $commands = self::$commands;
        $f = array();

        foreach ($commands as $name => $command) {
            $f[] = $name;
        }


        return implode("\n", $f);
    }

    public function __construct() {
        parent::__construct();

        self::init();
    }

    /**
     * Initializes some things before start command
     * @return null
     */
    public static function init() {
        if (self::$init !== false) {
            return;
        }

        self::$init = true;
    }

    /**
     * This function register a new command to app
     * @param type $command
     * @param array $parameters
     * @throws AppException
     */
    public static function registerCommand($command, array $parameters) {
        $command2 = explode(':', $command);


        $var = &self::$commands;


        foreach ($command2 as $k => $v) {
            if (!isset($var[$v])) {
                $var[$v] = array();
            } elseif (!is_array($var[$v])) {
                throw new AppException(__('An error has occured ! ' . $command . ' command console is already registred.'));
            }

            $var = &$var[$v];
        }

        $var = $parameters;
    }

    /**
     * Test by reccurence if a function exists.
     * 
     * @todo Not working
     * @param type $command
     * @param type $src
     */
    private function recurCommandExists($command, $src) {
        
    }

    /**
     * Checks if a function exists. 
     * Else, return description command and help.
     * @param type $command
     * @return boolean
     */
    public function commandExists($command) {
        $command2 = explode(':', $command);


        $var = self::$commands;

        if (isset($var[$command2[0]])) {
            $var = $var[$command2[0]];

            $commands_availables = isset($var['commands']) ? $var['commands'] : array();

            $paramTwo = isset($command2[1]) ? $command2[1] : "help";

            if (isset($commands_availables[$paramTwo])) {
                unset($command2[1]);
                if (isset($commands_availables[$paramTwo]['commands']) && count($command2) !== 2) {
                    $this->recurCommandExists($command2, $commands_availables[$paramTwo]['commands']);
                }
                
                return true;
                
            } 
            // check after help and descrition
            elseif(isset($var['help']) || isset($var['description'])) {
                return true;
            } 
            // default description for command
            
            else {
                
            }
        } else {
            return $this->determineMostRelevant($command);
        }

        return $this->determineMostRelevant($command);
    }

    public function determineMostRelevant($current) {
        $all = $this->getAllPaths();

        $seemsToBe = array();

        // no shortest distance found, yet
        $shortest = -1;


        foreach ($all as $path) {
            $lev = levenshtein(str_replace(':', '', $current), str_replace(':', '', $path));

            if ($lev == $shortest || $shortest < 0) {
                // set the closest match, and shortest distance
                $seemsToBe[] = $path;
                $shortest = $lev;
            } elseif($lev < $shortest) {
                $seemsToBe = array($path);
                $shortest = $lev;
            }
            
        }
        
        return !empty($seemsToBe) ? $seemsToBe : false;
    }

    /**
     * This function returns all path.
     * @return type
     */
    public function getAllPaths() {

        $allPath = self::$commands;

        $constructedPaths = $this->makeAllPaths($allPath);

        return $constructedPaths;
    }

    /**
     * Constitute all paths commands.
     * @param type $in
     * @return string
     */
    private function makeAllPaths($in) {
        $final = array();

        foreach ($in as $key => $value) {
            $final[] = $key;

            if (isset($value['commands'])) {
                foreach ($this->makeAllPaths($value['commands']) as $key2 => $value2) {
                    $final[] = $key . ":" . $value2;
                }
            }
        }

        return $final;
    }

    /**
     * This function will call a command which exists
     * @param type $command
     * @param type $input
     * @return type
     */
    public function callCommand($command, $input) {
        $command2 = explode(':', $command);

        $firstCommandName = $command2[0];
        unset($command2[0]);
        $commandArray = self::$commands[$firstCommandName];

        $lastGoodCommand = $firstCommandName;
        foreach ($command2 as $command) {
            $intermediate = $this->testNavigate($command, $commandArray);
            if ($intermediate !== false) {
                $commandArray = $intermediate;
                $lastGoodCommand.=":$command";
            } else {
                // Error has occured : command not found
                return "Command not found ! Last good command was : $lastGoodCommand";
            }
        }


        if (
                (in_array('?', $input['flags']) OR isset($input['options']['?'])) 
                OR isset($input['options']['help']) 
                OR (in_array('h', $input['flags']) 
                OR isset($input['options']['h'])) 
                
                && isset($commandArray['help'])) {
            
            return "Help of command $lastGoodCommand \n\t" . (is_array($commandArray['help']) ? implode("\n\t", $commandArray['help']) : $commandArray['help']);
        }
        
        if (empty($commandArray['action'])) {
            // check for help or description command
            if(isset($commandArray['help']) || isset($commandArray['description']) || isset($commandArray['commands'])){
                
                // description by default
                if(isset($commandArray['description']) && (!isset($input['options']['help']) && !in_array('h', $input['flags']))){
                    return (is_array($commandArray['description']) ? implode("\n", $commandArray['description']) : $commandArray['description']);
                } elseif(isset($commandArray['commands'])){
                    return "Commands available for $command:\n" . implode("\n", array_keys($commandArray['commands']));
                }
                else {
                    return (is_array($commandArray['help']) ? implode("\n", $commandArray['help']) : $commandArray['help']);
                }
            }
            
            return "Command $lastGoodCommand have no action !";
            
        } else {
            $actionName = $commandArray['action'];
            $action = explode('::', $actionName);

            if (count($action) !== 2) {
                return "ERROR :  $actionName must contain two part : CONTROLLER::Action";
            }

            $controller = $action[0];
            $action = strpos($action[1], 'Command') !== false ? $action[1] : $action[1] . "Command";

            if (!class_exists($controller)) {
                return "ERROR :  $controller not exists !";
            }

            $controller2 = new $controller();

            $allClass = \Frasy\HelpersBundle\Helpers\HeritanceHelper::getAllParent($controller2);

            if (!in_array("Frasy\DefaultBundle\Controllers\ConsoleCommand", $allClass)) {

                return "ERROR :  $controller must extends Frasy\DefaultBundle\Controllers\ConsoleCommand !";
            }

            if (!method_exists($controller, $action)) {
                return "ERROR :  $controller::$action not exists !";
            }

            /**
             * Check parameters
             */
            $this->checkParameters($commandArray);

            return $controller2->$action($input);
        }
    }

    /**
     * 
     * @param type $command
     * @param type $input
     * @return boolean
     */
    private function testNavigate($command, $input) {

        if (isset($input['commands'][$command])) {
            return $input['commands'][$command];
        } else {
            return false;
        }
    }

    /**
     * Just lunch doctrine console.
     * @return type
     */
    public function lunchDoctrineConsole() {
        $entityManager = DoctrineLoader::getEm();

        $helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
            'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($entityManager->getConnection()),
            'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
        ));


        return ConsoleRunner::run($helperSet);
    }

    /**
     * This function analyses the input parameters.
     * @param array $commandArray
     */
    public function checkParameters(array $commandArray) {
        $commandArray = isset($commandArray['parameters']) ? $commandArray['parameters'] : array();

        foreach ($commandArray as $k => $v) {
            if ($k === "useBuffer" && $v === false) {
                if (!defined('FRASY_PARAMETERS_BUFFER')) {
                    define('FRASY_PARAMETERS_BUFFER', false);
                }
            }
        }
    }

}
