<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Defaults;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\DefaultBundle\Controllers\TwigExtension;
use Frasy\ServicesBundle\Services\SecurityService;
use Frasy\ServicesBundle\Services\SessionService;

class TwigFunctions extends TwigExtension {

    protected $config = null;
    protected $siteUri = "http://localhost/";

    public function __construct() {
        $this->getConfig();
    }

    public function getName() {
        return 'frasytwigfunctions';
    }

    public function getGlobals() {
        return array(
            'FrasyVersions' => new \Frasy\Bin\Versions(),
            'config' => Controller::getConfiguration(true),
            'currentLanguage' => $this->currentLanguage(),
            'currentRoute' => $this->currentRoute()
        );
    }

    public function getFunctions() {

        return array(
            'charset' => new \Twig_Function_Method($this, 'charset', array('is_safe' => array('html'))),
            'docType' => new \Twig_Function_Method($this, 'docType', array('is_safe' => array('html'))),
            'meta' => new \Twig_Function_Method($this, 'meta', array('is_safe' => array('html'))),
            'style' => new \Twig_Function_Method($this, 'style', array('is_safe' => array('html'))),
            'css' => new \Twig_Function_Method($this, 'css', array('is_safe' => array('html'))),
            'script' => new \Twig_Function_Method($this, 'script', array('is_safe' => array('html'))),
            'image' => new \Twig_Function_Method($this, 'image', array('is_safe' => array('html'))),
            'url' => new \Twig_Function_Method($this, 'url', array('is_safe' => array('html'))),
            'route' => new \Twig_Function_Method($this, 'route', array('is_safe' => array('html'))),
            'print_r' => new \Twig_Function_Method($this, 'print_r', array('is_safe' => array('html'))),
            'getPreloads' => new \Twig_Function_Method($this, 'getPreloads', array('is_safe' => array('html'))),
            'flashMessage' => new \Twig_Function_Method($this, 'flashMessage', array('is_safe' => array('html'))),
            'genBtn' => new \Twig_Function_Method($this, 'genBtn', array('is_safe' => array('html'))),
            'getFlash' => new \Twig_Function_Method($this, 'getFlash', array('is_safe' => array('html'))),
            'getCrsf' => new \Twig_Function_Method($this, 'getCrsf', array('is_safe' => array('html'))),
            'pluralForm' => new \Twig_Function_Method($this, 'pluralForm', array('is_safe' => array('html'))),
            'dump' => new \Twig_Function_Method($this, 'dump', array('is_safe' => array('html'))),
            'convertDate' => new \Twig_Function_Method($this, 'convertDate', array('is_safe' => array('html')))
            
        );
    }
    
    /**
     * This function return the current route name.
     * @return string
     */
    public function currentRoute(){
        return Controller::getCurrentRoute();
    }

    /**
     * Generates a meta
     * @param type $name
     * @param type $value
     * @param type $third
     * @return type
     */
    public function meta($name, $value, $third = null) {


        $name = strtolower($name);


        switch ($name):


            default:
                return '<meta name="' . $name . '" content="' . $value . '" ' . $third . ' />\n';
                break;
        endswitch;
    }

    /**
     * Generate link type.
     * @param type $rel
     * @param array $options
     * @return type
     */
    public function link($rel, array $options) {

        $name = strtolower($rel);

        $args = $this->parseHtmlArguments($options);

        return '<link rel="' . $name . '" ' . $args . ' />' . "\n";
    }

    /**
     * Generates css link
     * @param string $link
     * @param type $rel
     * @param array $options
     * @return type
     */
    public function css($link, $rel = null, array $options = array()) {


        $prefix = "";
        if (strpos($link, 'http') === false) {
            if (!isset($options['noDir']))
                $dir = "bundles/commonbundle/css/";
            else {
                $dir = "";
                unset($options['noDir']);
            }

            $prefix = $this->siteUri . $dir;
        }
        if (!isset($options['extension']) or $options['extension'] === false) {
            $extension = ".css";

            if (isset($options['extension']))
                unset($options['extension']);
        }
        else {
            $extension = "." . $extension;

            unset($options['extension']);
        }

        
        $link = $prefix . $link . $extension;

        $rel = ($rel === null) ? 'stylesheet' : $rel;


        if (isset($options['media'])) {
            $media = $options['media'];
            unset($options['media']);
        }
        else
            $media = "screen";

        
        $link .= $this->getCacheVersion("?v=");
        $link= urlPrettify($link);
        
        return $this->link($rel, array(
                    'type' => 'text/css',
                    'media' => $media,
                    'href' => $link,
                        ) + $options);
    }

    /**
     * This function loads a js file
     * @param type $name
     * @param array $options
     * @return string
     */
    public function script($link, array $options = array()) {



        if (strpos($link, "http") !== false)
            return '<script type="text/javascript" src="' . $link . '"></script>';

        if (!isset($options['extension']) or $options['extension'] === false) {
            $extension = ".js";

            if (isset($options['extension']))
                unset($options['extension']);
        }
        else {
            $extension = "." . $extension;

            unset($options['extension']);
        }

        if (!isset($options['noDir']))
            $dir = "bundles/commonbundle/js/";
        else {
            $dir = "";
            unset($options['noDir']);
        }


        $path = '<script type="text/javascript" src="' . $this->siteUri . $dir . $link . $extension . $this->getCacheVersion("?v=") . '"></script>';

        return $path;
    }

    /**
     * Generates image link.
     * @param string $link
     * @param array $options
     * @return string
     */
    public function image($link, array $options = array()) {


        $prefix = $dir = "";
        if (strpos($link, 'http') === false) {
            if (!isset($options['noDir']))
                $dir = "bundles/commonbundle/images/";
            else
                unset($options['noDir']);

            $prefix = $this->siteUri . $dir;
        }


        $link = $prefix . $link;

        if (isset($options['justPath'])) {
            $justPath = true;
            unset($options['justPath']);
        }

        $args = $this->parseHtmlArguments($options);

        $link .= $this->getCacheVersion("?v=");

        if (isset($justPath))
            return $link;
        else
            return '<img src="' . $link . '" ' . $args . ' />';
    }

    /**
     * This function generates a url link
     * @param type $body
     * @param type $url
     * @param array $options
     * @param type $confirmMessage
     * @return type
     */
    public function url($body, $url = null, array $options = array(), $confirmMessage = false) {


        if (!isset($options['escape']))
            $body = str_replace('"', '\"', $body);
        else
            unset($options['escape']);

        if (isset($options['justPath'])) {
            $justPath = true;
            unset($options['justPath']);
        }

        if (!isset($options['title'])) {
            $title = ' title="' . $body . '"';
        }
        else {
            if ($options['title'] === null)
                $title = "";
            else
                $title = ' title="' . $options['title'] . '"';

            unset($options['title']);
        }

        $url = $this->path($url);

        if ($confirmMessage !== false) {
            $confirmMessage = ' onclick="return(confirm(\'' . $confirmMessage . '\'));"';
        }
        else
            $confirmMessage = "";

        $args = $this->parseHtmlArguments($options);


        if (!isset($justPath))
            $url = '<a href="' . $url . '" ' . $title . $args . $confirmMessage . '>' . $body . '</a>';
        else
            $url = $url;

        return (string) $url;
    }

    public function route($routeName, array $parameters = array()) {
        $r = array(
            'route' => $routeName,
            'arguments' => $parameters
        );

        return $this->path($r);
    }

    public function path($url) {
        
        if(!isset($this->config['Routing']) || is_object($this->config['Routing'])===false){
            return "#routerHasNotBeenInitied";
        }

        if (!is_array($url)) {
            if (strpos($url, 'http') === false) {
                $url = $this->config['Environment']['SiteUri'] . $url;
                $url = urlPrettify($url);
            }
        }
        else {
            if (isset($url['route'])) {
                $route = $url['route'];

                $dones = $this->config['Routing']->getRoute($route);
                if ($dones !== false) {
                    $arguments = (isset($url['arguments']) && is_array($url['arguments'])) ? $url['arguments'] : array();
                    $siteUri = $this->config['Environment']['SiteUri'];
                    $baseUri = $this->config['Environment']['BaseUri'];

                    $routeUrl = $this->config['Routing']->tryFillParameters($route, $arguments);

                    $url = $siteUri . $baseUri . $routeUrl;
                    
                    $url = urlPrettify($url);
                    
                }
                else {
                    $url = $route . "#routeNotFound";
                }
            }
        }

        return $url;
    }

    /**
     * This function shows an array
     * @param type $input
     * @return type
     */
    public function print_r($input, $pre = true) {


        if (!$pre)
            return htmlentities(print_r($input, true));
        else
            return "<pre>" . $this->print_r($input, false) . "</pre>";
    }

    /**
     * This function returns charset for html views.
     * @param type $charset
     * @return type
     */
    public function charset($charset = "utf-8") {


        return '<meta http-equiv="Content-Type" content="text/html; charset=' . ($charset) . '" />';
    }

    /**
     * This function returns a docType
     * @param type $type
     * @return string
     */
    public function docType($type = "html5") {


        switch ($type) {
            case "html4-trans":
                $r = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
                break;

            case "html4-strict":
                $r = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">';
                break;

            case "html4-frame":
                $r = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">';
                break;

            case "xhtml-strict":
                $r = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
                break;

            case "xhtml-frame":
                $r = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">';
                break;

            default:
                $r = "<!DOCTYPE html>";
                break;
        }

        return $r;
    }

    /**
     * This function returns a style balise
     * @param array $styles
     * @param bool $oneline
     * @return string
     */
    public function style(array $styles, $oneline = true) {


        if (!is_bool($oneline))
            return false;

        $output = "<style type='text/css'>";
        foreach ($styles as $k => $v) {
            if ($oneline === false)
                $output .= "\n";

            $output .= $k . ':' . $v . ';';
        }

        if ($oneline === false)
            $output.="\n</style>";
        else
            $output.="</style>";

        return $output;
    }

    public function flashMessage($id = null) {

        return SessionService::getFlash($id);
    }

    public function parseHtmlArguments(array $input) {


        $final = "";

        foreach ($input as $k => $v) {
            if (is_numeric($k))
                $k = $v;

            $final.= " $k=\"$v\"";
        }

        return $final;
    }

    // fonction pour generer un bouton
    function genBtn($name, $class = NULL, $name_submit = NULL, $complements = "") {


        if ($name_submit == NULL)
            $name_submit = "";
        else
            $name_submit = 'name="' . $name_submit . '"';

        if ($class == NULL)
            $class = "";
        else {
            if (preg_match('#\#(.+)#', $class)) { // si on a affaire à un id
                $class = preg_replace('#\#(.+)#', '$1', $class);
                $class = 'id="' . $class . '" ';
            }
            else { //sinon un class
                $class = preg_replace('#\.(.+)#', '$1', $class);
                $class = 'class="' . $class . '" ';
            }
        }

        if ($name == "retour" or $name == "annuler" or $name == "apercu" or $name == "editeur") {
            echo "<img style=\"vertical-align: baseline;\" src=\"/bundles/commonbundle/images/boutons/bouton_{$name}.png\" onMouseOver=\"this.src='/bundles/commonbundle/images/boutons/bouton_{$name}_hover.png'\" onMouseOut=\"this.src='/bundles/commonbundle/images/boutons/bouton_{$name}.png'\" {$class}{$name_submit} {$complements}/>";
        }
        else {
            echo "<input class=\"bouton_{$name}\" type=\"image\" src=\"/bundles/commonbundle/images/boutons/bouton_{$name}.png\" onMouseOver=\"this.src='/bundles/commonbundle/images/boutons/bouton_{$name}_hover.png'\" onMouseOut=\"this.src='/bundles/commonbundle/images/boutons/bouton_{$name}.png'\" {$class}{$name_submit} {$complements}/>";
        }
    }

    public function getFlash($key) {


        $dones = SessionService::getFlash($key);
        return $dones;
    }

    public function getCrsf() {


        return SecurityService::getCrsfToken();
    }

    private function getConfig() {
        if ($this->config === null)
            $this->config = Controller::getConfiguration();

        $this->siteUri = urlPrettify($this->config['Environment']['SiteUri']) . "/";
    }

    public function dump($in) {
        return "<pre>" . print_r($in, true) . "</pre>";
    }

    public function convertDate($timestamp, $mode = 1) {
        $helper = new \Frasy\HelpersBundle\Helpers\FormatHelper();
        return $helper->convertDate($timestamp, $mode);
    }

    /**
     * Returns cache version
     * @param type $id
     * @return string
     */
    public function getCacheVersion($id = "v=") {

        if (isset($this->config['Parameters']['Helpers']['Html']['browerForceCache'])) {
            return $id . $this->config['Parameters']['Helpers']['Html']['browerForceCache'];
        }
        else {
            return "";
        }
    }

    public function pluralForm($nb = 0, $sgForm = "", $plForm = "", $none = "") {
        $sgForm = str_replace("{nb}", $nb, $sgForm);
        $plForm = str_replace("{nb}", $nb, $plForm);

        if ($nb > 1)
            return $plForm;
        elseif ($nb === 1) {
            return $sgForm;
        }
        else
            return $none;
    }
    
    /**
     * If translation is used, it will return the current language (in 5 characters)
     * Ex: fr_FR, en_US, en_GB
     * @return type
     */
    public function currentLanguage(){
        return \Frasy\ServicesBundle\Services\Translator::getLanguage();
    }

}

