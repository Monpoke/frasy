<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Defaults;

use Frasy\DefaultBundle\Controllers\TwigExtension;
use Frasy\HelpersBundle\Forms\HtmlForm;
use Frasy\DefaultBundle\Controllers\Controller;

class FormTwigFunctions extends TwigExtension {

    protected $htmlCode = "";

    public function getName() {
        return 'frasyformtwigfunctions';
    }

    public function getFunctions() {
        return array(
            'form_widget' => new \Twig_Function_Method($this, 'form_widget', array('is_safe' => array('html'))),
            'form_field' => new \Twig_Function_Method($this, 'form_field', array('is_safe' => array('html'))),
            'form_label' => new \Twig_Function_Method($this, 'form_label', array('is_safe' => array('html'))),
            'form_error' => new \Twig_Function_Method($this, 'form_error', array('is_safe' => array('html'))),
            'form_isError' => new \Twig_Function_Method($this, 'form_isError', array('is_safe' => array('html'))),
            'form_open' => new \Twig_Function_Method($this, 'form_open', array('is_safe' => array('html'))),
            'form_close' => new \Twig_Function_Method($this, 'form_close', array('is_safe' => array('html'))),
            'form_rest' => new \Twig_Function_Method($this, 'form_rest', array('is_safe' => array('html'))),
            'form_get' => new \Twig_Function_Method($this, 'form_get', array('is_safe' => array('html'))),
        );
    }

    public function form_get($entity, $formtype) {
        $entity = "\\" . $entity;
        $entity = new $entity();
        $FormBuilder = Controller::createFormBuilder($entity, $formtype);
        return $FormBuilder->createView();
    }

    public function form_widget($form) {
        if (!is_object($form)) {
            return "Argument for form_widget is not allowed.";
        }

        return $form->makeAllForm();
    }

    public function form_field($form, $fieldName) {
        $field = $this->checkFieldExists($form, $fieldName);
        if (!is_array($field)) {
            return $field;
        }



        if (method_exists($form, "field_" . $field['htmlDones']['type'])) {
            $method = "field_" . $field['htmlDones']['type'];
            $codeHtmlGen = $form->$method($field);
        }
        else {
            $codeHtmlGen = $form->field_text($field);
        }

        return $codeHtmlGen;
    }

    public function form_label($form, $fieldName, $append = " :") {
        $field = $this->checkFieldExists($form, $fieldName);
        if (!is_array($field)) {
            return $field;
        }

        $codeHtmlGen = $form->label_form($field, $append);

        return $codeHtmlGen;
    }

    public function form_error($form, $fieldName, $pre = '', $post = '') {
        $field = $this->checkFieldExists($form, $fieldName);
        if (!is_array($field)) {
            return $field;
        }

        // Check for error
        $errorField = $form->checkErrorField($field);
        if ($errorField !== false) {
            return $pre . $errorField['message'] . $post;
        }

        return null;
    }

    public function form_isError($form, $fieldName, $value = "") {
        $field = $this->checkFieldExists($form, $fieldName);
        if (!is_array($field)) {
            return $field;
        }

        // Check for error
        $errorField = $form->checkErrorField($field);
        if ($errorField !== false) {
            return (empty($value) ? true : $value);
        }

        return false;
    }

    public function form_open($form, array $args = array()) {
        if (!is_object($form)) {
            return "[Given form not exists]";
        }

        return $form->form_open($args);
    }

    public function form_close($form) {
        if (!is_object($form)) {
            return "[Given form not exists]";
        }

        return $form->form_close();
    }

    public function form_rest($form, array $arguments = array()) {
        if (!is_object($form)) {
            return "[Given form not exists]";
        }

        return $form->form_rest($arguments);
    }

    public function checkFieldExists($form, $fieldName) {
        if (!is_object($form)) {
            return "[Given form not exists]";
        }

        $formView = $form->getForm();

        if (!isset($formView[$fieldName]))
            return "[Field $fieldName not exists]";

        $field = $formView[$fieldName];
        return $field;
    }

}
