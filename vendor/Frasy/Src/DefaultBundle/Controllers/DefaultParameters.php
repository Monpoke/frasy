<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Controllers;
use Frasy\ExceptionsBundle\Exceptions\AppException;

class DefaultParameters {
    protected $parameters = array();

    public function __construct() {
        $parameters = array();
        
        
        
        $this->saveParameters($parameters);
    }
    
    protected function saveParameters($parameters) {
        if (!is_array($parameters)) {
            throw new AppException(get_class($this) . " : Parameters must be an array");
        }

        $this->parameters = array_merge($this->parameters, $parameters);
        return;
    }

    public function getParameters(){
        return $this->parameters;
    }
    
    
}

