<?php

/**
 * This file is a part of Framewwork 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */

namespace Frasy\DefaultBundle\Controllers;

abstract class TwigExtension extends \Twig_Extension implements \Twig_ExtensionInterface {
    

}
