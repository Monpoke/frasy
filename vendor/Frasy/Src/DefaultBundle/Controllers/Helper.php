<?php

/**
 * This file is a part of Framewwork 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */
/**
 * This variable is used for add namespaces to autoloader
 */

namespace Frasy\DefaultBundle\Controllers;

use Frasy\DefaultBundle\Interfaces\Helper as HelperInterface;

abstract class Helper implements HelperInterface {

    public function __construct() {
        
    }

}