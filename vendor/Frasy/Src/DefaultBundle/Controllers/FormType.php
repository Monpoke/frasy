<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\DefaultBundle\Controllers;
use Frasy\DefaultBundle\Interfaces\FormType as FormTypeInterface;
abstract class FormType implements FormTypeInterface {
    /**
     *
     * @var \Frasy\ServicesBundle\Services\FormBuilder 
     */
    protected $Builder;
    
    /**
     * Constructor
     * @param \Frasy\ServicesBundle\Services\FormBuilder $Builder
     */
    public function __construct(\Frasy\ServicesBundle\Services\FormBuilder $Builder) {
        $this->Builder = $Builder;
    }
    
    /**
     * Return FormBuilder
     * @return \Frasy\ServicesBundle\Services\FormBuilder
     */
    final public function getBuilder() {
        return $this->Builder;
    }
    
    
}