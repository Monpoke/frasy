<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Controllers;

class Component extends Controller {
    
    public function __construct() {
        parent::__construct();
        
    }
    
}