<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 
 *@todo Construction dynamique de classe avec arguments

 */

namespace Frasy\DefaultBundle\Controllers;

use Frasy\DefaultBundle\Interfaces\Bundle as BundleInterface;
use Frasy\ExceptionsBundle\Exceptions\RouterException;

abstract class Bundle implements BundleInterface {

    public $bundleNamespace = __NAMESPACE__;
    
    public function __construct() {
        if (method_exists($this, "declareResources"))
            $this->declareResources();
    }

    /**
     * This function call controller from this bundle
     * @param type $controller
     * @param array $args
     * @return \Frasy\DefaultBundle\Controllers\controller
     * @throws \Exception
     */
    public function callController($controller, array $params = array()) {
        if (strpos($controller, "Controller") === false)
            $controller.= "Controller";
        
        $controller = $this->bundleNamespace . "\\Controllers\\" . $controller;
        
        if(!class_exists($controller))
            throw new \Exception ("Class not exist : $controller ");
        
       
       
        $object = new $controller();
       
        if(!method_exists($object, "heriteFromController"))
            throw new \Exception ("Class $controller must extends default Frasy\DefaultBundle\Controllers\Controller");
            
        
        return $object;
    }

    /**
     * This function is usefull to determines if child controllers are correctly extended
     * @return boolean
     */
    public function heriteFromBundle() {
        return true;
    }

    /**
     * This function returns 
     * @param string $type
     * @param type $path
     * @throws RouterException
     */
    protected function loadResource($type, $path) {
        $type = strtolower($type);
        if ($type === "routing") {
            if (!is_file($path))
                throw new RouterException(__("File $path not exists for routing resource."));

            $myNamespace = $this->bundleNamespace;
            $this->resources['routing'][$myNamespace][$path] = $path;
        
            
        } elseif($type === "events"){
            if (!is_file($path))
                throw new RouterException(__("File $path not exists for events resource."));
                
            require $path;
        } elseif($type === "consolecommands"){
            if (!is_file($path))
                throw new RouterException(__("File $path not exists for console commands resource."));
                
            require $path;
        } 
    }

}

