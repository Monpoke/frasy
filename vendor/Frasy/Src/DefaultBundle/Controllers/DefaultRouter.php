<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Controllers;

use Frasy\ExceptionsBundle\Exceptions\RouterException;
use App\Config\Configuration\ValidatorService as Validator;

class DefaultRouter {

    protected $declaredRoutes = array();
    protected $Routes = array();
    protected $tmp_namespaces = null;
    protected $tmp_prefix = null;
    protected $tmp_bundlename = null;

    public function __construct() {
        
    }

    final public function addRoute($route_name, $pattern, $destination, array $options = array()) {


        if (isset($this->declaredRoutes[$route_name])) {
            throw new RouterException(("Route $route_name already exists for bundle : " . $this->tmp_bundlename));
        }

        $this->declaredRoutes[$route_name] = $this->tmp_namespaces;

        // Checks options
        // $options;

        $pattern = $this->tmp_prefix . $pattern;
        if (substr($pattern, 0, 1) !== "/") {
            $pattern = "/" . $pattern;
        }

        // Del of blancks
        $pattern1 = explode('/', $pattern);
        $p = array();
        $nbSegments = 0;
        foreach ($pattern1 as $p2) {
            if (!empty($p2)) {
                $p[] = $p2;
                $nbSegments++;
            }
        }

        $pattern = "/" . implode("/", $p);


        // Determines destination
        if (!is_string($destination)) {
            throw new RouterException(("Destination for route $route_name must be a string"));
        }

        $destination = explode('::', $destination);
        if (count($destination) !== 2) {
            throw new RouterException(("Asked controller for route $route_name is not correct"));
        }

        $controller = $destination[0];
        if (!strpos($controller, "Controller")) {
            $controller.="Controller";
        }

        $action = $destination[1];
        if (!strpos($action, "Action")) {
            $action.="Action";
        }



        /* VARIABLES */
        $matches = array();
        preg_match_all("#\{[A-Za-z0-9]{1,}\}#Usi", $pattern, $matches);
        $variables = str_replace(array('{', '}'), '', $matches[0]);
        $variables_final = array();

        $nullableVariable = 0;
        $firstNullableVariable = null;

        foreach ($variables as $variable) {


            $contraints = array();

            if (isset($options['constraints'][$variable])) {

                if (!is_array($options['constraints'][$variable])) {
                    throw new RouterException(("Please checks that variable $variable contraints is an array"));
                }
                else {
                    $contraints = $this->checkContraints($options['constraints'][$variable], $variable);
                    unset($options['constraints'][$variable]);

                    if (isset($contraints['nullable']) && $contraints['nullable'] === true) {

                        // First variable to be nullable
                        if ($nullableVariable === 0) {
                            $nullableVariable = 1;
                            $firstNullableVariable = $variable;
                        }
                        else {
                            $nullableVariable += 1;
                        }
                    }
                    elseif ($nullableVariable > 0) {
                        throw new RouterException(__("Variables after $firstNullableVariable must be nullable for $route_name route"));
                    }
                }
            }
            else {
                throw new RouterException(__("Please create a constraint array for variable $variable from $route_name route"));
            }

            $variables_final[$variable] = array(
                'name' => $variable,
                'constraints' => $contraints,
            );
        }



        $this->Routes[$this->tmp_namespaces][$route_name] = array(
            'routeName' => $route_name,
            'pattern' => $pattern,
            'forceSegments' => $nbSegments - $nullableVariable,
            'controller' => "Controllers" . "\\" . $controller,
            'controller_name' => $controller,
            'action' => $action,
            'variables' => $variables_final,
            'options' => $options,
            'namespace' => $this->tmp_namespaces,
            'bundleName' => $this->tmp_bundlename
        );


        return true;
    }

    final public function setPrefix($prefix) {
        $this->tmp_prefix = $prefix;
    }

    final public function setBundleName($bundle) {
        $this->tmp_bundlename = $bundle;
    }

    final public function setTmpNamespace($set) {
        $this->tmp_prefix = "";
        $this->tmp_bundlename = "";

        return $this->tmp_namespaces = $set;
    }

    final public function loadRoutingFile($path) {

        if (!is_file($path)) {
            throw new RouterException(("Cannot load file $path"));
        }
    }

    final public function getRoutes() {
        return $this->Routes;
    }

    final public function getRoute($name) {
        if (isset($this->declaredRoutes[$name])) {
            return $this->Routes[$this->declaredRoutes[$name]][$name];
        }

        return false;
    }

    final private function checkContraints($options, $variable) {
        $Validator = new Validator();

        // These rules are required for validator
        $required_rules = $this->getRequiredRules();

        $final = array();

        /**
         * Rules could be :
         *  - type
         *  - nullable
         * 
         */
        foreach ($options as $rule => $value) {

            if ($rule === "type") {
                // Check taht type exists
                if (!$Validator->typeExists($value)) {
                    throw new RouterException(__("Please checks that contraint type $value is correctly registred for variable $variable"));
                }

                $final[$rule] = $value;
            }
            elseif ($rule === "nullable") {
                if (is_bool($value)) {
                    $final[$rule] = $value;
                }
                else {
                    throw new RouterException(__("Please checks that contraint nullable $value is a boolean for variable $variable"));
                }
            }
            elseif ($rule === "defaultValue") {
                $final[$rule] = $value;
            }


            // Add final key
            // Delete from required rules if found
            if (in_array($rule, $required_rules)) {
                unset($required_rules[array_search($rule, $required_rules)]);
            }
        }



        if (count($required_rules) > 0) {
            throw new RouterException(("All required keys following are not filled for variable $variable : " . implode(", ", $required_rules)));
        }

        return $final;
    }

    private function getRequiredRules() {
        return array(
            'type'
        );
    }

    /**
     * This function return route requested with parameters you've specified
     * @param type $route
     * @param array $params
     */
    final public function tryFillParameters($route, array $params) {

        $route = $this->getRoute($route);

        if ($route === false) {
            throw new RoutingException(__("Requested route does'nt exists."));
        }

        $uri = $route['pattern'];
        $validator = new Validator();

        foreach ($route['variables'] as $k => $v) {


            if (isset($params[$k])) {
                $value = $params[$k];


                $rule = $route['variables'][$k]['constraints']['type'];

                $return = $validator->validate($rule, $value);

                if ($return === false) {
                    $value = "typeError-" . gettype($value);
                }
            }
            elseif (isset($v['constraints']['defaultValue'])) {
                $value = $v['constraints']['defaultValue'];
            }
            elseif (isset($v['constraints']['nullable']) && $v['constraints']['nullable'] !== true) {
                $value = "undefined";
            }
            else {
                $value = "";
            }

            $uri = str_replace('{' . $k . '}', $value, $uri);
        }

        return (string) $uri;
    }

    static function __set_state(array $array) {
        $new = new self();
        $new->set_state($array);
        return $new;
    }
    
    
    public function set_state(array $array) {
        foreach ($array as $key => $value) {
            $this->$key = $value;
        }
        return $this;
    }
    
    

}
