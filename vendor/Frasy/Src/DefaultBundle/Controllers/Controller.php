<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Controllers;

use Doctrine\ORM\EntityManager;
use Frasy\ComponentsBundle\Entities\Auth\User;
use Frasy\DefaultBundle\Interfaces\Controller as ControllerInterface;
use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\ExceptionsBundle\Exceptions\NotFoundException;
use Frasy\HttpBundle\HttpKernel\HttpRequest;
use Frasy\HttpBundle\HttpKernel\HttpResponse;
use Frasy\LoadersBundle\Controllers\BundleController as BundleLoader;
use Frasy\LoadersBundle\Controllers\ConfigurationController as Configurator;
use Frasy\LoadersBundle\Controllers\DisplayController;
use Frasy\LoadersBundle\Controllers\DoctrineController;
use Frasy\LoadersBundle\Controllers\TwigController;
use Frasy\RouterBundle\Controllers\RouterController;
use Frasy\ServicesBundle\Services\FormBuilder;
use Frasy\ServicesBundle\Services\SessionService;
use ReflectionMethod;

abstract class Controller implements ControllerInterface {

    protected $publicVars = array();

    public function __construct() {
        
    }

    /**
     * This function returns the entire configuration.
     * @param boolean $secured
     * @return array
     */
    public static function getConfiguration($secured = false) {
        return Configurator::getConfig($secured);
    }

    /**
     * This function returns all registred bundle
     * @return array(Bundle)
     */
    public static function getBundles() {
        return BundleLoader::getMyBundles();
    }

    /**
     * This function return a bundle with name
     * @param string $name
     * @return Bundle
     */
    public static function getBundle($name) {
        return BundleLoader::getMyBundle($name);
    }

    /**
     * This funtion render a file
     * @param type $file
     * @param array $params
     * @return HttpResponse
     */
    public function render($file, array $params = array()) {
        $return = TwigController::displayFile($file, $params);
        return new HttpResponse($return);
    }

    /**
     * This function set a data for this controller
     * @param type $data
     * @param type $value
     * @return null
     */
    public function set($data, $value = null) {

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $this->set($k, $v);
            }
        }
        else {
            $this->publicVars[$data] = $value;
        }

        return;
    }

    /**
     * This function is a Redirection function.
     * @param array $address
     * @param seconds $time
     * @return null
     * @throws AppException
     */
    public static function redirect($address = "", $time = null) {

        if (is_array($address)) {
            if (isset($address['route'])) {
                $url = self::getUrlFromRoute($address);

                return self::redirect($url, $time);
            }

            $config = self::getConfiguration();

            $address = "/" . $config['Environment']['BaseUri'] . $address['controller'] . "/" . $address['action'] . (isset($address['params']) ? "/" . $address['params'] : "");
            $address = urlPrettify($address);
        }

        if (!empty($address) && $time == null) {
            header("Location: $address");
            exit(\__("<a href=\"$address\" title=\"Click here\">If you're not redirected, please click here.</a>"));
        }
        elseif (!empty($address)) {
            if (!headers_sent()) {
                header("Refresh:$time; url=$address");

                exit();
            }
            else {
                echo '<meta http-equiv="refresh" content="' . $time, ';url=' . $address . '" />';
            }
        }
        elseif ($address !== null) {
            self::redirect($_SERVER['REQUEST_URI']);
        }
        else {
            throw new AppException("Redirect value is not correct");
        }
    }

    /**
     * This function is able to call local action from controller. 
     * You should not rewrite this, because it's used by Frasy Dispatcher.
     * @param string $action
     * @param array $args
     * @return HttpResponse
     * @throws AppException
     * @throws NotFoundException
     */
    public final function callAction($action, array $args = array()) {

        if (strpos($action, 'Action') === false) {
            $action.="Action";
        }

        if (!method_exists($this, $action)) {
            throw new AppException("Action $action not exists in " . get_class($this));
        }

        $params = $this->getOrderedVariables($args, $this, $action);

        $reflection = new ReflectionMethod($this, $action);
        // Verifs of Required parameters
        if (count($params) < $reflection->getNumberOfRequiredParameters()) {
            throw new NotFoundException(__('Required parameters aren\'t filled'));
        }

        ob_start();
        $result = call_user_func_array(array($this, $action), $params);

        $content = ob_get_clean();


        if (!is_object($result) OR get_class($result) !== "Frasy\HttpBundle\HttpKernel\HttpResponse") {
            throw new AppException(__(get_class($this) . "::" . $action . " must return a HttpResponse"));
        }

        return $result;
    }

    public function disableCache() {
        
    }

    /**
     * This function return EntityManager from Doctrine
     * @return EntityManager
     */
    public static function getEntityManager() {
        return DoctrineController::getEm();
    }

    /**
     * @todo To do this function
     */
    public function referer() {
        
    }

    /**
     * This function is not usefull, it serve just to determine heritage.
     * @return boolean
     */
    public function heriteFromController() {
        return true;
    }

    /**
     * This function is a rapid helper for create an automatic form from Entities
     * @param type $entity
     * @return FormBuilder
     */
    final public static function createFormBuilder($entity = null, $loadType = false, array $parametersForType = array(), $formName = null) {

        $form = FormBuilder::createForm($entity, $formName);

        if ($loadType !== false && is_string($loadType)) {

            $dones = explode('::', $loadType);
            if (count($dones) !== 2) {
                throw new AppException(__("Type argument for FormBuilder is invalid !"));
            }

            if (strpos($dones[0], '\\') !== 0) {
                $class = "\\" . $dones[0];
            }
            else {
                $class = $dones[0];
            }

            if (strpos($dones[1], 'Type') !== false) {
                $action = $dones[1];
            }
            else {
                $action = $dones[1] . "Type";
            }

            if (!class_exists($class)) {
                throw new AppException("$class is not a form type.");
            }

            $formType = new $class($form);

            if (!method_exists($formType, $action)) {
                throw new AppException("$action action is not a good form type for class $class.");
            }

            $parametersForType = self::getOrderedVariables($parametersForType, $formType, $action);
            $r = call_user_func_array(array($formType, $action), $parametersForType);

            if (get_class($r) !== "Frasy\ServicesBundle\Services\FormBuilder") {
                throw new AppException("$action action must return the original form builder for class $class.");
            }
        }
        elseif ($loadType !== false) {
            throw new AppException("Type argument was not recognized : Unexpected type '" . gettype($loadType) . "'");
        }

        return $form;
    }

    /**
     * This function checks order of declared variables in a method and filter them
     * @param type $variables
     * @param type $controller
     * @param type $action
     * @return type
     */
    public static function getOrderedVariables($variables, $controller, $action) {

        // This method make an auto reorder of variables like in called controller method
        $r = new ReflectionMethod($controller, $action);
        $p = $r->getParameters();

        $order_function = array();

        foreach ($p as $k => $t) {
            $order_function[$k] = $t->name;
        }

        $final_order = array();
        $sautes = array();

        foreach ($order_function as $k => $v) {
            if (isset($variables[$v])) {

                $final_order[$k] = $variables[$v];
                unset($variables[$v]);
                continue;
            }
            else {
                $sautes[$k] = $v;
                //$final_order[$k] = null;
            }
        }


        // last variables
        if (count($sautes) > 0) {
            foreach ($sautes as $k => $v) {

                foreach ($variables as $k2 => $v2) {

                    $final_order[$k] = $v2;
                    unset($variables[$k2]);
                    break;
                }
            }
        }

        ksort($final_order);
        return $final_order;
    }

    /**
     * This function add header to print out before print content
     * If $header is false, nothing will be printed out
     * @param type $header
     */
    public static function addHeader($header, $identifier = null) {
        DisplayController::addHeader($header, $identifier);
    }

    /**
     * This function will print headers
     * @param boolean $force
     */
    public static function printHeaders($force = false) {
        DisplayController::printHeaders($force);
    }

    /**
     * This function return request method
     * @param type $input
     * @return boolean
     */
    public function methodIs($input) {
        $input = strtoupper($input);
        if ($_SERVER['REQUEST_METHOD'] == $input) {
            return true;
        }
        else {
            // SPECIAL METHOD
            if ($input === "AJAX" && array_key_exists('HTTP_X_REQUESTED_WITH', $_SERVER) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                return true;
            }
        }

        return false;
    }

    /**
     * This function return all variables contained in POST, GET, REQUEST
     * @param array $arg
     * @return HttpRequest
     */
    public function getRequest($arg = null) {
        return new HttpRequest($arg);
    }

    /**
     * This function return a string after find the long url.
     * @param array $address
     *  -> array('route' => 'routeName', 'arguments' => array('id' => 55, ...))
     * @return string
     */
    public static function getUrlFromRoute($address, array $arguments = array()) {
        if (is_string($address)) {
            $address = array(
                'route' => $address,
                'arguments' => $arguments
            );
        }

        $route = $address['route'];

        $config = self::getConfiguration();


        $dones = CONSOLE === true ? false : $config['Routing']->getRoute($route);

        if ($dones === false) {
            return $route . "#routeNotFound";
        }

        $arguments = (isset($address['arguments']) && is_array($address['arguments'])) ? $address['arguments'] : array();
        $siteUri = $config['Environment']['SiteUri'];
        $baseUri = $config['Environment']['BaseUri'];

        $routeUrl = $config['Routing']->tryFillParameters($route, $arguments);

        $url = $siteUri . $baseUri . $routeUrl;

        $url = urlPrettify($url);

        return $url;
    }

    /**
     * This function return if the current user is able to access to resource.
     * @param string $role
     * @return boolean
     */
    public static function is_granted($role) {
        $role = strtoupper($role);

        $logged = SessionService::get("App.Auth.Logged");

        if ($role === "ROLE_ANONYMOUS" && $logged !== true) {
            return true;
        }
        elseif ($logged !== true) {
            return false;
        }
        /* @var $currentUser User */
        $currentUser = SessionService::get("App.Auth.Current");


        if ($currentUser === null) {
            return false;
        }


        if ($role === "ROLE_ANONYMOUS") {
            return false;
        }
        elseif (!method_exists($currentUser, "hasRole") || !$currentUser->hasRole($role)) {
            return false;
        }
        return true;
    }

    /**
     * This function returns the current user
     * @return boolean|\Frasy\ComponentsBundle\Entities\Auth\User;
     */
    public static function getCurrentUser() {
        if (self::is_granted("ROLE_ANONYMOUS")) { // so not logged in
            return false;
        }

        $currentUser = SessionService::get("App.Auth.Current");

        if ($currentUser === null) {
            return false;
        }

        return $currentUser;
    }

    /**
     * This function return all parameters in url, which are the differents url segments.
     * @return array
     */
    public function getAllParameters() {
        return RouterController::getAllParametersData();
    }

    /**
     * This function return the current url
     * @return string
     */
    public function getCurrentUrl() {
        return RouterController::getCurrentUrlData();
    }

    /**
     * This function is usefull for redirect on an internal route fastly
     * @param type $route
     * @param array $arg
     * @param second $time
     * @param string $suffix
     */
    public static function redirectRoute($route, array $arg = array(), $time = 0, $suffix = "") {
        if (!empty($suffix)) {
            $suffix = "/" . $suffix;
        }

        self::redirect(self::getUrlFromRoute(array('route' => $route, 'arguments' => $arg)) . $suffix, $time);
    }

    /**
     * This function return the current route name
     * @return string
     */
    public static function getCurrentRoute() {
        return RouterController::getCurrentRoute();
    }

    /**
     * This function prevent PHP errors. 
     * You can override it.
     * @param type $name
     * @param type $arguments
     * @throws AppException
     */
    function __call($name, $arguments) {
        throw new AppException(__("The function $name doesn't exist in controller " . get_called_class())."\n");
    }

    /**
     * This function prevent PHP errors. 
     * You can override it.
     * @param type $name
     * @param type $arguments
     * @throws AppException
     */
    public static function __callStatic($name, $arguments) {
        throw new AppException(__("The function $name doesn't exist in controller " . get_called_class())."\n");
    }

}
