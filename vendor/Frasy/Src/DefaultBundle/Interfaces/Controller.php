<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\DefaultBundle\Interfaces;

interface Controller {
   
    public static function getConfiguration();
    
    public static function getBundles();
    
    public static function getBundle($name);
    
    public function render($file, array $params=null);
    
    public function set($data, $value=null);
    
    public static function redirect($address, $time=null);
    
    public function referer();
    
    public function disableCache();
    
    
    public function callAction($action, array $args = array());
    
    public static function getEntityManager();
    
    public static function getCurrentUser();
    
}


