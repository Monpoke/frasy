<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * This file is an interface for Bundle loader.
 */

namespace Frasy\DefaultBundle\Interfaces;
use Frasy\ServicesBundle\Services\FormBuilder;
interface FormType  {
    
    public function __construct(FormBuilder $Builder);
    
    public function getBuilder();
}