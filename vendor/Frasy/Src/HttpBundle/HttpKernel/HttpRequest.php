<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\HttpBundle\HttpKernel;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ServicesBundle\Services\SecurityService;

class HttpRequest extends Controller {

    protected $postData = null;
    protected $getData = null;
    protected $requestData = null;

    public function __construct($arg = null) {

        $this->postData = (isset($_POST)) ? $_POST : array();
        $this->getData = (isset($_GET)) ? $_GET : array();

        $this->requestData = (isset($_REQUEST)) ? $_REQUEST : array();
    }

    public function getPostData(array $autoProtect=array('trim', 'htmlspecialchars')) {
        return $this->protectAllData($autoProtect, $this->postData);
    }

    public function getGetData(array $autoProtect=array('trim', 'htmlspecialchars')) {
        return $this->protectAllData($autoProtect, $this->getData);
    }

    public function getRequestData(array $autoProtect=array('trim', 'htmlspecialchars')) {
        return $this->protectAllData($autoProtect, $this->requestData);
    }

    public function getAllData($autoProtect=array('trim', 'htmlspecialchars')) {
        $array = array_merge(
            $this->getData,
            $this->postData,
            $this->requestData
        );
        
        if(is_array($autoProtect)){
            
            $array = $this->protectAllData($autoProtect, $array);                 
            
        }
        
        return $array;
    }
    
   public function protectAllData($autoProtect, $array) {
        foreach($array as $k=>$v){
            if(is_array($v)){
                $array[$k] = $this->protectAllData($autoProtect, $v);
            } else {
                $array[$k] = SecurityService::secureData($autoProtect, $v);
            }
                
        }
        
        return $array;
    }

}

