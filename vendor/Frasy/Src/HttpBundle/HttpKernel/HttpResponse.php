<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */


namespace Frasy\HttpBundle\HttpKernel;
use Frasy\DefaultBundle\Controllers\Controller;

class HttpResponse extends Controller {
    
    protected $content = "Here";
    
    public function __construct($str) {
       $this->content = $str;
    }
    
    public function display($echoValue=true){
        if($echoValue===true)
            echo $this->content;
        
        return $this->content;
    }
    
    
    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }


}

