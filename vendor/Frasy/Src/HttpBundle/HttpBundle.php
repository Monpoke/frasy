<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\HttpBundle;
use Frasy\DefaultBundle\Controllers\Bundle;

class HttpBundle extends Bundle {
    
    
    public function getDir() {
        return __DIR__;
      
    }
    
    public function getNamespace() {
        return __NAMESPACE__;
    }
    
    public function getName() {
        return "FrasyHttp";
    }
}


