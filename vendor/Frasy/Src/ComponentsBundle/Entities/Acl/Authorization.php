<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Entities\Acl;

use Frasy\ComponentsBundle\Entities\Acl\AuthorizationInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\MappedSuperclass 
 */
abstract class Authorization implements AuthorizationInterface {

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    protected $name;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }


    
}

