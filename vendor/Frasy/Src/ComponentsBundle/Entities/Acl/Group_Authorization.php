<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Entities\Acl;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass 
 */
abstract class Group_Authorization {

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** @ORM\ManyToOne(targetEntity="\Frasy\ComponentsBundle\Entities\Auth\Group", inversedBy="authorizations") */
    protected $group;
    
    /** @ORM\ManyToOne(targetEntity="\Frasy\ComponentsBundle\Entities\Acl\Authorization") */
    protected $authorization;
    
    /** @ORM\Column(type="boolean") */
    protected $allowed = true;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getGroup() {
        return $this->group;
    }

    public function setGroup($group) {
        $this->group = $group;
    }

    public function getAuthorization() {
        return $this->authorization;
    }

    public function setAuthorization($authorization) {
        $this->authorization = $authorization;
    }


    public function getAllowed() {
        return $this->allowed;
    }

    public function isAllowed(){
        return $this->getAllowed();
    }
    
    public function setAllowed($allowed) {
        $this->allowed = $allowed;
    }


}

