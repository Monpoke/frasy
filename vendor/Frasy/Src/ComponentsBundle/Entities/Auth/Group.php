<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Entities\Auth;

use Frasy\ComponentsBundle\Entities\Auth\GroupInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass 
 */
abstract class Group implements GroupInterface {

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $priorityOrder;

    
    /**
     * @ORM\OneToMany(targetEntity="\Frasy\ComponentsBundle\Entities\Acl\Group_Authorization", mappedBy="group")
     */
    protected $authorizations;
    
     /**
     * @ORM\ManyToOne(targetEntity="\Frasy\ComponentsBundle\Entities\Acl\Group")
     */
    protected $parent;

    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getAuthorizations() {
        return $this->authorizations;
    }

    
    public function getPriorityOrder() {
        return $this->priorityOrder;
    }

    public function setPriorityOrder($priorityOrder) {
        $this->priorityOrder = $priorityOrder;
    }

    public function isAllowedFor($name){
        $permissions = $this->getAuthorizations();

        foreach ($permissions as $permission) {
            $permissionAuthorization = $permission->getAuthorization();
            if ($permissionAuthorization->getName() !== $name) {
                continue;
            }

            // FOUND
            return $permission->isAllowed();
        }
        
        // Search in parent
        if($this->getParent()!==null){
            return $this->getParent()->isAllowedFor($name);
        }
        
        return false;
    }

    
    
}

