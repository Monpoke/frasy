<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Entities\Auth;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass 
 */
abstract class Group_User {

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** @ORM\ManyToOne(targetEntity="\Frasy\ComponentsBundle\Entities\Auth\Group") */
    protected $group;
    
    /** @ORM\ManyToOne(targetEntity="\Frasy\ComponentsBundle\Entities\Auth\User", inversedBy="groups") */
    protected $user;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @return \Frasy\ComponentsBundle\Entities\Auth\Group
     */
    public function getGroup() {
        return $this->group;
    }

    public function setGroup($group) {
        $this->group = $group;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }


}

