<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\DefaultBundle\Defaults;     
use Doctrine\ORM AS ORM;
use Doctrine\ORM\EntityRepository;

class BaseUserRepository extends EntityRepository {
    
}

