<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Entities\Auth;

use Frasy\ComponentsBundle\Entities\Auth\UserInterface;
use Doctrine\ORM\Mapping AS ORM;
use Frasy\HelpersBundle\Helpers\FormatHelper;
use Frasy\ServicesBundle\Services\SecurityService;
use Doctrine\Common\Collections\ArrayCollection;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ComponentsBundle\Auth\RoleManager;

/**
 * @ORM\MappedSuperclass 
 */
abstract class User implements UserInterface {

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(name="usernameCanonical", type="string", length=255, unique=true)
     */
    protected $usernameCanonical;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = false;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     *
     * @var string 
     */
    protected $plainPassword;

    /**
     * @ORM\Column(name="lastLogin", type="datetime", nullable=true)
     */
    protected $lastLogin = null;

    /**
     * @ORM\Column(name="registerDate", type="datetime")
     */
    protected $registerDate;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles = array();

    /**
     * @ORM\Column(name="fb_uid", type="string", length=255, nullable=true)
     */
    protected $fb_uid;

    

    /**
     * DEFAULTS
     */
    public function __construct() {

        $this->setRegisterDate(new \DateTime());

        $randomHelper = new \Frasy\HelpersBundle\Helpers\RandomHelper();
        $this->setSalt($randomHelper->randomString(10));
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
        $this->setUsernameCanonical($username);
    }

    public function getUsernameCanonical() {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical($usernameCanonical) {
        $usernameCanonical = FormatHelper::canonicalForm($usernameCanonical);
        $this->usernameCanonical = $usernameCanonical;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
        $this->setEmailCanonical($email);
    }

    public function getEmailCanonical() {
        return $this->emailCanonical;
    }

    public function setEmailCanonical($emailCanonical) {
        $this->emailCanonical = $emailCanonical;
    }

    public function getEnabled() {
        return $this->enabled;
    }

    public function setEnabled($enabled) {
        $this->enabled = $enabled;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password, $force = false) {
        if (empty($password) && $force === false) {
            return;
        }

        $this->setPlainPassword($password);

        $password = $this->cryptPassword($password);

        $this->password = $password;
    }

    protected function cryptPassword($password) {
        return SecurityService::hashData($this->getSalt() . $password);
    }

    public function passwordsAreEquals($plain = null) {
        $return = false;

        if ($plain === null) {
            $plain = $this->getPlainPassword();
        }

        if ($plain !== null) {
            $current = $this->getPassword();
            $crypted = $this->cryptPassword($plain);

            if ($current === $crypted) {
                $return = true;
            }
        }

        $this->plainPassword = "";
        return $return;
    }

    public function getPlainPassword() {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword) {
        $this->plainPassword = $plainPassword;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getLastLogin() {
        return $this->lastLogin;
    }

    public function setLastLogin($lastLogin) {

        $this->lastLogin = $lastLogin;
    }

    public function getConfirmationToken() {
        return $this->confirmationToken;
    }

    public function setConfirmationToken($confirmationToken) {
        $this->confirmationToken = $confirmationToken;
    }

    public function getRegisterDate() {
        return $this->registerDate;
    }

    public function setRegisterDate($registerDate) {
        $this->registerDate = $registerDate;
    }

    public function getPasswordRequestedAt() {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt($passwordRequestedAt) {
        $this->passwordRequestedAt = $passwordRequestedAt;
    }

    public function getLocked() {
        return $this->locked;
    }

    public function setLocked($locked) {
        $this->locked = $locked;
    }

    public function getExpired() {
        return $this->expired;
    }

    public function setExpired($expired) {
        $this->expired = $expired;
    }

    public function getExpiresAt() {
        return $this->expiresAt;
    }

    public function setExpiresAt($expiresAt) {
        $this->expiresAt = $expiresAt;
    }

    /**
     * This function returns all roles that current user own
     * @return array
     */
    public function getRoles() {
        return $this->roles;
    }

    /**
     * This function adds a role for current user
     * @param type $role
     * @return boolean
     */
    public function addRole($role) {
        $role = strtoupper($role);
        $Config = Controller::getConfiguration();
        if (!isset($Config['Parameters']['Auth']['Roles'][$role])) {
            return false;
        }

        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        $this->roles = array_values($this->roles);

        return true;
    }

    /**
     * This function removes a role for current user
     * @param type $role
     * @todo Maybe there are an issue here
     */
    public function removeRole($role) {
        $role = strtoupper($role);

        if ($this->hasRole($role)) {
            $k = array_keys($this->roles, $role);
            unset($this->roles[$k[0]]);
        }

        $this->roles = array_values($this->roles);
        return true;
    }

    /**
     * This function checks if user has role ROLE
     * @param type $role
     * @return boolean
     */
    public function hasRole($role) {
        $role = strtoupper($role);
        if (in_array($role, (array)$this->roles)) {
            return true;
        }


        // Else maybe roles could be a subroles
        foreach ((array)$this->roles as $k => $v) {
            if (RoleManager::roleHaveParent($v, $role) === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * This function checks if user has roles ROLEA, ROLEB, ROLEC
     * @param array $roles
     * @return boolean
     */
    public function hasRoles(array $roles, $allRoles = true) {
        foreach ($roles as $role) {
            if (!$this->hasRole($role) && $allRoles === true) {
                return false;
            } elseif ($allRoles === false && $this->hasRole($role)) {
                return true;
            }
        }

        if ($allRoles === true) {
            return true;
        } else {
            return false;
        }
    }

    public function getCredentialsExpired() {
        return $this->credentialsExpired;
    }

    public function setCredentialsExpired($credentialsExpired) {
        $this->credentialsExpired = $credentialsExpired;
    }

    public function getCredentialsExpiredAt() {
        return $this->credentialsExpiredAt;
    }

    public function setCredentialsExpiredAt($credentialsExpiredAt) {
        $this->credentialsExpiredAt = $credentialsExpiredAt;
    }

    /**
     * Own methods
     */
    public function isAccountNonExpired() {
        if (true === $this->expired) {
            return false;
        }

        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    public function isAccountNonLocked() {
        return !$this->locked;
    }

    public function isCredentialsNonExpired() {
        if (true === $this->credentialsExpired) {
            return false;
        }

        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    public function isCredentialsExpired() {
        return !$this->isCredentialsNonExpired();
    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function isExpired() {
        return !$this->isAccountNonExpired();
    }

    public function isLocked() {
        return !$this->isAccountNonLocked();
    }

    public function getFbUid() {
        return $this->fb_uid;
    }

    public function setFbUid($fbuid) {
        $this->fb_uid = $fbuid;
    }

    public function getAuthorizations() {
        return $this->authorizations;
    }

    public function checkPermission($name) {
        // first, check user permission
        $permissions = $this->getAuthorizations();

        /* @var $permission \Frasy\ComponentsBundle\Entities\Acl\User_Authorization */
        foreach ($permissions as $permission) {
            $permissionAuthorization = $permission->getAuthorization();
            if ($permissionAuthorization->getName() !== $name) {
                continue;
            }

            // FOUND
            return $permission->isAllowed();
        } 
        
        // NOT FOUND IN USER PERMISSIONS, SO WE'LL FIND IT IN GROUPS PERMISSIONS
        $groups = $this->getGroups();
        $lastPriorityOrder = null;
        $returnPermission = null;
        
        /* @var $group_user Group_User */
        foreach ($groups as $group_user) {
            $group = $group_user->getGroup();
            
            if ($lastPriorityOrder !== null && $group->getPriorityOrder() <= $lastPriorityOrder) {
                continue;
            }

            $lastPriorityOrder=$group->getPriorityOrder();
            $permissions = $group->getAuthorizations();
            
            
            
            foreach ($permissions as $permission) {
                $permissionAuthorization = $permission->getAuthorization();
                if ($permissionAuthorization->getName() !== $name) {
                    continue;
                }
                
                // FOUND
                $returnPermission = $permission->isAllowed();
            }
        }
        
        if($returnPermission!==null){
            return $returnPermission;
        }
        

        // RETURN false BY DEFAULT
        return false;
    }

    /**
     * 
     * @return array
     */
    public function getGroups() {
        return $this->groups;
    }

    public function addGroup($group) {
        return $this->groups[] = $group;
    }
}

