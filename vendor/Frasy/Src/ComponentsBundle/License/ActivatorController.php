<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\License;

use Frasy\DefaultBundle\Controllers\Component;
use Frasy\ExceptionsBundle\Exceptions\ConfigurationException;
use Frasy\ServicesBundle\Services\SecurityService;
use Frasy\ComponentsBundle\License\ManagerController;

class ActivatorController extends Component {

    protected $params;
    protected $baseApi = "http://apiclients.plumedor.fr/";
    protected $manager;

    public function __construct($params) {
        parent::__construct();
        $this->params = $params;

        $this->manager = new ManagerController;
    }

    public function active() {
        if (empty($this->params['commands'][1]))
            return "Please provide the license to activate.";

        $license = $this->params['commands'][1];
        $dir = APP . DS . "Config" . DS . "Licenses";

        if (!is_file($dir . DS . $license)) {
            return "The license $license not exists.";
        }

        $contentLicense = file_get_contents($dir . DS . $license);

        $licenseCryptee = explode('|', $contentLicense);
        if (count($licenseCryptee) !== 3)
            return utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4="));

        $license = $licenseCryptee[0] . $licenseCryptee[2];
        $infos = explode('.', $licenseCryptee[1]);

        if (count($infos) !== 3)
            return utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4="));

        if (empty($infos[0]) || empty($infos[1]) || empty($infos[2]))
            return utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4="));

        $this->webRef = $infos[0];
        $this->webKey1 = $infos[1];
        $this->webKey2 = $infos[2];
        $this->license = $license;


        $resultApi = $this->getApi("license/active", array(
            'license' => $contentLicense,
        ));

        if ($resultApi['status'] !== '200')
            return $resultApi['data'];
        elseif(!isset($resultApi['license'])){
            return "Error coming from API. Please contact support.";
        }
        
        if (!file_put_contents(FRASY_LICENSE_LOCATION, $resultApi['license'])) {
            return "Cannot write the activated license. Please contact support.";
        }

        return "License activated!";
    }

    public function downloadLicense() {
        $params = $this->params;
        if (empty($params['commands'][1]))
            return "Please provide a reference for your license.";

        $resultApi = $this->getApi("license/generate", array(
            'refId' => $params['commands'][1],
        ));

        if (is_array($resultApi['data'])) {
            $license = $resultApi['data']['license'];

            if (!file_put_contents(FRASY_LICENSE_LOCATION, $license)) {
                return "Cannot write the license. Please download and install it.";
            }

            return "License installed. Please activate it.";
        } else {
            return "Server has returned : " . $resultApi['data'];
        }
    }

    public function getApi($api = "/", array $data = array(), $login = true) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->baseApi . $api);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        if ($login === true)
            $login = array(
                'webRef' => $this->webRef,
                'webKey1' => $this->webKey1,
                'webKey2' => $this->webKey2,
            );
        else {
            $login = array();
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data + $login);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl);

        return unserialize($result);
    }

    public function removeLicense() {
         if (empty($this->params['commands'][1]))
            return "Please provide the license to remove.";

        $license = $this->params['commands'][1];
        $dir = APP . DS . "Config" . DS . "Licenses";

        if (!is_file($dir . DS . $license)) {
            return "The license $license not exists.";
        }
        
        if(!unlink($dir . DS . $license)){
            return "License cannot be deleted.";
        }
        
        return "License deleted.";
    }

}