<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\License;

use Frasy\DefaultBundle\Controllers\Component;
use Frasy\ExceptionsBundle\Exceptions\LicenseException;
use Frasy\ServicesBundle\Services\SecurityService;

class ManagerController extends Component {

    protected $licenseFile;
    protected $webKey1 = "ff019a5748a52b5641624af88a54a2f0e46a9fb5";
    protected $webKey2 = "4de30ec9b66f41c5f0a7c5071e8245e0cb2f084f";
    protected $webRef = 1;
    protected $license = "";

    public function __construct() {
        parent::__construct();
        $this->licenseFile = APP . "/Config/Licenses/DefaultLicense";
        if(!defined('FRASY_LICENSE_LOCATION'))
            define('FRASY_LICENSE_LOCATION', $this->licenseFile);
        
        if(!CONSOLE)
            $this->checkLicense();
    }

    public function checkLicense() {

        if (!is_file($this->licenseFile)) {
            $throw = new LicenseException("Erreur dans la configuration du site. <br /><b>Veuillez vérifier la license d'utilisation.</b><br />Elle doit être située sous <i>" . APP . "/Config/Licences/DefaultLicense</i>", 505);
            throw $throw;
        }

        
        // Ligne temporaire, car la license est déjà cryptée ^^
        /*$crypte = SecurityService::encrypt($license, $this->getEncryptionKey());
        $nb = strlen($crypte);
        $pl1 = (int) $nb / 3;
        $allKey = "|" . $this->webRef . "." . $this->webKey1 . "." . $this->webKey2 . "|";

        $licenseCryptee = substr($crypte, 0, $pl1) . $allKey . substr($crypte, $pl1);
        echo $licenseCryptee;
        exit("<br /><br />t");//*/

        $licenseCryptee = file_get_contents($this->licenseFile);
        $this->getDonnees($licenseCryptee);

        $uncypted = SecurityService::decrypt($this->license, $this->getEncryptionKey());
        if ($uncypted === false) {
            throw new ConfigurationException(utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4=")), 505);
        }


        //exit(var_dump($uncypted));
        $this->checkField($uncypted['detailsLicense'], "activated");
    }

    public function getEncryptionKey() {
        return substr(sha1($this->webKey1 . $this->webKey2 . $this->webRef), 0, 8);
    }

    public function getDonnees($licenseCryptee) {
        $licenseCryptee = explode('|', $licenseCryptee);
        if (count($licenseCryptee) !== 3)
        //La licence installée est invalide.<br />Veuillez la réinstaller <i>ou</i> contactez le support.
            throw new LicenseException(utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4=")), 505);

        $license = $licenseCryptee[0] . $licenseCryptee[2];
        $infos = explode('.', $licenseCryptee[1]);

        if (count($infos) !== 3)
            throw new LicenseException(utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgZXN0IGludmFsaWRlLjxiciAvPlZldWlsbGV6IGxhIHLpaW5zdGFsbGVyIDxpPm91PC9pPiBjb250YWN0ZXogbGUgc3VwcG9ydC4=")), 505);

        $this->webRef = $infos[0];
        $this->webKey1 = $infos[1];
        $this->webKey2 = $infos[2];
        $this->license = $license;
    }

    private function checkField($uncypted, $in) {
        if (isset($uncypted[$in])) {
            $value = $uncypted[$in];
            if ($in === "activated" && $value !== true) {
                throw new LicenseException(utf8_encode(base64_decode("TGEgbGljZW5jZSBpbnN0YWxs6WUgbidlc3QgcGFzIGFjdGl26WUuPGJyIC8+IEFjdGl2ZXotbGEgZW4gY29uc29sZSBhdmVjIGxhIGNvbW1hbmRlIDxpPmxpY2Vuc2U6bWFuYWdlOmFjdGl2ZSBEZWZhdWx0TGljZW5zZTwvaT4=")), 505);
            }
        }
    }

}