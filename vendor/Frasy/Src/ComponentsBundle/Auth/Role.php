<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ComponentsBundle\Auth;

use Frasy\ComponentsBundle\Auth\RoleInterface as RoleInterface;

abstract class Role implements RoleInterface {

    
    
}

