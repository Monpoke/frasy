<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ComponentsBundle\Auth;
use Frasy\DefaultBundle\Controllers\TwigExtension;
use Frasy\ServicesBundle\Services\SessionService;
use Frasy\DefaultBundle\Controllers\Controller;


class AuthTwigFunctions extends TwigExtension {

    protected $htmlCode = "";
    
    public function getName() {
        return 'frasyauthtwigfunctions';
    }

    public function getGlobals() {
        
         return array(
            'user' => 'k',
            
        );
        
    }
    
    public function getFunctions() {
        return array(
            'is_granted' => new \Twig_Function_Method($this, 'is_granted', array('is_safe' => array('html'))),
            
        );
    }
    
    
    public function is_granted($role){
        
        return RoleManager::is_granted($role);
        
    }
    
}

