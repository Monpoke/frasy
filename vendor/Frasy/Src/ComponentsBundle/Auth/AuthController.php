<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

  

 */


namespace Frasy\ComponentsBundle\Auth;

use Frasy\ComponentsBundle\Entities\Auth\User;
use Frasy\DefaultBundle\Controllers\Component;
use Frasy\ServicesBundle\Services\SessionService;


class AuthController extends Component {
    
    public function __construct() {
        parent::__construct();
        
        
    }
    /**
     * This function try authentificate an user with username and salted password
     * @param \Frasy\ComponentsBundle\Entities\Auth\User $user
     * @return boolean
     */
    public function tryLoginAs($user){
        
        // Get userClass
        $userClass = get_class($user);
        
        // Get Repository
        $repository = $this->getEntityManager()->getRepository($userClass);
        
        /* @var $result User */
        $result = $repository->findOneBy(array('username' => $user->getUsername()));
        
        if($result !== null){
            
            if(!$result->isEnabled()){
                SessionService::addFlash("ErrorMessage", __("This account is not enabled!"));
                return false;
                
            }
            
            $passwordsMatch = $result->passwordsAreEquals($user->getPlainPassword());
            
            if($passwordsMatch === true){
                $this->updateLastLogin($result);
                return $this->loginAs($result);
            }
        }
        
        return false;
    }

    /**
     * This function authenticate an user without control access allowed.
     * Security warning if your code is exposed.
     * @param \Frasy\ComponentsBundle\Entities\Auth\User $user
     * @return boolean
     */
    public function loginAs($user) {
        
        if(SessionService::set("App.Auth.Current", $user) && SessionService::set("App.Auth.Logged", true))
            return true;
        else
            return false;
    }
    
    /**
     * 
     */
    public function logout() {
        SessionService::delete("App.Auth.Current");
        SessionService::set("App.Auth.Logged", false);
        return true;
    }
    
    /**
     * 
     * @param \Frasy\ComponentsBundle\Entities\Auth\User $user
     */
    protected function updateLastLogin($user) {
        if(!($user instanceof User)){
            return false;
        }
        
        $user->setLastLogin(new \DateTime());
        $em = $this->getEntityManager();
        $em->merge($user);
        $em->flush();
    }
}