<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0



 */

namespace Frasy\ComponentsBundle\Auth;

use Frasy\DefaultBundle\Controllers\Component;
use Frasy\ComponentsBundle\Entities\Auth\User;
use Frasy\ServicesBundle\Services\SessionService;

class UserAccess extends Component {

    public $current;

    public function __construct($name = null, User $user = null) {
        parent::__construct();

        if ($user === null) {
            $this->current = SessionService::get("App.Auth.Current");
        } else {
            $this->$name = $user;
        }
    }

    public static function checkRouteAccessEvent() {
        $config = self::getConfiguration();

        $AuthConfig = isset($config['Parameters']['Auth']) ? $config['Parameters']['Auth'] : false;

        if ($AuthConfig === false) {
            return false;
        }
        // Check route
        $currentRoute = self::getCurrentRoute();


        $routesAllowed = isset($AuthConfig['allowedRoutes']) ? $AuthConfig['allowedRoutes'] : array();

        // Check for non logged
        if (self::is_granted("ROLE_ANONYMOUS") && isset($routesAllowed['ROLE_ANONYMOUS'])) {
            $loginRoute = isset($AuthConfig['loginRoute']) ? $AuthConfig['loginRoute'] : false;

            // Check if current page is allowed for anonymous users
            if (!in_array($currentRoute, $routesAllowed['ROLE_ANONYMOUS']+array($loginRoute)) && $currentRoute !== $loginRoute) {
                if ($loginRoute === false) {
                    throw new \Frasy\ExceptionsBundle\Exceptions\ConfigurationException(__("Please set the login route for Auth Component in Parameters file."));
                } else {
                   $return_uri = (urlencode($_SERVER['REQUEST_URI']));
                   self::redirectRoute($loginRoute, array(), 0, "?return_uri=".$return_uri);
                }
            }
        }
    }

}
