<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ComponentsBundle\Auth;

use Frasy\DefaultBundle\Controllers\Controller;

class RoleManager extends Controller {

    protected static $cache = array();

    /**
     * This function checks if a role own a subrole
     * @param string $role
     * @return boolean
     */
    public static function roleHaveParent($role, $wantedRole) {
        $config = self::getConfiguration();
        $roles = $config['Parameters']['Auth']['Roles'];

        if (!isset($roles[$role]))
            return false;

        $parents = self::getParentsRole($roles[$role]);
        // Make correspondances
        $final = self::giveCorrespondances(self::findCorrespondances($roles), $parents);
        
        if (!in_array($wantedRole, $final))
            return false;
        
        return true;
    }

    public static function getParentsRole($object) {
        $r = $object;

        $final = array();
        $final[] = get_class($object);
        while (true) {
            $r = get_parent_class($r);

            if ($r === false) {
                break;
            }
            $final[] = $r;
        }

        krsort($final);
        $final = array_values($final);


        return $final;
    }

    /**
     * This function returns only correspondances between namespace and roles name
     * @param array $correspondancesClass
     * @param array $parents
     * @return array
     */
    public static function giveCorrespondances($correspondancesClass, $parents) {
        $return = array();

        foreach ($parents as $name => $v) {
            if (isset($correspondancesClass[$v]))
                $return[] = $correspondancesClass[$v];
        }

        return $return;
    }

    public static function findCorrespondances($roles) {
        $correspondancesClass = array();
        foreach ($roles as $name => $object) {
            $correspondancesClass[get_class($object)] = $name;
        }

        return $correspondancesClass;
    }
    
    
}