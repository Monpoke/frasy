<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\HelpersBundle\Forms;

use Frasy\ServicesBundle\Services\SecurityService;
use Frasy\DefaultBundle\Controllers\Controller;

class HtmlForm {

    protected $form;
    protected $configuration;
    protected $FormConfiguration;
    protected $shownConstitutes = array();
    protected $stateForm = false;
    protected $action = null;
    protected $nameForm = "";
    protected $binded = false;
    private $fields_html5_attributes = array(
    );
    private $crsf = false;

    public function __construct(array $form, $action = null, $FormName = null, $binded = false) {
        $this->setForm($form);
        $this->setConfig();
        $this->binded = $binded;

        if ($action !== null) {
            $this->setAction($action);
        }

        $this->nameForm = $FormName;
    }

    /**
     * This function is used by Frasy to set a form.
     * @param array $form
     */
    private function setForm($form) {
        $this->form = $form;
    }

    /**
     * This function set the action attribute of current form.
     * @param string|array $action
     * @return \Frasy\HelpersBundle\Forms\HtmlForm
     */
    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    /**
     * This function returns true if this form was submited
     * @return boolean
     */
    public function isChecked() {
        return $this->binded;
    }

    /**
     * This function get the config for form helper.
     */
    public function setConfig() {
        $this->configuration = Controller::getConfiguration();
        if (isset($this->configuration['Parameters']['Helpers']['Form'])) {
            $this->FormConfiguration = &$this->configuration['Parameters']['Helpers']['Form'];
        }
        else {
            $this->FormConfiguration = array();
        }
    }

    /**
     * Returns the current form.
     * @return array
     */
    public function getForm() {
        return $this->form;
    }

    /**
     * General function for input
     * @param array $attributes
     * @return string
     */
    public function input($attributes) {
        return '<input  ' . $attributes . '/>';
    }

    /**
     * For a field type text
     * @param type $options
     * @return type
     */
    public function field_text($options) {
        $attributes = $this->constituteFieldArray('text', $options);
        return $this->input($attributes);
    }

    /**
     * For a field type email
     * @param type $options
     * @return type
     */
    public function field_email($options) {
        $attributes = $this->constituteFieldArray('email', $options);
        return $this->input($attributes);
    }

    /**
     * For a field type password
     * @param type $options
     * @return type
     */
    public function field_password($options) {
        $attributes = $this->constituteFieldArray('password', $options);
        return $this->input($attributes);
    }

    /**
     * For a field type url
     * @param type $options
     * @return type
     */
    public function field_url($options) {
        $attributes = $this->constituteFieldArray('url', $options);
        return $this->input($attributes);
    }

    /**
     * For a field type telephone
     * @param type $options
     * @return type
     */
    public function field_telephone($options) {
        $attributes = $this->constituteFieldArray('tel', $options);
        return $this->input($attributes);
    }

    /**
     * For a field type textarea
     * @param type $options
     * @return type
     */
    public function field_textarea($options) {
        $attributes = $this->constituteFieldArray("textarea", $options);
        $defaultValue = $this->getValueFrom($options);
        return "<textarea $attributes>$defaultValue</textarea>";
    }

    /**
     * For a field type hidden
     * @param type $options
     * @return type
     */
    public function field_hidden($options) {
        $name = $this->extractName($options);
        $dontUse = array();

        if ($name === "_crsf") {
            $dontUse[] = "value";
        }


        $value = $this->getValueFrom($options, $dontUse);
        return '<input type="hidden" name="' . $name . '" value="' . $value . '" />';
    }

    /**
     * @todo Error_msg
     * @param type $fieldName
     */
    public function error_msg($fieldName) {
        
    }

    /**
     * For a field type password
     * @param type $options
     * @return type
     */
    public function label_form($options, $append = ' :') {
        $attributes = $this->analyseAttributes(array(), $options, "label_attr");
        $name = $this->extractName($options);

        $text = (isset($attributes['text'])) ? $attributes['text'] : $name;


        if (!isset($attributes['title']) && (!isset($options['dones']['options']['label_attr']['noTitle']) OR $options['dones']['options']['label_attr']['noTitle'] === false)) {
            $attributes['title'] = $text;
        }
        
        $attributes = $this->removeBadAttributes(array('noTitle', 'text'), $attributes);

        $attributes2 = $this->implodeAttributes($attributes);

        $for = $this->getNameField($name);


        return '<label for="' . $for . '" ' . $attributes2 . '>' . $text . $append . '</label>';
    }

    /**
     * For a select box
     * @param type $options
     * @return type
     */
    public function field_select($options) {
        $attributes = $this->constituteFieldArray(null, $options, array('value', 'required'));
        $postValue = $this->getValueFrom($options);
        $oneOptionIsSelected = false;

        $html = '<select ' . $attributes . '>';

        /**
         * Verification for values
         */
        $openedOpt = 0;

        if (!isset($options['dones']['options']['values']) or ! is_array($options['dones']['options']['values'])) {
            $html .= "<option value='nothing'>No value has been setted!</option>\n";
        }
        else {
            $default = isset($options['dones']['options']['defaultValue']) && is_string($options['dones']['options']['defaultValue']) ? $options['dones']['options']['defaultValue'] : null;


            $html.=$this->exploreChoiceSelect($options['dones']['options']['values'], $postValue, $default, $oneOptionIsSelected);
        }


        $html .= '</select>';

        $this->addConstitute('fields', $options['htmlDones']['name']);
        return $html;
    }

    /**
     * This function is usefull for explore the values that you've setted in form builder.
     * @param type $array
     * @param type $postValue
     * @param null $default
     * @param boolean $oneOptionIsSelected
     * @return string
     */
    public function exploreChoiceSelect($array, $postValue, $default, $oneOptionIsSelected = false) {
        $html = "";

        foreach ($array as $name => $value) {
            if (is_array($value)) {

                $html.="<optgroup label=\"" . utf8_encode(htmlentities($name)) . "\">\n";

                $html.= $this->exploreChoiceSelect($value, $postValue, $default);

                $html.="</optgroup>\n";
            }
            else {
                $selected = "";

                if ($oneOptionIsSelected === false && (($postValue === "" && $name == $default) OR $postValue == $name)) {
                    $selected = " selected=\"selected\"";
                    $oneOptionIsSelected = true;
                    $default = null;
                }

                // DETECT METHODS OBJECT
                if (is_object($value)) {
                    $object = $value;
                    $value = "#Object-Unable to find a good name#";

                    $checksMethod = array(
                        'getName'
                    );

                    foreach ($checksMethod as $method) {
                        if (method_exists($object, $method)) {
                            $value = $object->$method();
                            break;
                        }
                    }
                }

                $value = utf8_encode(htmlentities($value));
                $name = utf8_encode(htmlentities($name));
                $html .= "<option" . $selected . " value=\"" . $name . "\">" . $value . "</option>\n";
            }
        }

        return $html;
    }

    public function field_checkbox($options) {

        $attributes = $this->constituteFieldArray("checkbox", $options, array('value'));
        $name = $this->extractName($options);
        $defaultValue = (bool) $this->getValueFrom($options, array('value'));

        // rempli par post
        if (isset($options['dones']['binded']) && $options['dones']['binded'] === true) {
            
            if ($options['dones']['value'] === true) {
                $attributes .= " checked='checked'";
            }
        }
        else {
            if ($defaultValue === true) {
                $attributes .= " checked='checked'";
            }
        }

        $html = '<input ' . $attributes . ' />';


        $this->addConstitute('fields', $options['htmlDones']['name']);
        return $html;
    }

    /**
     * This function returns the name of field
     * @param type $input
     * @return string
     */
    protected function extractName($input) {
        return isset($input['htmlDones']['name']) ? $input['htmlDones']['name'] : $input['dones']['name'];
    }

    /**
     * This function return an array with all attributes
     * @param array $attributes
     * @param type $options
     * @param type $from
     * @return array
     */
    public function analyseAttributes(array $attributes, $options, $from = "attr") {

        if (isset($options['dones']['options'][$from]) && is_array($options['dones']['options'][$from])) {
            foreach ($options['dones']['options'][$from] as $attr => $value) {

                if (!isset($attributes[$attr])) {
                    $attributes[$attr] = $value;
                }
                elseif (isset($this->fields_html5_attributes[$attr])) {
                    $attributes[$attr] = $value;
                }
            }
        }

        return $attributes;
    }

    /**
     * This function returns an html string with all attributes
     * @param string $type
     * @param array $options
     * @return string
     */
    public function constituteFieldArray($type, $options, array $deleteAttribute = array()) {
        $finalAttributes = array();

        // Add type
        if ($type !== null && $type !== "textarea") {
            $finalAttributes['type'] = $type;
        }

        // Add name
        $finalAttributes['name'] = $this->extractName($options);
        $this->addConstitute('fields', $finalAttributes['name']);
        // Add id
        $finalAttributes['id'] = $this->getNameField($finalAttributes['name']);

        // Value [but protect some type from autocompletition]
        if ($type !== "password" && $type !== "textarea" && $this->getValueFrom($options) !== null) {
            $finalAttributes['value'] = $this->getValueFrom($options);
        }

        if (!isset($options['dones']['options']['attr']['noAttribute']) || $options['dones']['options']['attr']['noAttribute'] === false) {
            $finalAttributes = array_merge($finalAttributes, $this->fields_html5_attributes);
        }

        if (isset($options['dones']['options']['attr']['noAttribute'])) {
            unset($options['dones']['options']['attr']['noAttribute']);
        }


        /**
         * CHECK IF MUST DELETE SOME ATTRIBUTES
         */
        if (!empty($deleteAttribute)) {
            foreach ($finalAttributes as $key => $value) {
                if (in_array($key, $deleteAttribute)) {
                    unset($finalAttributes[$key]);
                }
                else {
                    if (isset($deleteAttribute[$key]) && $deleteAttribute[$key] == $value) {
                        unset($deleteAttribute[$key]);
                    }
                }
            }
        }



        $AllAttributes = $this->analyseAttributes($finalAttributes, $options);

        
        
        // Checks if error class is activate
        if (isset($this->FormConfiguration['invalidClass']) && $this->checkErrorField($options) !== false) {
            $classToAdd = $this->FormConfiguration['invalidClass'];

            if (isset($AllAttributes['class'])) {
                $AllAttributes['class'] = $classToAdd . " " . $AllAttributes['class'];
            }
            else {
                $AllAttributes['class'] = $classToAdd;
            }
        }
        
        
        $html = $this->implodeAttributes($AllAttributes);


        return $html;
    }

    /**
     * Just implode attributes
     * @param array $AllAttributes
     * @return string
     */
    public function implodeAttributes(array $AllAttributes) {
        $html = "";
        foreach ($AllAttributes as $attribute => $value) {
            $html.= $attribute . '="' . $value . '" ';
        }
        return $html;
    }

    /**
     * This function returns just an escaped name
     * @param string $in
     * @return string
     */
    protected function getNameField($in) {
        $out = str_replace(array('[', ']'), array('_', ''), "Field_" . ucfirst($in) . "_" . $this->nameForm);
        return $out;
    }

    /**
     * This function removes bad attributes
     * @param array $bad
     * @param array $attributes
     * @return array
     */
    public function removeBadAttributes(array $bad, array $attributes) {
        foreach ($bad as $a) {
            if (isset($attributes[$a])) {
                unset($attributes[$a]);
            }
        }

        return $attributes;
    }

    /**
     * Return an open code for form html code.
     * @param array $arguments
     * @return string
     */
    public function form_open(array $arguments = array()) {
        if ($this->stateForm !== false) {
            return;
        }

        $this->addConstitute('tags', 'form_open');
        $this->stateForm = 1;
        $action = "";

        if ($this->action !== null) {
            if (is_array($this->action) && isset($this->action['route'])) {
                $routerArguments = array(
                    'route' => $this->action['route'],
                    'arguments' => isset($this->action['arguments']) ? $this->action['arguments'] : array(),
                );

                $url = Controller::getUrlFromRoute($routerArguments);
                $action = " action='$url'";
            }
            else {
                $action = " action='$this->action'";
            }
        }

        $arguments = $this->implodeAttributes($arguments);
        if (!empty($arguments)) {
            $arguments = " " . $arguments;
        }
        return "<form method='post'{$action}{$arguments}>\n";
    }

    /**
     * Returns the end code for form html code.
     * @return string
     */
    public function form_close() {
        if ($this->stateForm !== 1) { // Not opened
            return;
        }
        $this->stateForm = 2;

        $this->addConstitute('tags', 'form_close');
        return "</form>";
    }

    /**
     * This function returns all parameters that was not view on current view.
     * @param array $arguments
     * @return string
     */
    public function form_rest($arguments = array()) {
        $realForm = $this->getForm();
        $finalHtml = "";

        // Check if form is opened or closed
        if ($this->stateForm === false) {
            $finalHtml.= $this->form_open();
        }
        elseif ($this->stateForm !== 1) { // Form is closed so nothing could be do
            return;
        }


        foreach ($realForm as $name => $options) {
            $codeHtmlGen = "";

            $name2 = $this->extractName($options);
            if ($this->checkConstitute("fields", $name2) === true) {
                continue;
            }

            if (method_exists($this, "field_" . $options['htmlDones']['type'])) {
                $method = "field_" . $options['htmlDones']['type'];
                $codeHtmlGen = $this->$method($options);
            }
            else {
                $codeHtmlGen = $this->field_text($options);
            }


            $codeLabel = "";

            if (!isset($options['dones']['options']['noLabel']) or $options['dones']['options']['noLabel'] === false) {
                $codeLabel = $this->label_form($options,  ' :');
            }

            if ($options['htmlDones']['type'] !== "hidden") {
                $finalHtml .= $codeLabel . $codeHtmlGen . "<br />\n\n";

                // Check for error
                $errorField = $this->checkErrorField($options);
                if ($errorField !== false) {
                    $finalHtml .= "<span style='color:red;'>" . $errorField['message'] . "</span><br /><br />\n";
                }
            }
            else {
                $finalHtml .= $codeHtmlGen . "\n";
            }
        }

        // BUTTONS
        if (!isset($arguments['without']['submit']) && !$this->checkConstitute("buttons", "submit")) {
            $finalHtml.="<input type='submit' value='Submit form' />\n";
        }

        // CLOSE FORM
        $finalHtml .= $this->form_close();

        return $finalHtml;
    }

    /**
     * This function returns all form
     * @return string
     */
    public function makeAllForm() {
        $realForm = $this->getForm();

        $finalHtml = "";

        // OPEN FORM
        $finalHtml .= $this->form_open();

        foreach ($realForm as $name => $options) {
            $codeHtmlGen = "";

            if (method_exists($this, "field_" . $options['htmlDones']['type'])) {
                $method = "field_" . $options['htmlDones']['type'];
                $codeHtmlGen = $this->$method($options);
            }
            else {
                $codeHtmlGen = $this->field_text($options);
            }


            $codeLabel = "";

            if (!isset($options['dones']['options']['noLabel']) or $options['dones']['options']['noLabel'] === false) {
                $codeLabel = $this->label_form($options);
            }

            if ($options['htmlDones']['type'] !== "hidden") {
                $finalHtml .= $codeLabel . " : " . $codeHtmlGen . "<br />\n";

                // Check for error
                $errorField = $this->checkErrorField($options);
                if ($errorField !== false) {
                    $finalHtml .= "<span style='color:red;'>" . $errorField['message'] . "</span><br /><br />\n";
                }
            }
            else {
                $finalHtml .= $codeHtmlGen . "\n";
            }
        }

        // BUTTONS
        $finalHtml.="<input type='submit' value='ok' />";

        // CLOSE FORM
        $finalHtml .= $this->form_close();

        return $finalHtml;
    }

    /**
     * This function returns all form in html
     * @param array $options
     * @return string
     */
    public function getValueFrom($options, array $dontUse = array()) {


        $value = null;

        if (isset($options['dones']['value']) && !in_array('value', $dontUse)) {
            $value = $options['dones']['value'];
        }
        elseif (isset($options['dones']['options']['value']) && !in_array('options.value', $dontUse)) {
            $value = $options['dones']['options']['value'];
        }
        elseif (isset($options['dones']['analyse']['defaultValue']) && !in_array('defaultValue', $dontUse)) {
            $value = $options['dones']['analyse']['defaultValue'];
        }


        switch (gettype($value)) {

            case "object":
                $value = $this->getValueFromObject($value);
                break;


            default:
                break;
        }


        // Secure value
        $value = SecurityService::secureData(array('trim', 'htmlspecialchars'), $value);

        return $value;
    }

    /**
     * This function get value from an object like Datetime or other.
     * @param object $value
     * @return null|string|integer
     */
    public function getValueFromObject($value) {
        if (!is_object($value)) {
            return null;
        }

        // SAVE VALUE
        $originalValue = $value;
        $value = null;
        
        
        
        switch (get_class($originalValue)) {
            case "DateTime":
                
                $originalValue instanceof \DateTime;
                
                $value = date('d/m/Y', $originalValue->getTimestamp());
                
                break;

            default:
                
                break;
        }


        return $value;
    }

    /**
     * This function checks if a field is in error.
     * @param type $options
     * @return boolean
     */
    public function checkErrorField($options) {

        if (!isset($options['dones']['error']['error']) or $options['dones']['error']['error'] !== true) {
            return false;
        }
        else {
            return $options['dones']['error'];
        }
    }

    public function addConstitute($locate, $value) {
        $this->shownConstitutes[$locate][] = $value;
    }

    /**
     * 
     * @param type $locate
     * @param type $value
     * @return boolean
     */
    public function checkConstitute($locate, $value) {
        if (isset($this->shownConstitutes[$locate]) && in_array($value, $this->shownConstitutes[$locate])) {
            return true;
        }


        return false;
    }

    /**
     * Used to know if Form has been binded.
     * @param type $options
     * @return boolean
     */
    public function hasBeenBinded($options) {

        if (isset($options['dones']['binded']) && $options['dones']['binded'] === true) {
            return true;
        }

        return false;
    }

}
