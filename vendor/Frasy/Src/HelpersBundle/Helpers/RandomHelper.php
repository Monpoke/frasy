<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * RANDOM HELPER
 */

namespace Frasy\HelpersBundle\Helpers;

use Frasy\DefaultBundle\Controllers\Helper;

class RandomHelper extends Helper {
    
    protected static $characters = "abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWYZ0123456789";
    
    public static function randomString($nbr) {
        $str = "";
        
        srand((double) microtime() * 1000);

        for ($i = 0; $i < $nbr; $i++) {
            $str .= self::$characters[rand() % strlen(self::$characters)];
        }

        return $str;
    }

    
}