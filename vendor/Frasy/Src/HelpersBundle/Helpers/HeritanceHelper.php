<?php

/**
 * This file is a part of Framewwork 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * TIME HELPER
 */

namespace Frasy\HelpersBundle\Helpers;

use Frasy\DefaultBundle\Controllers\Helper;
use Frasy\LoadersBundle\Controllers\CacheController as Cache;

class HeritanceHelper extends Helper {

    public function __construct() {
        parent::__construct();
    }

    public static function getAllParent($class) {
        $parents = array(get_class($class));
        while (true) {

            $parent = get_parent_class($class);
            if ($parent !== false) {
                $parents[] = $parent;
                $class = $parent;
            } else {
                break;
            }
        }

        return $parents;
    }

    public static function scanDirectory($directory) {
        $files = array();

        $inDir = scandir($directory);
        foreach ($inDir as $value) {
            if ($value === ".." || $value === ".")
                continue;

            $path = $directory . DS . $value;
            if (is_file($path)) {
                $files [] = $value;
            } elseif (is_dir($path)) {
                $files[$value] = self::scanDirectory($path);
            } else {
                return "Error in " . __FILE__;
            }
        }

        return $files;
    }

    /**
     * Delete a tree folder
     * @param type $directory
     * @return boolean
     */
    public static function deleteTree($directory) {
        $files = self::scanDirectory($directory);

        self::deleteTreeSub($directory, $files);

        rmdir($directory);
        return true;
    }

    /**
     * Delete tree and subdirs
     * @param type $origin
     * @param type $directory
     */
    public static function deleteTreeSub($origin, $directory) {
        foreach ($directory as $k => $v) {
            // IF IS DIRECTORY
            if (is_dir($origin . DS . $k) && !is_link($origin . DS . $k)) { // Directory
                self::deleteTreeSub($origin . DS . $k, $v);
                rmdir($origin . DS . $k);
            } 
            // IF IS LINK, DO NOT DELETE CONTENT
            elseif(is_link($origin . DS . $k)) {
                unlink($origin . DS . $k);
            } 
            // DELETE File
            else { 
                unlink($origin . DS . $v);
            }
        }
    }

}