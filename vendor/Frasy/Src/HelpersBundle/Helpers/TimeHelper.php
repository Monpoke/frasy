<?php

/**
 * This file is a part of Framewwork 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * TIME HELPER
 */


namespace Frasy\HelpersBundle\Helpers;
use Frasy\DefaultBundle\Controllers\Helper;
use Frasy\LoadersBundle\Controllers\CacheController as Cache;

class TimeHelper extends Helper {
    
    public function __construct() {
        parent::__construct();
        
    }
    
    /**
     * This function returns timestamp for asked moment
     * @return timestamp
     */
    public function getTime(){
        return isset($_SERVER['REQUEST_TIME'])? $_SERVER['REQUEST_TIME'] : time();
        
    }
    
   public static function getTimestamp(){
       
       exit(var_dump(Cache::getPathFile("Frasy.Helper.Time.LastTimestamp")));
       
       $lastGood = time();
       
       $current = time()-7854;
       
       exit('new is : '.date('H:i:s',($current + ($lastGood-$current))));
   }
    
    
}