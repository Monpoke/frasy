<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * HTML HELPER
 */

namespace Frasy\HelpersBundle\Helpers;

use Frasy\DefaultBundle\Controllers\Helper;
use Frasy\HelpersBundle\Forms\HtmlForm;

class HtmlHelper extends Helper {

    public function createViewFromFormBuilder(array $form, $action = null, $FormName=null, $binded=false) {

        $finalForm = array();

        foreach ($form['form'] as $name => $array) {
            // INIT
            $htmlDones = array();

            // Autodetection of types
            $htmlType = $this->autoDetectType(isset($array['type']) ? $array['type'] : 'text');

            // Constitution name
            if ($name !== "_crsf") {
                $htmlDones['name'] = $form['entityName'] . "[$name]";
            } else {
                $htmlDones['name'] = "_crsf";
            }

            // Value
            /**
             * @TODO
             */
            $htmlDones['value'] = isset($array['analyse']['defaultValue']) ? $array['analyse']['defaultValue'] : '';

            
            // Add final form to final
            $finalForm[$name] = array(
                'htmlDones' => array_merge($htmlType, $htmlDones),
                'dones' => $array
            );
        }



        return new HtmlForm($finalForm,  $action, $FormName, $binded);
    }

    public function autoDetectType($type) {
        $return = array(
            'type' => "text",
        );

        switch ($type) {

            // TEXT TYPE
            case "text":
            case "integer": case "int": case "number":

                $return['type'] = "text";
                break;

            // TEXTAREA TYPE
            case "textarea":
                $return['type'] = "textarea";
                break;

            // URL TYPE
            case "url": case "http": case "https":
                $return['type'] = "url";
                break;

            case "":
                break;

            // PASSWORD TYPE
            case "password": case "passwd": case "password_confirm": case "passwd_confirm":
                $return['type'] = "password";
                break;


            case "":
                break;

            /**
             * Other matching are not supported
             */
            default:
                $return['type'] = $type;
        }

        return $return;
    }

    
}