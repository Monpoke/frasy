<?php


/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\HelpersBundle\Helpers;

use Frasy\DefaultBundle\Controllers\Helper;
use Frasy\HelpersBundle\Helpers\MobileDetect;

class OSHelper extends Helper {

    protected static $characters = "abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWYZ0123456789";

    /**
     * Return Server OS
     * @param type $in
     * @return string
     */
    public static function getNameOs($in = PHP_OS) {
        $str = null;

        $in = strtolower($in);

        if ($in === "linux") {
            $str = "linux";
        } elseif (substr($in, 0, 3) === "win") {
            $str = "windows";
        } else {
            $str = "unknown";
        }

        return $str;
    }

    /**
     * This function returns if user is on mobile device
     * @return boolean
     */
    public static function isMobile(){
        $MD = new MobileDetect();
        return $MD->isMobile();
    }
    
    /**
     * This function returns if user is on tablet device
     * @return boolean
     */
    public static function isTablet(){
        $MD = new MobileDetect();
        return $MD->isTablet();
        
    }
    
    
    
    
    
}