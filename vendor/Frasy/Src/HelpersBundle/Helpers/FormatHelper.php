<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 * HTML HELPER
 */

namespace Frasy\HelpersBundle\Helpers;

use Frasy\DefaultBundle\Controllers\Helper;
use Doctrine\Common\Util\Inflector;

class FormatHelper extends Helper {

    /**
     * This function replace special characters in html format
     * @param type $in
     * @return string
     */
    public static function canonicalForm($in) {
        $in = trim($in);
        $in = strtr($in, "@ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ", "aaaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
        $in = strtr($in, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz");
        $in = preg_replace('#([^.a-z0-9]+)#i', '-', $in);
        $in = preg_replace('#-{2,}#', '-', $in);
        $in = preg_replace('#-$#', '', $in);
        $in = preg_replace('#^-#', '', $in);
        return $in;
    }

    public static function convertDate($timestamp, $mode = 1) {
        $timestamp = intval($timestamp);

        $days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        $months = array('Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre');

        $dayNum = date('N', $timestamp);
        $monthNum = date('n', $timestamp);
        $num = date('j', $timestamp);
        $hour = date('G', $timestamp);
        $minute = date('i', $timestamp);
        $year = date('Y', $timestamp);

        $finalString = "";

        if ($mode == 1) {
            $finalString = $days[$dayNum - 1] . " $num " . $months[$monthNum - 1] . " $year à $hour:$minute";
        }
        elseif ($mode == 2) {
            $finalString = $days[$dayNum - 1] . " $num " . $months[$monthNum - 1];
        }


        return utf8_encode(ucfirst(strtolower(utf8_decode($finalString))));
    }

    public static function convertMemory($in) {

        if ($in < 1024) {
            return $in . "B";
        } elseif ($in < 1048576) {
            return round($in / 1024, 2) . "KB";
        } else {
            return round($in / 1048576, 2) . "MB";
        }
    }

    public static function betweenBornes($in, $min = 0, $max = 100) {
        if ($min !== null && $in < $min) {
            return $min;
        } elseif ($max !== null && $in > $max) {
            return $max;
        }

        return $in;
    }

    public static function isBeetweenBornes($in, $min, $max) {
        if ($in < $min || $in > $max) {
            return false;
        }

        return true;
    }

    /**
     * Modifies a string to remove all non ASCII characters and spaces.
     */
    static public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text2 = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text2);
        }

        // lowercase
        $text3 = strtolower($text2);

        // remove unwanted characters
        $text4 = preg_replace('~[^-\w]+~', '', $text3);

        if (empty($text4)) {
            return 'n-a';
        }

        return $text4;
    }

}