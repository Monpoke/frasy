<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */
use Frasy\ConsoleBundle\Controllers\CommandsController as Commands;

Commands::registerCommand("router", array(
    'commands' => array(
        'routes' => array(
            'description' => 'List routes',
            'action' => '\Frasy\RouterBundle\Console\ConsoleCommand::getAllRoutesCommand'
        ),
        'help' => array(
        ),
    ),
));

Commands::registerCommand("doctrine", array());

