<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\RouterBundle\Controllers;

use App\Config\Configuration\ValidatorService as Validator;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\NotFoundException;
use Frasy\ExceptionsBundle\Exceptions\RouterException;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

class RouterController extends Controller {

    protected static $configuration = null;
    protected static $routeName = null;
    protected static $activeNotFoundException = true;

    public static function getActiveNotFoundException() {
        return self::$activeNotFoundException;
    }

    public static function setActiveNotFoundException($activeNotFoundException) {
        self::$activeNotFoundException = (bool)$activeNotFoundException;
    }

        
    /**
     * This function lunch analyse
     * @return array
     */
    public function lunchAnalyse() {
        self::$configuration = self::getConfiguration();

        $from = $this->determineMethod();

        $fromFinal = $this->clearFilenameFromUrl($from);

        $routes = $this->getRoutingPath();

        $return = $this->compareRoutes($fromFinal, $routes);

        return $return;
    }

    protected static function determineMethod() {
        $Configuration = self::$configuration;
        $methodFrom = $Configuration['Parameters']['Router']['dispatchFrom'];

        switch ($methodFrom):
            case "server.path_info":
                $from = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "/";
                break;
            case "server.request_uri": default:
                $from = $_SERVER['REQUEST_URI'];

                break;
        endswitch;



        return $from;
    }

    protected function getRoutingPath() {
        $routing = self::$configuration['Routing'];

        $routes = $routing->getRoutes();


        return $routes;
    }

    protected function compareRoutes($from, $routes) {
        $from = $this->getExplodedRoute($from);
        $requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : false;

        $Validator = new Validator();

        /**
         * Foreach all registred bundle
         */
        foreach ($routes as $namespace => $list) {


            /**
             * FOREACH all routes in namespace
             */
            foreach ($list as $route_name => $params) {

                $pattern = $this->getExplodedRoute($params['pattern']);


                // Init parameters
                $parameters = array();
                $break = false;

                /** IF NUMBER OF ARGUMENTS IS DIFFERENT */
                if ((count($params['variables']) === 0 && count($pattern) !== count($from)) || (count($params['variables']) === 0 && count($from) < $params['forceSegments'])) {

                    continue;
                } elseif (isset($params['options']['options']['method'])) {
                    $method = $params['options']['options']['method'];

                    if (is_array($method) && !in_array('*', $method) && !in_array($requestMethod, $method)) {
                        continue;
                    } elseif (!is_array($method) && $method !== "*" && $requestMethod !== $method) {

                        continue;
                    }
                }

                $argumentsList = array();
                foreach ($pattern as $k => $part) {
                    $argumentsList[] = $part;

                    // Verif variable
                    if (strpos($part, '{') !== false) {
                        $part = str_replace(array('{', '}'), '', $part);

                        if (!in_array($part, $params['variables'][$part]) or !isset($from[$k]) && (!isset($params['variables'][$part]['constraints']['nullable']) OR $params['variables'][$part]['constraints']['nullable'] !== true)) {
                            $break = true;
                            continue 2;
                        } else {
                            // Get Argument 
                            if (isset($from[$k]))
                                $arg = $from[$k];
                            else
                                $arg = null;

                            $constraintsOn = $params['variables'][$part]['constraints'];

                            // Checks that argument is valid
                            if (!empty($arg) && $Validator->validate($constraintsOn['type'], $arg) === false) {

                                $break = true;
                                continue 2;
                            } else {
                                // Valid argument
                                $parameters[$part] = $arg;
                            }
                        }
                    } elseif (!isset($from[$k]) or $part !== $from[$k]) { // Skip if portion is different from URI
                        $break = true;
                        continue 2;
                    }
                }


                if ($break === true) {
                    continue;
                }

                /** ROUTE FOUND */
                return $this->routeFound($params, $parameters);
            }
        }

        /**
         * EMIT EVENT 
         * IF DEVELOPER WANT TO INJECT HIS OWN ROUTER
         */
        EventsListener::notifyEvent("FRASY_NOT_DETERMINED_ROUTE", array());


        if (self::$activeNotFoundException === true) {
                throw new NotFoundException(__("No route matching"));
        }
    }

    public function getExplodedRoute($input) {
        $input = explode('/', $input);
        $final = array();

        foreach ($input as $in) {
            if (empty($in)) {
                continue;
            }
            $final[] = $in;
        }

        return $final;
    }

    public function getCuttedNamespace($input) {
        $input = explode('\\', $input);
        $final = array();

        foreach ($input as $in) {
            if (empty($in)) {
                continue;
            }
            $final[] = $in;
        }

        return implode('\\', $final);
    }

    public static function getAllParametersData() {
        $from = explode('/', self::determineMethod());
        $l = array();

        foreach ($from as $k => $v) {
            if (empty($v)) {
                continue;
            }
            $l[] = $v;
        }

        return $l;
    }

    public static function getCurrentUrlData() {
        $from = self::determineMethod();
        return $from;
    }

    public static function getCurrentRoute() {
        return self::$routeName;
    }

    public function clearFilenameFromUrl($from) {
        if (!isset($_SERVER['SCRIPT_NAME'])) {
            return $from;
        }
        $from = str_replace_once($_SERVER['SCRIPT_NAME'], "", $from);
        return (empty($from) ? "/" : $from);
    }

    /**
     * This function is called in order to call the correct controller
     * @param array $paramsRoute
     * @param array $parameters This variable is application configuration
     * @return array
     * @throws RouterException
     */
    public function routeFound($paramsRoute, $parameters = null) {
        self::$routeName = $paramsRoute['routeName'];
        $controllerName = $paramsRoute['namespace'] . "\\" . $paramsRoute['controller'];
        if (class_exists($controllerName, true)) {
            $controller = new $controllerName();


            if (method_exists($controller, $paramsRoute['action'])) {

                $bundleName = !empty($paramsRoute['bundleName']) ? $paramsRoute['bundleName'] : $paramsRoute['namespace'];
                $bundle = $this->getBundle($bundleName);

                return array(
                    'namespace' => $this->getCuttedNamespace($paramsRoute['namespace']),
                    'parameters' => $parameters,
                    'bundleName' => $bundleName,
                    'bundle' => $bundle,
                    'controller' => $paramsRoute['controller_name'],
                    'action' => $paramsRoute['action'],
                    'routeName' => self::$routeName
                );
            } else {
                throw new RouterException(__("{$paramsRoute['action']} action not exits in $controllerName for route " . self::$routeName));
            }
        } else {
            throw new RouterException(__("$controllerName not exits for route " . self::$routeName));
        }
    }

}
