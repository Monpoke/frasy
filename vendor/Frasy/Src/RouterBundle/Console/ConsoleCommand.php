<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\RouterBundle\Console;

use Frasy\DefaultBundle\Controllers\ConsoleCommand as BaseConsoleCommand;

class ConsoleCommand extends BaseConsoleCommand {

    public function getAllRoutesCommand($params) {

        $config = $this->getConfiguration();
        $routes = $config['Routing']->getRoutes();

        $output = "";

        foreach ($routes as $bundleName => $routes) {
            $output .= "Bundle $bundleName\n";

            foreach ($routes as $name => $route) {
                $output.="\t";

                $output .= $name . "     |     ";

                $output .= $route['pattern']. "     |     ";
                
                if (isset($route['options']['options']['method'])) {
                    $method = $route['options']['options']['method'];
                    if(is_array($method))
                        $method = implode('|', $method);
                    
                    $output .= $method;
                } else 
                    $output .=  "ANY";

                $output .= "\n";
            }

            $output .= "\n\n";
        }

        return $output;
    }

    
}

