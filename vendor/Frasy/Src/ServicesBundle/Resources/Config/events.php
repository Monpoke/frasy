<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */


use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

EventsListener::registerEvent("FRASY_BEFORE_RENDER", "\Frasy\ServicesBundle\Services\SecurityService::initWithFrasyEvent", array());
EventsListener::registerEvent("FRASY_BEFORE_RENDER", "\Frasy\ServicesBundle\Services\SessionService::FrasyRenderEvent", array());