<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */
/**
 * This variable is used for add namespaces to autoloader
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Service;
use Frasy\LoadersBundle\Controllers\TranslationController;

abstract class Translator extends Service {

    public static function switchLanguage($locale){
        return TranslationController::switchLanguage($locale);
    }
    
    public static function getLanguage(){
        return TranslationController::getCurrentLanguage();
    }
    
    
}