<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ServicesBundle\Services;
use Frasy\DefaultBundle\Controllers\Service;

class MailerService extends Service {
    protected $transport;
    protected $mailer;
    
    public function __construct() {
        // Get a default transport
        $this->setTransport(\Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs'));

        // Create the Mailer using your created Transport
        $this->setMailer(\Swift_Mailer::newInstance($this->transport));
    }
    
    /**
     * This function create a new message to send
     * @return \Swift_Message
     */
    public function newMessage(){
        return \Swift_Message::newInstance();
    }
    
    /**
     * This function return transport
     * @return \Swift_SendmailTransport
     */
    public function getTransport(){
        return $this->transport;
    }
    
    /**
     * This function returns mailer
     * @return \Swift_Mailer
     */
    public function getMailer(){
        return $this->mailer;
    }
    
    /**
     * This function set mailer
     * @param \Swift_Mailer $mailer
     * @return \Frasy\ServicesBundle\Services\MailerService
     */
    public function setMailer(\Swift_Mailer $mailer){
        $this->mailer = $mailer;
        return $this;
    }
    
    /**
     * This function set transport
     * @param \Swift_SendmailTransport $transport
     * @return \Frasy\ServicesBundle\Services\MailerService
     */
    public function setTransport(\Swift_SendmailTransport $transport){
        $this->transport = $transport;
        return $this;
    }
    
    /**
     * This function send a message
     * @param \Swift_Message $message
     * @return boolean
     */
    public function send(\Swift_Message $message){
        if($this->mailer !== null){
            return $this->mailer->send($message);
        }
        
        return false;
    }
    
}