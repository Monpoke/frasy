<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ServicesBundle\Services;

use App\Config\Configuration\ValidatorService as Validator;
use Frasy\DefaultBundle\Controllers\Service;
use Frasy\ExceptionsBundle\Exceptions\AppException;
use Frasy\ExceptionsBundle\Exceptions\SecurityException;
use Frasy\HelpersBundle\Helpers\HtmlHelper;
use Frasy\HttpBundle\HttpKernel\HttpRequest;
use Frasy\ServicesBundle\Services\SecurityService;
use ReflectionClass;

class FormBuilder extends Service {

    private $entity = null;
    private $Validator = null;
    private $declaredProperty = array();
    private $fieldsName = array();
    private $params = array();
    private $binded = false;
    private $formName = null;

    /**
     * Construct form
     * @param null|entity $entity
     */
    private function __construct($entity = null, $FormName = null) {
        $this->entity = $entity;
        $this->Validator = new Validator();
        $this->HtmlHelper = new HtmlHelper();

        // Give possibility to make form without entity
        if ($this->entity !== null) {

            $reflect = new ReflectionClass($entity);
            $ar = $reflect->getProperties();

            foreach ($ar as $property) {
                $property = (array) $property;
                $this->declaredProperty[] = $property['name'];
            }
        }

        // Set name for this form
        if ($FormName === null) {
            $this->formName = substr(sha1(rand(0, 99)), 0, 12);
        }
        else {
            $this->formName = $FormName;
        }

        // Add automaticaly a hidden field for security
        $this->add("_crsf", "hidden", array('value' => SecurityService::getCrsfToken(), 'noLabel' => true));
    }

    /**
     * This function will add a new field to form.
     * Name is the name of entity property, or a custom name if entity is null or options['notSave'] equals true.
     * Type is the type of field as text password, select...
     * @param string $name
     * @param string $type
     * @param array $options
     * @return FormBuilder
     * @throws AppException
     */
    final public function add($name, $type, array $options = array()) {

        $nameServer = isset($options['alias']) && is_string($options['alias']) ? $options['alias'] : $name;

        // Just for security
        if ($name === "_crsf") {
            if ($this->getParam("CrsfToken") == "off") {
                return $this;
            }

            $this->fieldsName[$name] = array(
                'name' => $nameServer,
                'frontName' => $name,
                'type' => $type,
                'options' => $options
            );

            return $this;
        }

        // Anything else
        if ($this->entity !== null && !in_array($nameServer, $this->declaredProperty) && (!isset($options['notSave']) OR ( (isset($options['notSave']) && $options['notSave'] !== true) || (isset($options['dontSave']) && $options['dontSave'] !== true)))) {

            throw new AppException(__("Argument $nameServer for Form Builder not exists"));
        }


        /**
         * ADD DEFAULT VALIDATION RULE FOR 
         * SELECT TYPE FIELD
         */
        if ($type === "select") {
            if (!isset($options['validate']['inList'])) {
                $options['validate']['inArray'] = array(
                    'rule' => 'inArray',
                    'src' => 'values'
                );
            }
        }

        if (isset($options['validate'])) {
            $this->parseValidation($options);
        }
        elseif ($type !== "checkbox") {
            $options['validate']['type'] = array(
                'rule' => "alphanumeric",
                'message' => __("$name must be alphanumeric.")
            );
        }
        else {
            $options['validate'] = array();
        }

        if ($this->entity !== null) {
            $returnfromEntity = $this->analyseEntity($nameServer);
        }
        else {
            $returnfromEntity = array();
            $options['notSave'] = true;
        }

        $this->fieldsName[$name] = array(
            'name' => $nameServer,
            'frontName' => $name,
            'type' => $type,
            'options' => $options,
            'analyse' => $returnfromEntity,
        );

        return $this;
    }

    /**
     * Return the current form
     * @return array
     */
    public function getForm() {

        $fields = $this->fieldsName;

        return $fields;
    }

    /**
     * This function create the render view for front. 
     * If $action is specified, the form attribute 'action' will be filled. 
     * @param type $form
     * @param string $action
     * @return array
     */
    final public function createView($form = null, $action = null) {
        if ($form === null) {
            $form = $this->getForm();
        }


        $HtmlHelper = new HtmlHelper();

        $entityName = explode('\\', \get_class($this->entity));
        $entityName = $entityName[count($entityName) - 1];

        $form = array(
            'entityName' => $entityName,
            'form' => $form
        );

        if ($action === null) {
            $action = $this->getParam("FormAction");
        }

        return $HtmlHelper->createViewFromFormBuilder($form, $action, $this->formName, $this->binded);
    }

    /**
     * This function will initiate a form with an entity or not
     * @param entity $entity
     * @return FormBuilder
     * @throws AppException
     */
    public static function createForm($entity, $FormName = null) {
        if (!is_object($entity) && $entity !== null) {
            throw new AppException(__("Argument for Form Builder must be an Entity"));
        }

        return new FormBuilder($entity, $FormName);
    }

    /**
     * This function will analyse entity to research the default values
     * @param type $name
     * @return type
     */
    public function analyseEntity($name) {
        $entity = $this->entity;
        if ($entity === null) {
            return;
        }
        $return = array();

        $name = ucfirst($name);

        $getter = "get$name";


        if (method_exists($entity, $getter)) {
            $return['defaultValue'] = $entity->$getter();
        }
        else {
            $getter = "is$name";
            if (method_exists($entity, $getter)) {
                $return['defaultValue'] = $entity->$getter();
            }
        }


        return $return;
    }

    /**
     * This function will bind value with the HttpRequest
     * @param HttpRequest $request
     * @return FormBuilder
     */
    public function bind(HttpRequest $request) {
        $data = $request->getAllData(array('trim'));
        $this->binded = true;

        $myClassName = get_class($this->entity);
        $myClassName = explode('\\', $myClassName);
        $myClassName = $myClassName[count($myClassName) - 1];

        if (isset($data[$myClassName])) {

            foreach ($data[$myClassName] as $k => $v) {
                // Skip not existing fields
                if (!isset($this->fieldsName[$k])) {
                    return;
                }

                if ($this->fieldsName[$k]['type'] === "checkbox") {
                    $v = true;
                }


                $nameServer = $this->fieldsName[$k]['name'];

                /**
                 * TO PROCESS DATA BEFORE SETTING
                 */
                if (isset($this->fieldsName[$k]['options']['preProcessData']) && is_callable($this->fieldsName[$k]['options']['preProcessData'])) {
                    $v = call_user_func($this->fieldsName[$k]['options']['preProcessData'], $v, $this->fieldsName[$k]);
                }

                $this->fieldsName[$k]['value'] = $v;

                $this->fieldsName[$k]['binded'] = true;

                if ($this->entity !== null && method_exists($this->entity, "set" . ucfirst($nameServer))) {
                    $setter = "set" . ucfirst($nameServer);
                    $this->entity->$setter($v);
                }
            }


            /** search in array after checkbox */
            foreach ($this->fieldsName as $k => $v) {
                if ($v['type'] === "checkbox") {
                    // name of field entity
                    $nameServer = $v['name'];
                    // If is not binded yet, it's because the field doesn't exist
                    
                    
                    
                    if (!isset($v['binded']) || $v['binded'] === false) {
                        $this->fieldsName[$k]['binded'] = true;
                        $valueToSet = false;
                    }
                    else {
                        $valueToSet = true;
                    }
                    
                    /**
                     * SET VALUE IN FORM
                     */
                    $this->fieldsName[$k]['value'] = $valueToSet;

                    
                    /**
                     * SET VALUE IN ENTITY
                     */
                    if (method_exists($this->entity, "set" . ucfirst($nameServer))) {
                        $setter = "set" . ucfirst($nameServer);
                        $this->entity->$setter($valueToSet);
                    }
                }
            }
        }

        if (isset($data['_crsf'])) {
            $this->fieldsName['_crsf']['value'] = $data['_crsf'];
        }

        return $this;
    }

    /**
     * This function will check all value with validate options given to form
     * @return boolean
     * @throws SecurityException
     */
    public function validate() {
        $verifiedFields = array();
        $countErrors = 0;

        $allErrors = array();

        /** Get all values */
        $allValues = array();
        foreach ($this->fieldsName as $name => $options) {
            $allValues[$name] = (isset($options['value'])) ? $options['value'] : null;
        }

        foreach ($this->fieldsName as $name => $options) {

            if ($name === "_crsf") {

                if ($this->getParam("CrsfToken") !== "off" && isset($options['value'])) {
                    if (!SecurityService::compareCrsfTokens($options['value'])) {
                        $countErrors++;
                        throw new SecurityException(__("Crsf token is invalid!"));
                    }
                }
                else {
                    continue;
                }
            }
            else {
                $value = $allValues[$name];

                foreach ($options['options']['validate'] as $ruleName => $params) {


                    $params = array_merge($params, array(
                        'entityName' => $this->getNameEntity(true),
                        'fieldName' => $name,
                        'options' => $options,
                    ));



                    $resultValidation = $this->Validator->validate($params['rule'], $value, $params, $allValues);


                    if (isset($params['skipAfter']) && $params['skipAfter'] === true) {
                        if ($resultValidation === true) {
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    elseif (true !== $resultValidation) {

                        $countErrors++;

                        $message = (!empty($params['message'])) ? $params['message'] : __("This field is invalid!");
                        $allErrors[] = array(
                            'message' => $message,
                            'field' => $name
                        );

                        $this->fieldsName[$name]['error'] = array(
                            'error' => true,
                            'message' => $message
                        );
                    }
                    else {
                        continue;
                    }
                }
            }

            $verifiedFields[] = $name;
        }


        if (!in_array('_crsf', $verifiedFields) && $this->getParam("CrsfToken") !== "off") {
            throw new SecurityException(__("Crsf token was not found"));
        }
        else {

            if ($countErrors === 0) {
                return true;
            }
            else {

                // OPERATIONS TO SAY

                return false;
            }
        }
    }

    /**
     * This function returns a parameter setted before with setParam method
     * @param string $name
     * @return any
     */
    public function getParam($name) {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        }
        else {
            return null;
        }
    }

    /**
     * This function is usefull to set some parameters for the FormBuilder
     * @param string $name
     * @param any $value
     * @return FormBuilder
     */
    public function setParam($name, $value) {
        $this->params[$name] = $value;

        return $this;
    }

    /**
     * This function gives possibility to set a custom error for a field
     * @param type $name
     * @param type $message
     * @return type
     */
    public function setErrorField($name, $message) {
        return $this->fieldsName[$name]['error'] = array(
            'error' => true,
            'message' => $message
        );
    }

    /**
     * This function return the error of a field if it's exists
     * @param type $name
     * @return boolean string
     */
    public function getErrorField($name) {
        if (isset($this->fieldsName[$name]['error'])) {
            return $this->fieldsName[$name]['error'];
        }
        else {
            return false;
        }
    }

    /**
     * This fuction will validate the options of registred fields
     * @param array $options
     * @return boolean
     * @throws AppException
     */
    public function parseValidation($options) {
        if (!isset($options['validate'])) {
            return false;
        }

        $validation = $options['validate'];

        foreach ($validation as $name => $params) {
            if (!isset($params['rule'])) {
                throw new AppException(__("Param 'rule' not exists for $name attribute validation of a form."));
            }

            if (!$this->Validator->typeExists($params['rule'])) {
                throw new AppException(__("Argument {$params['rule']} for param 'rule' not exists for $name attribute validation for a form."));
            }
        }
    }

    /**
     * This function returns the entity name
     * @param boolean $plain
     * @return null string
     */
    public function getNameEntity($plain = false) {
        if ($this->entity === null) {
            return null;
        }

        $entity = get_class($this->entity);

        if ($plain !== true) {
            $entityEx = explode('\\', $entity);
            return $entity[count($entityEx) - 1];
        }

        return $entity;
    }

    /**
     * This function return the modified entity
     * @return entity
     */
    public function getEntity() {
        return $this->entity;
    }

    /**
     * This function return the current value of a field
     * @param string $name
     * @return string null
     */
    public function getFieldValue($name) {
        return (isset($this->fieldsName[$name]['value']) ? $this->fieldsName[$name]['value'] : null);
    }

    public function setFormName($name) {
        return $this->formName = $name;
    }

}
