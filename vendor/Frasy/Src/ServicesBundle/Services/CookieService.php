<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */
/**
 * This variable is used for add namespaces to autoloader
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Service;

class CookieService extends Service {

    public function __construct() {
        parent::__construct();
        self::init();
    }

    public static function init() {
        
    }

    /**
     * This function create cookie with name $name
     * @param type $name
     * @param type $value
     */
    public static function set($name, $value=null, $expire=0, $path=null, $domain=null, $secure=false, $httponly=false) {
        self::init();
        
        return setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);

    }

    /**
     * This function returns value for a path key
     * @param type $name
     * @return null
     */
    public static function get($name) {
        self::init();

        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        }
        else
            return null;
    }

    /**
     * This function delete value for a key
     * @param type $path
     * @return null
     */
    public static function delete($name) {
        self::init();

        if (isset($_COOKIE[$name])) {
            return setcookie($name, "", time() - 9000);
        }
        else
            return true;
    }

}