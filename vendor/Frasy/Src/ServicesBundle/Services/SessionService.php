<?php

/**
 * This file is a part of Frasy 
 * 
 * (c) 2013 Pierre BOURGEOIS <dragonralph@gmail.com>
 *
 */
/**
 * This variable is used for add namespaces to autoloader
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Service;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

class SessionService extends Service {

    private static $initied = false;
    
    public function __construct() {
        parent::__construct();
        self::init();
    }

    public static function init() {
        
        if (!self::$initied && !headers_sent() && session_id() === ""){
            EventsListener::notifyEvent("FRASY_SERVICE_SESSION_LOAD");
            session_start();
            self::$initied = true;
        }
    }

    /**
     * This function set value for a key
     * @param type $path
     * @param type $value
     */
    public static function set($path, $value, $personnal = true) {
        self::init();
        $path = explode('.', $path);
        
        if (!isset($_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'])) {
            $_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'] = array();
        }

        if ($personnal === true) {
            $var = &$_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'];
        }
        else {
            $var = &$_SESSION[FRASY_ENVIRONMENT];
        }

        foreach ($path as $k => $v) {
            if (!isset($var[$v])) {
                $var[$v] = array();
            }
            elseif (!is_array($var[$v])) {
                $var[$v] = array();
            }

            $var = &$var[$v];
        }


        $var = $value;

        return true;
    }

    /**
     * This function returns value for a path key
     * @param type $path
     * @return null
     */
    public static function get($path, $personnal = true) {
        self::init();

        $path = explode('.', $path);
        
        if (!isset($_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'])) {
            $_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'] = array();
        }

        if ($personnal === true) {

            $var = $_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'];
        }
        else {
            $var = $_SESSION[FRASY_ENVIRONMENT];
        }

        foreach ($path as $k => $v) {
            if (!isset($var[$v])) {
                return null;
            }

            $var = $var[$v];
        }


        return $var;
    }
    
    /**
     * This function delete value for a path key
     * @param type $path
     * @return null
     */
    public static function delete($path, $personnal = true) {
        self::init();

        $path = explode('.', $path);
        
        if (!isset($_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'])) {
            $_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'] = array();
        }

        if ($personnal === true) {
            $var = &$_SESSION[FRASY_ENVIRONMENT]['PersonnalValues'];
        }
        else {
            $var = &$_SESSION[FRASY_ENVIRONMENT];
        }

        foreach ($path as $k => $v) {
            if (!isset($var[$v])) {
                return null;
            }

            $var = &$var[$v];
        }


        $var = null;
        
    }
    
    

    /**
     * This function allows to add flash messages
     * @param type $key
     * @param type $message
     * @param array $parameters
     */
    public static function addFlash($key, $message, array $parameters = array()) {
        self::init();

        $_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key][] = array(
            'key' => $key,
            'message' => $message,
            'parameters' => array(
        'turns' => 0
            ) + $parameters
        );
    }

    /**
     * This function get flash message with $key key
     * @param type $key
     * @return array
     */
    public static function getFlash($key) {
        self::init();

        if (isset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key]) && count($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key]) > 0) {
            return $_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key];
        }
        elseif ($key === null) {
            if (isset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'])) {
                return $_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'];
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    /**
     * This function destroy all flash message
     */
    private static function destroyFlash() {
        if (isset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'])) {
            unset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages']);
        }
    }

    /**
     * This function checks state of flash message
     */
    public static function checkFlashs() {
        $flashs = isset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages']) ? $_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'] : array();
        foreach ($flashs as $key1 => $params1) {
            foreach($params1 as $key => $params){
                if ($params['parameters']['turns'] < 1) {
                    unset($_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key1][$key]);
                } else {
                    $_SESSION[FRASY_ENVIRONMENT]['Frasy']['Services']['FlashMessages'][$key1][$key]['parameters']['turns']--;
                }
            
            }
        }
        
            
    }

    public static function FrasyRenderEvent() {
        self::checkFlashs();
    }

    /**
     * Destroy session
     */
    public static function destroy() {
        if(self::$initied===true){
            session_unset();
            session_destroy();
            self::$initied=false;
        }
    }

}