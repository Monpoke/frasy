<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Service;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HelpersBundle\Helpers\HeritanceHelper;

class SqlDumper extends Service {
    
    /**
     * This array is default parameters
     * @var array 
     */
    protected $parameters = array(
        'table.data' => true,
        'table.drop' => true,
        'table.create' => true
    );
    
    public function __construct(){
        
    }
    
    /**
     * Set a parameter
     * @param string $name
     * @param bool $value
     * @return boolean
     */
    public function setParameter($name, $value){
        if(!is_bool($value))
            return FALSE;
        
        if(!isset($this->parameters[$name]))
            return false;
        
        $this->parameters[$name] = $value;
    }
    
    /**
     * This function will return a parameter value
     * @param type $name
     * @return boolean
     */
    public function getParameter($name){
        if(isset($this->parameters[$name]))
            return $this->parameters[$name];
        
        return false;
    }
    
    /**
     * This function will return the sql request
     * @param array|string $tables
     * @return string
     * @throws \Frasy\ExceptionsBundle\Exceptions\ConfigurationException
     */
    public function dumpTable($tables="*"){
        set_time_limit(0);
        
        $config = Controller::getConfiguration();
        
        // Get db config
        if($config['Environment']['dbMultiple']===true)
            $configDb = $config['Environment']['dbParams']['default'];
        else {
            $configDb = $config['Environment']['dbParams'];
        }
        
        
        $tables = "*";
        try {
            $host = !empty($configDb['host']) ? $configDb['host'] : "localhost";
            $dbname = !empty($configDb['dbname']) ? $configDb['dbname'] : null;
            
            $strConnection = 'mysql:host='.$host.';dbname='.$dbname;

            /**
             * DATASOURCE CONNECTION
             */
            $arrExtraParam = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $pdo = new \PDO($strConnection, $configDb['user'], $configDb['password'], $arrExtraParam);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            /**
             * GET TABLES
             */
            if ($tables === "*") {
                $tables = array();
                $rows = $pdo->query("SHOW TABLES")->fetchAll();

                foreach ($rows as $row) {
                    $tables[] = $row[0];
                }
            }
            else {
                $tables = is_array($tables) ? $tables : explode(',', $tables);
            }

            // BEGIN DUMP
            $return = "/**\n* SQL DUMP \n* GENERATED : " . date("m/d/Y : H:m:i:s") . "\n*/\n\n";

            //cycle through
            foreach ($tables as $table) {
                $result = $pdo->query('SELECT * FROM ' . $table);
                $num_fields = $result->columnCount();
                
                $return.= "/** \n* TABLE $table\n*/\n";
                
                if($this->getParameter("table.drop") && $this->getParameter("table.create"))
                    $return.= 'DROP TABLE IF EXISTS ' . $table . ";\n";
                
                if($this->getParameter("table.create")){
                    $row2 = $pdo->query('SHOW CREATE TABLE ' . $table)->fetch();
                    $return.= $row2[1] . ";\n\n";
                }
                
                if($this->getParameter("table.data")){
                    
                    for ($i = 0; $i < $num_fields; $i++) {
                        while ($row = $result->fetch(\PDO::FETCH_NUM)) {

                            $return.= 'INSERT INTO ' . $table . ' VALUES(';
                            for ($j = 0; $j < $num_fields; $j++) {
                                $row[$j] = addslashes($row[$j]);
                                $row[$j] = str_replace("\n", "\\n", $row[$j]);
                                if (isset($row[$j])) {
                                    $return.= '"' . $row[$j] . '"';
                                }
                                else {
                                    $return.= '""';
                                }
                                if ($j < ($num_fields - 1)) {
                                    $return.= ',';
                                }
                            }
                            $return.= ");\n";
                        }
                    }
                }
                $return.="\n\n\n";
                
                
                $result->closeCursor();
            }
            
            return $return;
            
        }
        catch (\PDOException $e) {
            throw new \Frasy\ExceptionsBundle\Exceptions\ConfigurationException($e->getMessage(), $this->getCode(), $e);
        }
    }
    
}