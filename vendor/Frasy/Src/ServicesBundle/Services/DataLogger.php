<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\DefaultBundle\Controllers\Service;
use Frasy\ExceptionsBundle\Exceptions\BasicException;
use Frasy\LoadersBundle\Controllers\ConfigurationController;

class DataLogger extends Service {

    // Granularité
    const GRAN_VOID = 'VOID';  // Aucun archivage
    const GRAN_MONTH = 'MONTH'; // Archivage mensuel
    const GRAN_YEAR = 'YEAR';  // Archivage annuel

    /**
     * Cette fonction va créer le dossier
     * @param type $type
     * @param type $name
     * @param type $granularity
     * @return string|boolean
     */

    public static function path($depot, $name, $granularity = self::GRAN_YEAR) {

        // Contrôle des arguments
        if (empty($name)) {
            trigger_error("Paramètres incorrects", E_USER_WARNING);
            return false;
        }

        /**
         *  Si $type est vide, on enregistre le log directement à la racine du dépôt
         */
        $type_path = $depot . DS;
        if (!is_dir($type_path)) {
            mkdir($type_path);
        }



        // Création du dossier granularity
        if ($granularity == self::GRAN_VOID) {
            $logfile = $type_path . $name . '.log';
        }
        elseif ($granularity == self::GRAN_MONTH) {
            $mois_courant = date('Y-m');
            $type_path_mois = $type_path . $mois_courant;
            if (!is_dir($type_path_mois)) {
                mkdir($type_path_mois);
            }
            $logfile = $type_path_mois . DS . date('d') . '_' . $name . '.log';
        }
        elseif ($granularity == self::GRAN_YEAR) {
            $current_year = date('Y');
            $type_path_year = $type_path . $current_year;
            if (!is_dir($type_path_year)) {
                mkdir($type_path_year);
            }
            $logfile = $type_path_year . DS . $current_year . '_' . $name . '.log';
        }
        else {
            trigger_error("Granularité '$granularity' non prise en charge", E_USER_WARNING);
            return false;
        }



        return $logfile;
    }

    /**
     * This function will log datas in parameters
     * @param string $type
     * @param string $name
     * @param string $row
     * @param self::constant $granularity
     * @return boolean
     */
    public static function log($depot, $type, $name, $row, $granularity = self::GRAN_YEAR, $code = 200) {

        $logfile = self::path($depot, $name, $granularity);

        if ($logfile === false) {
            trigger_error("Impossible d'enregistrer le log", E_USER_WARNING);
            return false;
        }

        // Ajout de la date et de l'heure au début de la ligne
        $row = date('[j M y H:i:s ' . $code . ']') . ' [' . $type . '] [client ' . $_SERVER['REMOTE_ADDR'] . '] ' . $row;

        // [Wed Oct 11 14:32:52 2000] [error] [client 127.0.0.1]
        // Ajout du retour chariot de fin de ligne si il n'y en a pas
        if (!preg_match('#\n$#', $row)) {
            $row .= "\n";
        }

        self::write($logfile, $row);
    }

    public static function logEnvironmentData($token = null, $bundle = null, $controller = null, $action = null, $force = false) {
        
    }

    /**
     * 
     * @param BasicException $e
     * @return type
     */
    public static function logThrowing($e) {
        $config = Controller::getConfiguration();

        $logPath = $config['Environment']['Tmp']['Logs'];
        $logLevel = $config['Environment']['LogLevel'];

        $allLogs = ConfigurationController::getAllLogsLevels();
        
        if ($logPath === false || $logLevel === false) {
            return;
        }

        $currentLogLevel = $allLogs[$logLevel];
        
        $errorLevel = method_exists($e, 'getType') && isset($allLogs[$e->getType()]) ? $allLogs[$e->getType()] : $currentLogLevel;
        $errorLevelName = method_exists($e, 'getType') && isset($allLogs[$e->getType()]) ? $e->getType() : $logLevel;
        
        // NOT SAVE IF ERROR IS UP TO LOG WANTED
        if($errorLevel > $currentLogLevel){
            return;
        }

        
        self::log($logPath, $errorLevelName, "error", $e->getMessage(), self::GRAN_MONTH, $e->getCode());
    }

    private static function write($logfile, $row) {
        
        if (empty($logfile)) {
            trigger_error("<code>$logfile</code> est vide", E_USER_WARNING);
            return false;
        }
        
        $fichier = fopen($logfile, 'a+');
        fputs($fichier, $row);
        fclose($fichier);
    }

}
