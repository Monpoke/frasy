<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0
 */

namespace Frasy\ServicesBundle\Services;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\DefaultBundle\Controllers\Service;
use Frasy\HelpersBundle\Helpers\RandomHelper;
use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;
use Frasy\ServicesBundle\Services\SessionService;

/**
 * SECURITY SERVICE
 * This class provides functionalities for Security improvement.
 */
class SecurityService extends Service {

    public function __construct() {
        parent::__construct();
        self::init();
    }

    /**
     * Gets the CRSF token
     * @return string
     */
    public static function getCrsfToken() {
        $current = SessionService::get("Frasy.Services.Security.CrsfToken.new", false);

        if ($current === null) {
            self::createCrsfToken();

            return self::getCrsfToken();
        } elseif ($current === false) {
            return SessionService::get("Frasy.Services.Security.CrsfToken.old", false);
        }

        return $current;
    }

    /**
     * This function creates a CRSF token.
     * It's called by Frasy
     */
    public static function createCrsfToken() {
        $config = Controller::getConfiguration();
        if (isset($config['Parameters'])) {
            $securityConfig = (bool) $config['Parameters']['Security']['RegenerateCrsfOnRefresh'];
        } else {
            $securityConfig = false;
        }
        /**
         * GENERATE A TOKEN BY PAGE
         */
        if ($securityConfig === true) {

            // Old Token
            $oldToken = SessionService::get("Frasy.Services.Security.CrsfToken.new", false);
            SessionService::set("Frasy.Services.Security.CrsfToken.old", $oldToken, false);

            // New Token
            $token = RandomHelper::randomString(20);
            SessionService::set("Frasy.Services.Security.CrsfToken.new", $token, false);
        }
        /**
         * GENERATE A TOKEN IN START NAVIGATION
         */ else {
            // Old Token
            $token = SessionService::get("Frasy.Services.Security.CrsfToken.old", false);
            if ($token === null) {
                // generate a token
                $newToken = RandomHelper::randomString(20);

                SessionService::set("Frasy.Services.Security.CrsfToken.old", $newToken, false);
                SessionService::set("Frasy.Services.Security.CrsfToken.new", $newToken, false);
            } else {
                // exit($oldToken);
            }
        }
    }

    /**
     * Compares CRSF Tokens
     * @param string $input
     * @return boolean
     */
    public static function compareCrsfTokens($input) {

        if (defined('FRASY_SECURITY_CRSF') && FRASY_SECURITY_CRSF === true) {
            return true;
        }

        $previousToken = SessionService::get("Frasy.Services.Security.CrsfToken.old", false);

        if ($previousToken !== $input) {
            return false;
        }

        return true;
    }

    /**
     * Destroy session after inactivity.
     */
    public static function saveLastActivity() {
        $config = Controller::getConfiguration();
        if (isset($config['Parameters'])) {
            $securityConfig = $config['Parameters']['Security']['SessionDestroyAfter'] !== false;
        } else {
            $securityConfig = false;
        }

        // Check can be executed
        if (!$securityConfig) {
            return false;
        }

        /**
         * Check
         */
        $lastTime = (int) SessionService::get("Frasy.Services.Security.LastActivity", false);

        // time for session destroy
        $timeout = $config['Parameters']['Security']['SessionDestroyAfter'];
        $destroyByFrasy = $config['Parameters']['Security']['SessionDestroyFrasy'];

        $currentTime = time();
        // set last time
        SessionService::set("Frasy.Services.Security.LastActivity", $currentTime, false);

        

        // destroy if applicable
        if ($lastTime != 0) {
            $diffSeconds = $currentTime - $lastTime;

            if ($diffSeconds > $timeout) {
                self::destroySession($destroyByFrasy);
                EventsListener::notifyEvent("FRASY_SESSION_EXPIRED_BY_INACTIVITY");
            }
        }
    }

    /**
     * Called on boot.
     */
    public static function initWithFrasyEvent() {
        self::createCrsfToken();
        self::saveLastActivity();
    }

    /**
     * This function secure all data that it's receives
     * @param type $protections
     * @param type $value
     */
    public static function secureData(array $protections, $value) {
        foreach ($protections as $v) {
            if ($v == "trim") {
                $value = trim($value);
            } elseif ($v == "htmlspecialchars") {
                $value = htmlspecialchars($value);
            } elseif ($v == "htmlentities") {
                $value = htmlentities($value);
            }
        }

        return $value;
    }

    public static function hashData($in) {
        $config = Controller::getConfiguration();

        return sha1($config['Parameters']['Security']['Hash'] . $in . $config['Parameters']['Security']['Hash'] . $in);
    }

    public static function encrypt($data, $key = false) {
        if ($key === false) {
            $config = Controller::getConfiguration();
            $key = $config['Parameters']['Security']['Crypt'];
        }

        $data = serialize($data);
        $td = mcrypt_module_open(MCRYPT_DES, "", MCRYPT_MODE_ECB, "");
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = base64_encode(mcrypt_generic($td, '!' . $data));
        mcrypt_generic_deinit($td);
        return $data;
    }

    public static function decrypt($data, $key = false) {
        if ($key === false) {
            $config = Controller::getConfiguration();
            $key = $config['Parameters']['Security']['Crypt'];
        }

        $td = mcrypt_module_open(MCRYPT_DES, "", MCRYPT_MODE_ECB, "");
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = mdecrypt_generic($td, base64_decode($data));
        mcrypt_generic_deinit($td);

        if (substr($data, 0, 1) != '!') {
            return false;
        }

        $data = substr($data, 1, strlen($data) - 1);
        return unserialize($data);
    }

    
    /**
     * Destroy session by Frasy
     * @param type $destroyByFrasy
     */
    private static function destroySession($destroyByFrasy) {
        if ($destroyByFrasy) {
            SessionService::destroy();
        }
    }

}
