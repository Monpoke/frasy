<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 * @todo Construction dynamique de classe avec arguments

 */

namespace Frasy\ServicesBundle\Controllers;

use Frasy\ServicesBundle\Interfaces\Validator as ValidatorInterface;
use Frasy\DefaultBundle\Controllers\Service;
use Frasy\ExceptionsBundle\Exceptions\ConfigurationException;
use Frasy\DefaultBundle\Controllers\Controller;

abstract class ValidatorController extends Service implements ValidatorInterface {

    protected $types = array();

    public function __construct() {
        $this->addTypeToValidate("phone", "#^0[1-9]([-. ]?[0-9]{2}){4}$");
        $this->addTypeToValidate("regex", "@regex");

        $this->addTypeToValidate("numeric", "@ctype_digit");
        $this->addTypeToValidate("is_numeric", "@is_numeric");
        $this->addTypeToValidate("is_int", "@is_int");
        $this->addTypeToValidate("alphanumeric", "@ctype_alnum");
        $this->addTypeToValidate("string", "@is_string");
        $this->addTypeToValidate("alpha", "@ctype_alpha");
        $this->addTypeToValidate("print", "@ctype_print");
        $this->addTypeToValidate("any", "@any");
        $this->addTypeToValidate("slug", "@slug");

        $this->addTypeToValidate("alphanumericCanonical", "@alphanumericCanonical");

        $this->addTypeToValidate("float", "/FILTER_VALIDATE_FLOAT");
        $this->addTypeToValidate("ip", "/FILTER_VALIDATE_IP");
        $this->addTypeToValidate("url", "/FILTER_VALIDATE_URL");
        $this->addTypeToValidate("email", "App\Config\Configuration\ValidatorService::validateEmailFormat");
        $this->addTypeToValidate("boolean", "/FILTER_VALIDATE_BOOLEAN");

        $this->addTypeToValidate("length", "@length");
        $this->addTypeToValidate("between", "@between");
        $this->addTypeToValidate("unique", "App\Config\Configuration\ValidatorService::checkUnique");
        $this->addTypeToValidate("match", "@match");
        $this->addTypeToValidate("inArray", "@inArray");
        $this->addTypeToValidate("allowEmpty", "@allowEmpty");
        $this->addTypeToValidate("securityToken", "@securityToken");
    }

    final public function addTypeToValidate($name, $pattern) {
        $this->types[$name] = $pattern;
    }

    final public function typeExists($name) {
        if (isset($this->types[$name])) {
            return true;
        }
        elseif (strpos($name, '\\') !== false) { // Maybe a class
            return true;
        }

        return false;
    }

    final public function validate($type, $input, $options = null, array $othersVars = array()) {
        $pattern = $type;

        if (!isset($this->types[$type]) && strpos($type, '::') === false) {
            return false;
        }
        elseif (isset($this->types[$type])) {
            $pattern = $this->types[$type];
        }

        if (substr($pattern, 0, 1) === "/") {
            $pattern = substr($pattern, 1);

            if (in_array($pattern, filter_list()) && filter_var($input, $this->convert($pattern))) {
                return true;
            }
        }
        elseif (substr($pattern, 0, 1) === "#") {
            $pattern = substr($pattern, 1);


            if (filter_var($pattern, validate_regexp) && preg_match("#$pattern#", $input)) {
                return true;
            }
        }
        elseif (strpos($pattern, '::') !== false) {
            $pattern2 = explode('::', $pattern);
            if (count($pattern2) !== 2) {
                throw new ConfigurationException(__("The pattern $pattern for type $type is not valid."));
            }

            $class = "\\" . $pattern2[0];
            $action = $pattern2[1];

            if (!class_exists($class)) {
                throw new ConfigurationException(__("The pattern $pattern for type $type is not valid : class $class was not found"));
            }

            $class2 = new $class();
            if (!method_exists($class2, $action)) {
                throw new ConfigurationException(__("The pattern $pattern for type $type is not valid : action $action for class $class was not found"));
            }

            return $class2->$action($input, $options, $othersVars);
        }
        else {
            if (strpos($pattern, '::') !== false) {
                exit($pattern);
            }


            if ($pattern === "@is_numeric" && (is_numeric($input) || is_integer($input) || $input == 0)) {
                return true;
            }
            elseif ($pattern === "@is_int" && is_int($input)) {
                return true;
            }
            elseif ($pattern === "@slug" && preg_match("#^[^-][a-z-]{1,}[^-]$#", $input)) {
                return true;
            }
            elseif ($pattern === "@ctype_digit" && (ctype_digit($input) || is_integer($input) || $input == 0)) {
                return true;
            }
            elseif ($pattern === "@ctype_alpha" && ctype_alpha($input)) {
                return true;
            }
            elseif ($pattern === "@ctype_alnum" && (ctype_alnum($input) || is_integer($input) || $input == 0)) {
                return true;
            }
            elseif ($pattern === "@alphanumericCanonical" && (ctype_alnum($input) || is_integer($input) || $input == 0)) {
                return true;
            }
            elseif ($pattern === "@is_string" && is_string($input)) {
                return true;
            }
            elseif ($pattern === "@ctype_print" && ctype_print($input)) {
                return true;
            }
            elseif ($pattern === "@length") {
                if (isset($options['minLength']) && strlen($input) < $options['minLength']) {
                    return false;
                }
                elseif (isset($options['maxLength']) && strlen($input) > $options['maxLength']) {
                    return false;
                }

                return true;
            }
            elseif ($pattern === "@between") {
                if (isset($options['minValue']) && (float)($input) < (float)$options['minValue']) {
                    return false;
                }
                elseif (isset($options['maxValue']) && (float)($input) > (float)$options['maxValue']) {
                    return false;
                }

                return true;
            }
            elseif ($pattern === "@match") {

                if (!isset($options['src'])) {
                    return false;
                }
                elseif (!isset($othersVars[$options['src']])) {
                    return false;
                }
                elseif ($othersVars[$options['src']] !== $input) {
                    return false;
                }
                else {
                    return true;
                }
            }
            elseif ($pattern === "@any") {
                return true;
            }
            elseif ($pattern === "@allowEmpty") {
                if (empty($input)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            elseif ($pattern === "@inArray") {

                if (isset($options['options']['type']) && ($options['options']['type'] === "select" OR $options['options']['type'] === "checkbox")) {

                    $allowedValues = isset($options['options']['options']['values']) ? $options['options']['options']['values'] : array();

                    return $this->checkIfSubValueExistForInArray($allowedValues, $input);
                }
            }
            elseif ($pattern === "@securityToken") {
                return \Frasy\ServicesBundle\Services\SecurityService::compareCrsfTokens($input);
            }
            
            elseif ($pattern === "@regex") {
                if(!isset($options['regex'])){
                    return false;
                }
                
                return (bool)preg_match($options['regex'], $input);                
            }
        }



        return false;
    }

    /**
     * 
     * @param type $array
     * @param type $valueToSearch
     * @return boolean
     */
    private function checkIfSubValueExistForInArray($array, $valueToSearch) {

        if (isset($array[$valueToSearch])) {
            return true;
        }

        foreach ($array as $key => $value) {
            if (is_array($value)) {

                if ($this->checkIfSubValueExistForInArray($array[$key], $valueToSearch) === true) {
                    return true;
                }
            }
        }

        return false;
    }

    public function convert($input) {
        switch ($input) {
            case "FILTER_VALIDATE_INT":
                return FILTER_VALIDATE_INT;


            default:
                return false;
        }
    }

    public function checkUnique($value, $options, array $othersVars = array()) {
        $em = Controller::getEntityManager();

        if (isset($options['checkIfDifferent']) && $options['checkIfDifferent'] === true) {

            $oldValue = isset($options['options']['analyse']['defaultValue']) ? $options['options']['analyse']['defaultValue'] : null;

            // This option is usefull in the case where a entity is updated
            if ($oldValue !== null && $oldValue === $value) {
                return true;
            }
        }

        $repo = $em->getRepository("\\" . $options['entityName']);

        $r = $repo->findOneBy(array($options['fieldName'] => $value));

        if ($r === null) {
            return true;
        }
        else {
            return false;
        }
    }

    public function validateEmailFormat($value, $options) {

        if (empty($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return true;
    }

}
