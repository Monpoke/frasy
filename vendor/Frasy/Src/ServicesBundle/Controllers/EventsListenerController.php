<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */


namespace Frasy\ServicesBundle\Controllers;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ExceptionsBundle\Exceptions\AppException;

class EventsListenerController  extends Controller {
    
    private static $registredEvents = array();
    
    

    public static function registerEvent($eventName, $called, array $parameters = array()) {

        // checks
        if(!is_string($eventName) or empty($eventName)) {
            trigger_error(__("Event name must be a string"));
            return;
        }
        
        
        $registred = &self::$registredEvents;

        $registred[$eventName][] = array(
            'name' => $eventName,
            'called' => $called,
            'parameters' => $parameters,
        );

        return true;
    }

    public static function notifyEvent($eventName, array $parameters = array()) {

        if(isset(self::$registredEvents[$eventName])){
             self::callEvent($eventName, $parameters, $eventName);
        }
        
        if(isset(self::$registredEvents['SPECIAL_LISTEN_TO_ALL_EVENTS'])){
            if (strpos($eventName, 'FRASY') !== false) {
                return;
            }
            
            self::callEvent('SPECIAL_LISTEN_TO_ALL_EVENTS', $parameters, $eventName);
        }
        
        
    }
    
    protected static function callEvent($eventName, array $parameters, $orName){
        
            foreach(self::$registredEvents[$eventName] as $k=>$v){
                $called = $v['called'];
                $calledParameters = array_merge($parameters, $v['parameters'], array('eventName' => $orName));
        
                if(strpos($called, '::')){ // static
                    $called2 = explode('::', $called);
                    $controllerName = $called2[0];
                    
                    $action = strpos($called2[1], 'Event') === false ? $called2[1]."Event" : $called2[1];
                    
                    if(!class_exists($controllerName)){
                        trigger_error (__("Class $controllerName not exists."), E_USER_ERROR);
                        return;
                    }
                
                    if(!method_exists($controllerName, $action)){
                        trigger_error (__("Action $action not exists in class $controllerName."), E_USER_ERROR);
                        return;
                    }
                    
                    call_user_func_array(array($controllerName, $action), $calledParameters);
                    
                    
                    
                } else { // controller
                    $e = explode(':', $called);
                    if(count($e) != 2){
                        trigger_error (__("Please verify $called."), E_USER_ERROR);
                    }
                    
                    $controllerName = $e[0];
                    $action = strpos($e[1], 'Event') === false ? $e[1]."Event" : $e[1];
                    
                    if(!class_exists($controllerName)){
                        trigger_error (__("Class $controllerName not exists."), E_USER_ERROR);
                        return;
                    }
                    
                    $controller = new $controllerName();
                    
                    if(!method_exists($controller, $action)){
                        trigger_error (__("Action $action not exists in class $controller."), E_USER_ERROR);
                        return;
                    }
                    
                    $controller->$action($calledParameters);
                    
                    
                }
                
                
            }
             
    }

    public static function getRegistred() {
        return self::$registredEvents;
    }

    public static function initialization() {
        
    }
    
    
}

