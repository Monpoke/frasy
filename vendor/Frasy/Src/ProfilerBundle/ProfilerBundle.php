<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle;
use Frasy\DefaultBundle\Controllers\Bundle;

class ProfilerBundle extends Bundle {
    
    public function declareResources(){
        $this->loadResource("events", $this->getDir() . DS . "Resources" . DS . "Config" . DS . "events.php");
        $this->loadResource("routing", $this->getDir() . DS . "Resources" . DS . "Config" . DS . "routing.php");
        
        
    }
    
    public function getDir() {
        return __DIR__;
    }
    
    public function getNamespace() {
        return __NAMESPACE__;
    }
    
    public function getName() {
        return "FrasyProfiler";
    }
    
    
}


