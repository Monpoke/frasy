<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers;

use Frasy\HelpersBundle\Helpers\RandomHelper;

class FrontController extends CommonController {

    public function indexAction() {
        return $this->render("Frasy:ProfilerBundle:index.html.twig");
    }

    public function toolsAction() {
        return $this->render("Frasy:ProfilerBundle:tools.html.twig");
    }

    public function downloadAction($file) {
        $config = $this->getConfiguration();
        $dir = $config['Environment']['Tmp']['PublicFiles'] . DS . "tmp";

        $file = $dir . DS . $file;

        if (!is_file($file)) {
            throw new \Frasy\ExceptionsBundle\Exceptions\NotFoundException(__("File not found."));
        }

        $content = unserialize(file_get_contents($file));
        
        
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $content['filename']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        
        echo $content['content'];
        unlink($file);
        exit;
    }

}

