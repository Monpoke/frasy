<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers;

use Frasy\HelpersBundle\Helpers\RandomHelper;

class GeneratorsController extends CommonController {

    /**
     * This function returns a view
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function indexAction() {
        
        return $this->render("Frasy:ProfilerBundle:generators/index.html.twig");
    }
    
    
    
}

