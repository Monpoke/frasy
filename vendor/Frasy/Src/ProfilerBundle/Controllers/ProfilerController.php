<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HelpersBundle\Helpers\FormatHelper;
use Frasy\HelpersBundle\Helpers\RandomHelper;
use Frasy\ServicesBundle\Services\DataLogger;

class ProfilerController extends Controller {

    protected static $bundle;
    protected static $controller;
    protected static $action;
    protected static $token;
    protected static $code = 500;
    protected static $startTime = null;
    protected static $endTime = null;
    protected static $routeName;
    protected static $memoryUsage = "";
    protected static $errors = array();

    public function __construct() {
        parent::__construct();
    }

    public static function addBar($html) {

        // foreach errors
        $errors = "";
        $config = Controller::getConfiguration();
        $siteUri = urlPrettify($config['Environment']['SiteUri'])."/";
        
        foreach(self::$errors as $error){
            $errors .= "<small>{$error['errstr']} : Line {$error['errline']} in file {$error['errfile']}</small><br /><br />";
            
        }
        
        if(!empty($errors)){
            $errors = "<details closed><summary>There are errors on this page.</summary>$errors</details>";
        }
        
        $html = preg_replace("#\<body(.+)\>#Usi", "$0".$errors, $html);
        
        $code = self::$code;
        $bundle = self::$bundle;
        $controller = self::$controller;
        $action = self::$action;
        $token = self::getToken();

        $codeColor = "green";
        $codeTitle = "Status: ";
        switch ($code) {

            case '500':
                $codeColor = "red";
                $codeTitle .= "Internal Error";
                break;

            default:
                $codeColor = "green";
                $codeTitle .= "Found";
                break;
        }

        if (!self::isProfiler()) {
            //$router = $this->getBundle("FrasyRouter");
            $url = self::getUrlFromRoute(array('route' => '_profiler_view_log', 'arguments' => array('token' => $token)));
        } else {
            $url = "#";
        }

        $timeExec = self::calcTime();
        $routeName = self::$routeName;
        $memoryUsage = self::$memoryUsage;

        $style = <<<EOF
background-image: linear-gradient(bottom, rgb(18,18,17) 27%, rgb(36,36,36) 51%, rgb(9,10,10) 82%);
background-image: -o-linear-gradient(bottom, rgb(18,18,17) 27%, rgb(36,36,36) 51%, rgb(9,10,10) 82%);
background-image: -moz-linear-gradient(bottom, rgb(18,18,17) 27%, rgb(36,36,36) 51%, rgb(9,10,10) 82%);
background-image: -webkit-linear-gradient(bottom, rgb(18,18,17) 27%, rgb(36,36,36) 51%, rgb(9,10,10) 82%);
background-image: -ms-linear-gradient(bottom, rgb(18,18,17) 27%, rgb(36,36,36) 51%, rgb(9,10,10) 82%);
z-index:10000;
background-image: -webkit-gradient(
	linear,
	left bottom,
	left top,
	color-stop(0.27, rgb(18,18,17)),
	color-stop(0.51, rgb(36,36,36)),
	color-stop(0.82, rgb(9,10,10))
);

position:fixed;
margin-left:0;
bottom:0;
left: 0;
color:white;
height:30px;
padding-left:20px;

width:100%;
line-height:30px;
EOF;

        $style = self::deleteSpace($style);


        $styleBloc = <<<EOF
margin-right: 30px;
    border-left:black solid 1px;
    display:inline-block;
    padding-left:15px;
EOF;


        $bar = <<<EOF
    <div style="$style" id="_Frasy_Profiler_BottomBar">
        
   <a href="$url" title="View rapport">
        <div style='$styleBloc border-left:none;padding-left:0px;'>
            <img src='{$siteUri}bundles/profilerbundle/images/profiler_logo.png' alt='Logo profiler' style='vertical-align: middle;width: 20px;margin-right: 5px;' />Profiler
        </div></a>
    
    <div style='$styleBloc'>
        Execution time: {$timeExec}
    </div>
    
    <div title='$codeTitle' style='$styleBloc'>
        
        <div style="display:inline-block;margin-right:8px;background-color:$codeColor;border-radius:5px;padding-left:3px;padding-right:3px;height:24px;line-height:24px;">$code</div>
        
        <span title="Route name: $routeName">$bundle::$controller::$action()</span>
    </div>
                
    <div style='$styleBloc'>
       Memory Usage: {$memoryUsage}
    </div>
   
    <div style='$styleBloc;cursor:pointer;font-style:italic;font-size:0.8em;float: right;' onclick="document.getElementById('_Frasy_Profiler_BottomBar').style.display = 'none';">
       Hide bar
    </div>
   
     
   
   </div>
        
EOF;
        $bar = self::deleteSpace($bar);

        $output = str_replace('</body>', $bar . "\n</body>", $html);

        return $output;
    }

    public static function implodeStyles(array $array) {
        $final = "";

        foreach ($array as $k => $v) {
            $final .= "$k:$v;";
        }

        return $final;
    }

    public static function deleteSpace($blocs) {
        $blocs = str_replace("
", '', $blocs);
        $blocs = str_replace("	", "", $blocs);

        return $blocs;
    }

    public static function getToken() {
        return self::$token;
    }

    public static function isProfiler() {
        if (strpos(self::$bundle, 'ProfilerBundle') !== false) {
            return true;
        }
        return false;
    }

    public static function calcTime() {
        if (self::$startTime === null or self::$endTime === null) {
            return "Error";
        }

        $tps = self::$endTime - self::$startTime;
        return number_format($tps, 4) . "s";
    }

    /**
     * register start time
     */
    public static function frasyBootingEvent() {
        self::$startTime = microtime(true);
        
        self::$token = RandomHelper::randomString(20);
    }

    /**
     * 
     */
    public static function frasyDeterminedRouteEvent($return, $status = 500) {
        self::$controller = $return['controller'];
        self::$action = $return['action'];
        self::$bundle = $return['namespace'];
        self::$code = $status;
        self::$routeName = $return['routeName'];
        self::$memoryUsage = FormatHelper::convertMemory(memory_get_usage());
                
       
    }

    public static function frasyBeforeRenderEvent() {
        self::$endTime = microtime(true);
    }

    /**
     * Log actions..
     */
    public static function frasyFinishedEvent() {
        self::logData();
    }

    protected static function logData(){
        if (!self::isProfiler()) {
            return DataLogger::logEnvironmentData(self::$token, self::$bundle, self::$controller, self::$action);
        }
        else {
            return false;
        }
    }
    
    public static  function addDetectedErrorEvent($params){
        self::$errors[] = $params;
    }
}

