<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers\Modules;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\ServicesBundle\Services\SqlDumper;
use Frasy\LoadersBundle\Controllers\CacheController as Cache;

class DatabaseController extends Controller {

    /**
     * This function returns a view for token selected
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function indexAction() {

        return $this->render("Frasy:ProfilerBundle:database/index.html.twig", array(
                    'path' => APP
        ));
    }

    /**
     * This function returns a view for token selected
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function dumpHomeAction() {

        $Form = $this->createFormBuilder();

        $Form->add("dumpStructure", "select", array(
            'label_attr' => array(
                'text' => __("Dump structure?")
            ),
            'values' => array(
                'yes' => __("Yes"),
                'no' => __("No")
            ),
                )
        );

        $Form->add("dropBefore", "select", array(
            'label_attr' => array(
                'text' => __("Insert drop table?")
            ),
            'values' => array(
                'yes' => __("Yes"),
                'no' => __("No")
            ),
                )
        );

        $Form->add("dumpData", "select", array(
            'label_attr' => array(
                'text' => __("Dump data?")
            ),
            'values' => array(
                'yes' => __("Yes"),
                'no' => __("No")
            ),
                )
        );

        $Form->add("choiceForDownload", "select", array(
            'label_attr' => array(
                'text' => __("Download or save?")
            ),
            'values' => array(
                'download' => __("Download"),
                'save' => __("Save on server"),
                'both' => __("Both")
            ),
                )
        );

        $Form->add("pathSave", "text", array(
            'label_attr' => array(
                'text' => __("Path where save file"),
                'title' => __("If path does not exists, it will be created.")
            ),
            'value' => APP . DS . "Resources" . DS . "Saves" . DS . "SqlDump"
                )
        );


        if ($this->methodIs("POST")) {
            $Form->bind($this->getRequest());
            if ($Form->validate()) {

                $dumpStructure = ($Form->getFieldValue("dumpStructure") === "yes") ? true : false;
                $dropBefore = ($Form->getFieldValue("dropBefore") === "yes") ? true : false;
                $dumpData = ($Form->getFieldValue("dumpData") === "yes") ? true : false;

                // only download for the moment
                $SqlDumper = new SqlDumper();

                $SqlDumper->setParameter("table.drop", $dropBefore);
                $SqlDumper->setParameter("table.create", $dumpStructure);
                $SqlDumper->setParameter("table.data", $dumpData);

                $config = $this->getConfiguration();

                $sqlFile = $SqlDumper->dumpTable();


                /**
                 * IF WANT TO SAVE LOCAL
                 */
                $this->lunchLocalSave($Form, $sqlFile);

                /**
                 * IF WANT DOWNLOAD
                 */
                $this->lunchDownload($Form, $config, $sqlFile);
            }
        }

        return $this->render("Frasy:ProfilerBundle:database/dump/index.html.twig", array(
                    'path' => APP,
                    'form' => $Form->createView()
        ));
    }

    protected function lunchDownload($Form, $config, $sqlFile) {
        if ($Form->getFieldValue("choiceForDownload") === "download" || $Form->getFieldValue("choiceForDownload") === "both") {
            $tmpFile['content'] = $sqlFile;
            $tmpFile['mimetype'] = "sql";
            $tmpFile['generated'] = new \DateTime();
            $tmpFile['filename'] = "SQLDUMP_" . date("m/d/Y-H:m:i:s") . ".sql";

            $dir = $config['Environment']['Tmp']['PublicFiles'] . DS . "tmp";

            if (!is_dir($dir)) {
                mkdir($dir, 0777);
            }

            $filename = sha1(time() . microtime(true));
            $file = $dir . DS . $filename;

            file_put_contents($file, serialize($tmpFile));

            $this->redirectRoute("_profiler_download", array(
                'file' => $filename
            ));
        }
    }

    public function lunchLocalSave($Form, $sqlFile) {
        if ($Form->getFieldValue("choiceForDownload") === "save" || $Form->getFieldValue("choiceForDownload") === "both") {
            $filename = "SQLDUMP_" . date("m-d-Y_H-m-i-s") . ".sql";
            $dir = $Form->getFieldValue("pathSave");

            if (!is_dir($dir)) {
                if (mkdir($dir, 0777, true)) {
                    $dirOkay = true;
                }
                else {
                    \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", __("The dump dir cannot be created. Permissions are ok?"), array('type'=>'error'));
                }
            }
            else {
                $dirOkay = true;
            }

            if (isset($dirOkay) && $dirOkay) {
                
                if (file_put_contents($dir . DS . $filename, $sqlFile)) {
                    \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", __("The dump file was created under specified path."), array('type'=>'success'));
                }
                else {
                    \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", __("The dump file cannot be created. Permissions are ok?"), array('type'=>'error'));
                }
            }

            return true;
        }
    }

}
