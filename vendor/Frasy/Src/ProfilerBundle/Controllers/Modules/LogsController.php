<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers\Modules;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HelpersBundle\Helpers\RandomHelper;

class LogsController extends Controller {

    /**
     * This function returns a view for token selected
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function indexAction() {
        $config = $this->getConfiguration();
        $pathLog = $config['Environment']['Tmp']['Logs'] . DS . FRASY_ENVIRONMENT;
        $files = glob($pathLog . DS . "*.log");
        $allFiles = array();
        foreach ($files as $file) {
            if (strpos($file, 'error.log') !== false)
                continue;

            $data = array();
            $lastPart = explode(DS, $file);
            $lastPart = $lastPart[count($lastPart) - 1];

            $segm = explode('-', $lastPart);
            $tok = $segm[0];


            $allFiles[$segm[1]] = array(
                'token' => $tok,
                'date' => $segm[1],
                'bundle' => $segm[2],
                'controller' => $segm[3],
                'action' => str_replace('.log', '', $segm[4])
            );
        }

        krsort($allFiles);

        return $this->render("Frasy:ProfilerBundle:logs/index.html.twig", array('files' => $allFiles));
    }

    /**
     * This function returns a view for token selected
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function viewLogAction($token) {
        $config = $this->getConfiguration();
        $pathLog = $config['Environment']['Tmp']['Logs'] . DS . FRASY_ENVIRONMENT;
        $files = glob($pathLog . DS . "*.log");
        foreach ($files as $file) {
            $lastPart = explode(DS, $file);
            $lastPart = $lastPart[count($lastPart) - 1];

            $segm = explode('-', $lastPart);
            $tok = $segm[0];
            if ($tok != $token) {
                continue;
            }

            $contentLog = unserialize(file_get_contents($file));
            
            $contentLog['Configuration'] = \Frasy\LoadersBundle\Controllers\ConfigurationController::secureConfiguration($contentLog['Configuration']);

            return $this->render("Frasy:ProfilerBundle:logs/view.html.twig", array('data' => $contentLog));
        }

        throw new \Frasy\ExceptionsBundle\Exceptions\NotFoundException(__("Log file was not found."));
    }

    /**
     * This function delete a log with token selected
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function deleteLogAction($token) {
        $config = $this->getConfiguration();
        $pathLog = $config['Environment']['Tmp']['Logs'] . DS . FRASY_ENVIRONMENT;
        $files = glob($pathLog . DS . "*.log");
        foreach ($files as $file) {
            $lastPart = explode(DS, $file);
            $lastPart = $lastPart[count($lastPart) - 1];

            $segm = explode('-', $lastPart);
            $tok = $segm[0];
            if ($tok != $token) {
                continue;
            }

            if (unlink($file)) {
                \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "Log file deleted.", array('type' => 'success'));
            }
            else {
                \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "Cannot delete log file.", array('type' => 'error'));
            }

            break;
        }

        $this->redirectRoute("_profiler_logs");
    }

    /**
     * This function delete all logs
     * @param string $token
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function deleteAllLogAction() {
        $config = $this->getConfiguration();
        $pathLog = $config['Environment']['Tmp']['Logs'] . DS . FRASY_ENVIRONMENT;
        $files = glob($pathLog . DS . "*.log");

        $noDeleted = array();
        $filesDeleted = 0;
        foreach ($files as $file) {

            if (unlink($file)) {
                $filesDeleted++;
            }
            else {
                $noDeleted[] = $file;
            }
        }
        if($filesDeleted===0 && count($noDeleted)>0){
          \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "No log files was deleted.", array('type' => 'error'));
  
        } elseif($filesDeleted>0){
            
            if(count($noDeleted)>0){
                \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "$filesDeleted files deleted. But ".count($noDeleted)." files was not deleted...", array('type' => 'error'));
            } else {
                \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "$filesDeleted files deleted.", array('type' => 'success'));

            }
            
        } else {
             \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", "No files deleted.", array('type' => 'info'));

        }
        
        $this->redirectRoute("_profiler_logs");
    }

}

