<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers\Modules;

use Frasy\DefaultBundle\Controllers\DefaultRouter;
use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\RouterBundle\Controllers\RouterController;

class ToolsController extends Controller {

    public function routesAction() {
        
        $config = $this->getConfiguration();
        
        /* @var $router DefaultRouter */
        $router = $config['Routing'];
        $routes = $router->getRoutes();
        
        // DELETE FRASY ROUTES
        unset($routes["\Frasy\ProfilerBundle"]);
        
        
        return $this->render("Frasy:ProfilerBundle:tools/routes.html.twig", array('routes'=>$routes));
    }

}

