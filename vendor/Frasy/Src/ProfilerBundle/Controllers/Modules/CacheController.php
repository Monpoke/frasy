<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers\Modules;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HelpersBundle\Helpers\RandomHelper;
use Frasy\ConsoleBundle\Console\CacheCommand;

class CacheController extends Controller {

    public function cacheClearAction() {
        $cacheCommand = new CacheCommand();
        $return = $cacheCommand->cacheClearCommand();

        if ($return['status'] === false)
            $status = "error";
        else
            $status = "success";

        \Frasy\ServicesBundle\Services\SessionService::addFlash("_profiler_flash", $return['message'], array('type' => $status));
        $this->redirectRoute("_profiler_tools");
    }

}

