<?php

/*
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;

class SystemController extends CommonController {

    /**
     * 
     * @return \Frasy\HttpBundle\HttpKernel\HttpResponse
     */
    public function indexAction(){  
        /**
         * LOAD BUNDLES
         */
        $AllBundlesLoaded = $this->getBundles();
        $bundlesLoaded = array();
        foreach ($AllBundlesLoaded as $name => $value) {
            $bundlesLoaded[] = (get_class($value));
        }
        
        /** 
         * RENDER
         */
        return $this->render("Frasy:ProfilerBundle:system.html.twig", array('bundlesLoaded' => $bundlesLoaded));
    }
    
    
    
    
}

