<?php

/**
  ====================================================
  ----------- This file is a part of Frasy -----------
  ====================================================

  @author     Pierre Bourgeois <dragonralph@gmail.com>
  @link       http://frasy.plumedor.fr
  @version    1.0.0
  @since      1.0.0

 */

namespace Frasy\ProfilerBundle\Controllers;

use Frasy\DefaultBundle\Controllers\Controller;
use Frasy\HelpersBundle\Helpers\RandomHelper;

class CommonController extends Controller {

    public function __construct(){
        $config = $this->getConfiguration();
        if($config['Environment']['Debug']!==true)
            throw new \Frasy\ExceptionsBundle\Exceptions\NotFoundException(__("No route matching!"));
    }
    

}

