<?php 

/**
 * Events file for bundle ProfilerBundle
 */

use Frasy\ServicesBundle\Controllers\EventsListenerController as EventsListener;

EventsListener::registerEvent("FRASY_BOOT_START", "\Frasy\ProfilerBundle\Controllers\ProfilerController::frasyBooting", array());
EventsListener::registerEvent("FRASY_BOOT_END", "\Frasy\ProfilerBundle\Controllers\ProfilerController::frasyFinished", array());
EventsListener::registerEvent("FRASY_BEFORE_RENDER", "\Frasy\ProfilerBundle\Controllers\ProfilerController::frasyBeforeRender", array());
EventsListener::registerEvent("FRASY_DETERMINED_ROUTE", "\Frasy\ProfilerBundle\Controllers\ProfilerController::frasyDeterminedRoute", array());

EventsListener::registerEvent("ERROR_DETECTED", "\Frasy\ProfilerBundle\Controllers\ProfilerController::addDetectedError");
