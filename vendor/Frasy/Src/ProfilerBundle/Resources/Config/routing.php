<?php
$prefix = "_profiler";
$router->setPrefix($prefix);
$router->setBundleName('profilerBundle');

$router->addRoute('_profiler_home', '/', 'FrontController::indexAction');
$router->addRoute('_profiler_tools', '/tools', 'FrontController::toolsAction');
$router->addRoute('_profiler_system', '/system', 'SystemController::indexAction');
$router->addRoute('_profiler_download', '/download/{file}', 'FrontController::download', array(
    'constraints' => array(
        'file' => array(
            'type' => 'alphanumeric'
        )
    )
));


/**
 * MODULES
 */

/** LOGS */
$router->setPrefix($prefix."/logs");

$router->addRoute('_profiler_logs', '/', 'Modules\LogsController::indexAction');

$router->addRoute('_profiler_view_log', '/view/{token}', 'Modules\LogsController::viewLogAction', array(
    'constraints' => array(
        'token' => array(
            'type' => 'alphanumeric'
            
        )
    )
));
$router->addRoute('_profiler_delete_log', '/delete/{token}', 'Modules\LogsController::deleteLogAction', array(
    'constraints' => array(
        'token' => array(
            'type' => 'alphanumeric'
            
        )
    )
));
$router->addRoute('_profiler_delete_all_logs', '/deleteAll', 'Modules\LogsController::deleteAllLogAction');

/** CACHE CLEAR */
$router->setPrefix($prefix."/cache");
$router->addRoute('_profiler_cache_clear', '/clear', 'Modules\CacheController::cacheClearAction');



/**
 * GENERATORS
 */
$router->setPrefix($prefix."/generators");
$router->addRoute('_profiler_generators', '/', 'GeneratorsController::indexAction');

$router->addRoute('_profiler_generators_bundles_home', '/bundles', 'Modules\Generators\BundleController::indexAction');




/** DATABASE TOOLS */
$router->setPrefix($prefix."/database");

$router->addRoute('_profiler_database_home', '/', 'Modules\DatabaseController::indexAction');
$router->addRoute('_profiler_database_dump_home', '/dump', 'Modules\DatabaseController::dumpHomeAction');
$router->addRoute('_profiler_database_dump_go', '/dump/go', 'Modules\DatabaseController::dumpGoAction');



/** TOOLS */
$router->setPrefix($prefix."/tools");

$router->addRoute('_profiler_tools_routes', '/routes', 'Modules\ToolsController::routesAction');
